var express = require('express');
const mysql = require('mysql');
var bodyParser = require("body-parser");
var cors = require('cors');
var config = require('./app/config/config.js');
var app = express();

var Chats = require('./app/Chats/index');

var server = app.listen(9090, () => {
    console.log("Server is listening on port 9090");
});
// var io = require('socket.io')(server);

const io = require("socket.io").listen(server);

app.use(bodyParser.json());
app.use(cors({
    origin: function (origin, callback) {
        return callback(null, true);
    },
    optionsSuccessStatus: 200,
    credentials: true
}));
// server.use(cors());



// Connecting to the database
const mc = mysql.createConnection({
    // host     : '192.168.1.17',
    // user     : 'root',
    // password : 'admin41**00',
    // database : 'RootSec_Local'
    host: config.DB.host,
    user: config.DB.user,
    password: config.DB.password,
    database: config.DB.Livedatabase
});

// connect to database
mc.connect();


//Serves resources from public folder
app.use(express.static('statics'));

// Require routes
require('./app/routes/routes.js')(app);

io.on('connection', function (socket) {
    io.emit('this', { will: 'be received by everyone' });

    socket.on('Sendmessage', function (from, msg) {
        console.log('I received a private message by ', from, ' saying ', msg);
        socket.broadcast.emit('Loadmessage', { message: msg });

        var newMessage = {
            Message: msg.data.text,
            ChatroomId: msg.ChatroomId,
            MessageType: msg.type == 'text' ? 1 : 2,
            SendBy: msg.userid,
            CreatedAt: new Date(),
            CreatedBy: 1
        }

        Chats.SendMessage(newMessage);
    });

    socket.on('disconnect', function () {
        io.emit('user disconnected');
    });
});

// connection port set
// app.listen(9090, () => {
//     console.log("Server is listening on port 9090");
// });