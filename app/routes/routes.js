module.exports = (app) => {


  app.use('/api/pentestapi', require('../Controllers/Custom/Pentest'));
  app.use('/api/userapi', require('../Controllers/Custom/User'));
  app.use('/api/systemapi', require('../Controllers/Custom/System'));
  app.use('/api/uploadapi', require('../Controllers/Custom/Imageupload'));
  app.use('/api/Assetapi', require('../Controllers/Custom/Asset'));
  app.use('/api/Vulnerabilitiesapi', require('../Controllers/Custom/Vulnerabilities'));
  app.use('/api/Notificationapi', require('../Controllers/Custom/Notifications'));
  app.use('/api/languageapi', require('../Controllers/Custom/LanguageTranslations'));
  app.use('/api/labelapi', require('../Controllers/Custom/LabelTranslations'));
  app.use('/api/bulkimportapi', require('../Controllers/Custom/BulkImport'));
  app.use('/api/Dashboard', require('../Controllers/Custom/Dashboard'));
  app.use('/api/ticketsapi', require('../Controllers/Custom/SupportTickets'));
  app.use('/api/chatapi', require('../Controllers/Custom/Chats'));
  app.use('/api/incidentsapi', require('../Controllers/Custom/Incident'));
  app.use('/api', require('../Controllers/Common'));
  app.use('/auth', require('../Auth'));
  // app.use('/userapi', require('../Controllers/Custom/User'));


}
