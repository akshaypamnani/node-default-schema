
const db = require('../models');
var config = require('../config/config.js');

async function SendMessage(message) {

    var ChatRoom;

    if (message.ChatroomId == 0) {
        ChatRoom = await db.Chatroom.create({
            RoomCode: '000',
            CreatedAt: new Date(),
            CreatedBy: 1
        });

        message.ChatroomId = ChatRoom.id;
    }

    var ChatMessage = await db.ChatMessages.create(message);

    var ChatRoomUsers = await db.ChatroomUsers.findAll({
        where: { RoomId: message.ChatroomId }
    });

    var ChatMessageNotificationUsers = [];

    if (ChatRoomUsers && ChatRoomUsers.length > 0) {
        ChatRoomUsers.forEach(chatuser => {
            ChatMessageNotificationUsers.push({
                MessageId: ChatMessage.id,
                UserId: message.UserId,
                ReceiverId: chatuser.UserId,
                CreatedAt: new Date(),
                CreatedBy: 1
            });
        });

        var ChatroomNotifications = db.ChatMessageNotifications.bulkCreate(ChatMessageNotificationUsers);

    }
}

exports.SendMessage = SendMessage;