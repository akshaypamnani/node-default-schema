//var mongoose = require("mongoose");
const mysql = require('mysql');
//const User = require('../../../models/user');
const User = require('../../models/user');
const db = require('../../models');
var Helper = require('../../Helper');

const Cryptr = require('cryptr');

const cryptr = new Cryptr('R@@tSeC');


// Create and Save a New Record
exports.create = async (req, res) => {
    var lang = req.headers['lang'];

    if (!req.body) {
        return res.status(200).send({
            status: 0,
            message: "content can not be empty"
        });
    }

    // Create a record
    console.log('req.body', req.body);
    const object = req.body;

    var VerifiedUser = await Helper.VerifiedUser(req.headers['authorization']);
    object.CreatedAt = new Date();
    object.CreatedBy = VerifiedUser ? VerifiedUser.id : 1;
    db[req.params.collection].create(object)
        .then(data => {
            var message = '';
            if (lang == 'nl') {
                message = 'Toegevoegd succesvol!';
            } else {
                message = 'Created Successfully!';
            }

            res.json({ status: 1, message: message, data: data });
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating."
            });
        });
};

//create and save record in pivot
exports.createpivot = async (req, res) => {
    var lang = req.headers['lang'];

    if (!req.body) {
        return res.status(200).send({
            status: 0,
            message: "content can not be empty"
        });
    }
    // Create a record
    const object = req.body;
    var VerifiedUser = await Helper.VerifiedUser(req.headers['authorization']);
    object.CreatedAt = new Date();
    object.CreatedBy = VerifiedUser ? VerifiedUser.id : 1;
    db[req.params.collection].findAll({
        where: req.body
    }).then(data => {
        if (data.length == 0) {
            // object.CreatedAt = new Date();
            // object.CreatedBy = 1;
            db[req.params.collection].create(object)
                .then(data => {
                    var message = '';
                    if (lang == 'nl') {
                        message = 'Record gemaakt!';
                    } else {
                        message = 'Record Created!';
                    }
                    return res.status(200).send({ status: 1, message: message, data: object });
                }).catch(err => {
                    res.status(500).send({
                        message: err.message || "Some error occurred while creating."
                    });
                });
            // return res.status(200).send({ status: 1, message: 'Record Created!',data:object });
        }
        else {
            var message = '';
            if (lang == 'nl') {
                message = 'Record bestaat al!';
            } else {
                message = 'Record already Exsist!';
            }
            return res.status(200).send({ status: 0, message: message, data: {} });
        }

    }).catch(err => {
        console.log(err)
        return res.status(500).send({ status: 0, message: 'Record Not Found!', data: err });
    });
};


exports.Updatepivot = (req, res) => {
    var lang = req.headers['lang'];
    const decryptedID = cryptr.decrypt(req.params.id);
    db[req.params.collection].findOne({
        where: req.body
    }).then(data => {
        if (data) {
            var message = '';
            if (lang == 'nl') {
                message = 'Record bestaat al!';
            } else {
                message = 'Record already Exsist!';
            }
            return res.status(200).send({ status: 0, message: message, data: null });
        }
        else {
            db[req.params.collection].findOne({
                where: {
                    id: decryptedID
                }
            }).then(pivotdata => {
                console.log(pivotdata);
                pivotdata.update({
                    ModifiedAt: new Date(),
                    ModifiedBy: 1,
                    UserId: req.body.UserId,
                    CompanyId: req.body.CompanyId
                })
                var message = '';
                if (lang == 'nl') {
                    message = 'Record bijgewerkt!';
                } else {
                    message = 'Record Updated!';
                }
                return res.status(200).send({ status: 1, message: message, data: pivotdata });
            }).catch(err => {
                return res.status(500).send({ status: 0, message: 'Error updating record!', data: null });
            });
        }
    }).catch(err => {
        return res.status(500).send({ status: 0, message: 'error!!!', data: null });
    });

}

//Get Pivot tables
exports.getpivot = (req, res) => {
    //console.log(Model.findAll()); 
    return db[req.params.collection].findAll({
        include: [{ all: true, nested: false }]
    }).then((contacts) =>
        res.json({ status: 1, message: "Success", data: contacts }))
        //res.send(contacts))
        .catch((err) => {
            console.log('There was an error !', JSON.stringify(err))
            return res.send(err)
        });
};

//Get Pivot tables By id
exports.getpivotById = (req, res) => {
    var lang = req.headers['lang'];
    const decryptedID = cryptr.decrypt(req.params.id);
    return db[req.params.collection].findOne({
        include: [{ all: true, nested: false }],
        where: {
            id: decryptedID
        }
    }).then(data => {
        if (!data) {
            var message = '';
            if (lang == 'nl') {
                message = 'Record niet gevonden met ID ';
            } else {
                message = 'Record not found with id ';
            }
            return res.status(200).send({
                status: 0,
                message: message + decryptedID
            });
        }
        res.json({ status: 1, message: "Success", data: data });
        //  res.send(data);
    }).catch(err => {
        if (err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Record not found with id " + decryptedID
            });
        }
        return res.status(500).send({
            message: "Error retrieving record with id " + decryptedID
        });
    });
};

// Retrieve and return all records from the collection.
exports.get = async (req, res) => {
    return db[req.params.collection].findAll({
        include: [{
            all: true,
            nested: false
        }],
        where: {
            IsActive: true
        }
    }).then((contacts) =>
        res.json({ status: 1, message: "Success", data: contacts }))
        //res.send(contacts))
        .catch((err) => {
            console.log('There was an error !', JSON.stringify(err))
            return res.send(err)
        });
};

// Find a single record with a id
exports.getById = (req, res) => {
    var lang = req.headers['lang'];
    const decryptedID = cryptr.decrypt(req.params.id);
    return db[req.params.collection].findOne({
        include: [{ all: true, nested: true }],
        where: {
            id: decryptedID,
            IsActive: true
        }
    }).then(data => {
        if (!data) {
            var message = '';
            if (lang == 'nl') {
                message = 'Record niet gevonden met ID ';
            } else {
                message = 'Record not found with id ';
            }
            return res.status(200).send({
                status: 0,
                message: message + decryptedID
            });
        }
        res.json({ status: 1, message: "Success", data: data });
        //  res.send(data);
    }).catch(err => {
        if (err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Record not found with id " + decryptedID + " " + err
            });
        }
        return res.status(500).send({
            message: "Error retrieving record with id " + decryptedID + " " + err
        });
    });
};

// Update a record identified by the id in the request
exports.update = (req, res) => {
    var lang = req.headers['lang'];
    const decryptedID = cryptr.decrypt(req.params.id);
    if (!req.body) {
        return res.status(400).send({
            message: "Record content can not be empty"
        });
    }
    else {
        db[req.params.collection].findByPk(decryptedID)
            .then(data => {
                if (!data) {
                    var message = '';
                    if (lang == 'nl') {
                        message = 'Record niet gevonden met ID ';
                    } else {
                        message = 'Record not found with id ';
                    }
                    return res.status(200).send({
                        status: 0,
                        message: message + decryptedID
                    });
                }
                // Check if record exists in db
                else {
                    // Find record and update it with the request body
                    data.update(req.body)
                        .then(data => {
                            if (!data) {
                                var message = '';
                                if (lang == 'nl') {
                                    message = 'Record niet gevonden met ID ';
                                } else {
                                    message = 'Record not found with id ';
                                }
                                return res.status(200).send({
                                    status: 0,
                                    message: message + decryptedID
                                });
                            }
                            data.update({
                                ModifiedAt: new Date(),
                                ModifiedBy: data.id
                            });
                            var message = '';
                            if (lang == 'nl') {
                                message = 'Succesvol geupdatet!';
                            } else {
                                message = 'Updated Successfully! ';
                            }
                            res.json({ status: 1, message: message, data: data });
                            // res.send(data);
                        }).catch(err => {
                            if (err.kind === 'ObjectId') {
                                return res.status(404).send({
                                    message: "Record not found with id " + decryptedID
                                });
                            }
                            return res.status(500).send({
                                message: "Error updating record with id " + decryptedID
                            });
                        });
                }
            }).catch(err => {
                if (err.kind === 'ObjectId') {
                    return res.status(404).send({
                        message: "Record not found with id " + decryptedID
                    });
                }
                return res.status(500).send({
                    message: "Error retrieving record with id " + decryptedID
                });
            });
    }
};

// Delete a record with the specified id in the request
exports.destroy = (req, res) => {
    var lang = req.headers['lang'];
    const decryptedID = cryptr.decrypt(req.params.id);

    db[req.params.collection].destroy({
        where: {
            id: decryptedID
        }
    }).then(data => {
        console.log("sucess");
        if (!data) {
            var message = '';
            if (lang == 'nl') {
                message = 'Record niet gevonden met ID ';
            } else {
                message = 'Record not found with id ';
            }
            return res.status(200).send({
                status: 0,
                message: message + decryptedID
            });
        }
        res.json({ status: 1, message: 'Succesvol verwijderd!' });
    }).catch(err => {
        if (err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Record not found with id " + decryptedID
            });
        }
        return res.status(500).send({
            message: "Could not delete record with id " + decryptedID
        });
    });
};

exports.softdestroy = (req, res) => {
    var lang = req.headers['lang'];
    const decryptedID = cryptr.decrypt(req.params.id);

    db[req.params.collection].update({ IsActive: false }, {
        where: {
            id: decryptedID
        }
    }).then(data => {
        console.log("sucess");
        if (!data) {
            var message = '';
            if (lang == 'nl') {
                message = 'Record niet gevonden met ID ';
            } else {
                message = 'Record not found with id ';
            }
            return res.status(200).send({
                status: 0,
                message: message + decryptedID
            });
        }
        var message = '';
        if (lang == 'nl') {
            message = 'Succesvol verwijderd!';
        } else {
            message = 'Successfully deleted!';
        }
        return res.status(200).send({
            status: 1,
            message: message
        });
    }).catch(err => {
        if (err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Record not found with id " + decryptedID
            });
        }
        return res.status(500).send({
            message: "Could not delete record with id " + decryptedID
        });
    });
};

exports.executeQuery = (req, res) => {
    var Model = mongoose.model(req.params.collection);
    var where = req.body.where;
    var populate = req.body.populate;
    var fields = req.body.fields;
    var sort = req.body.sort;
    var limit = req.body.limit;

    var query = Model.find({});

    if (where) {
        query = Model.find(where);
    }
    if (populate) {
        query = query.populate(populate);
    }
    if (fields) {
        query = query.select(fields);
    }
    if (sort) {
        query = query.sort(sort);
    }
    if (limit) {
        query = query.skip(limit.skip).limit(limit.limit);
    }
    if (count === true) {
        query = query.count();
    }

    query.exec()
        .then(data => {
            res.send(data);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving."
            });
        });

}

exports.GetByLimit = (req, res) => {

    console.log("Common controller....GetByCondition")
    console.log(req.params.collection)
    console.log(req.body)
    db[req.params.collection].findAll({
        include: [{ all: true, nested: true }],
        limit: req.body.limit
    }).then(data => {
        console.log(data)
        return res.status(200).send({ status: 1, message: 'Record Found!', data: data });
    }).catch(err => {
        console.log(err)
        return res.status(500).send({ status: 0, message: 'Record Not Found!', data: err });
    });
};


//common allCondition
exports.GetByCondition = (req, res) => {
    var lang = req.headers['lang'];
    var where = req.body.Where;
    var order = req.body.order;
    var limit = req.body.limit;

    var query = {};

    if (where) {
        query.where = where;
    }
    if (order) {
        query.order = order;
    }
    if (limit) {
        query.limit = limit;
    }
    query.include = [{ all: true, nested: true }];
    console.log('query', query);
    db[req.params.collection].findAll(query).then(data => {
        // console.log(data)
        var message = '';
        if (lang == 'nl') {
            message = 'Record gevonden!';
        } else {
            message = 'Record Found!';
        }
        return res.status(200).send({ status: 1, message: message, data: data });
    }).catch(err => {
        console.log(err)
        return res.status(500).send({ status: 0, message: 'Record Not Found!', data: err });
    });
};

exports.GetByConditionwithoutNested = (req, res) => {
    var lang = req.headers['lang'];
    var where = req.body.Where;
    var order = req.body.order;
    var limit = req.body.limit;

    var query = {};

    if (where) {
        query.where = where;
    }
    if (order) {
        query.order = order;
    }
    if (limit) {
        query.limit = limit;
    }
    query.include = [{ all: true, nested: false }];
    console.log('query', query);
    db[req.params.collection].findAll(query).then(data => {
        var message = '';
        if (lang == 'nl') {
            message = 'Record gevonden!';
        } else {
            message = 'Record Found!';
        }
        return res.status(200).send({ status: 1, message: message, data: data });
    }).catch(err => {
        console.log(err)
        return res.status(500).send({ status: 0, message: 'Record Not Found!', data: err });
    });
};