'use strict';

var express = require('express');
var controller = require('./common.controller');
var router = express.Router();
var auth = require('../../Auth/auth.service');

router.get('/:collection', auth.isAuthenticated(), controller.get);

router.put('/pivot/:collection', auth.isAuthenticated(), controller.createpivot);
router.get('/pivot/:collection', auth.isAuthenticated(), controller.getpivot);
router.get('/pivot/:collection/:id', auth.isAuthenticated(), controller.getpivotById);
router.post('/pivot/:collection/:id', auth.isAuthenticated(), controller.Updatepivot);
//router.get('/:collection', controller.get); 
router.get('/:collection/:id', auth.isAuthenticated(), controller.getById);
router.post('/:collection/:id', auth.isAuthenticated(), controller.update);
router.post('/:collection', auth.isAuthenticated(), controller.update);
router.put('/:collection', auth.isAuthenticated(), controller.create);
router.delete('/:collection/:id', auth.isAuthenticated(), controller.destroy);
router.delete('/soft/:collection/:id', auth.isAuthenticated(), controller.softdestroy);
router.post('/execute/conditions/:collection', auth.isAuthenticated(), controller.GetByCondition);
router.post('/execute/withoutnestedconditions/:collection', auth.isAuthenticated(), controller.GetByConditionwithoutNested);

//router.post('/execute/limit/:collection', auth.isAuthenticated(), controller.GetByLimit);

// router.post('/exeacutecondition/:collection/', controller.GetByCondition);
// router.get('/metadata/info/:collection/:type', controller.metadata);
// router.put('/bulk/insert/:collection', controller.bulk_insert);
// router.post('/server/page/:collection', controller.server_side_pagination);

module.exports = router;

