'use strict';

var express = require('express');
var SupportTicketcontroller = require('./supporttickets.controller');
var router = express.Router();
var auth = require('../../../Auth/auth.service');

router.get('/GetLatestTicketCode/', SupportTicketcontroller.GetLatestTicketCode);
router.post('/TicketTrends/', SupportTicketcontroller.TicketTrends);
module.exports = router;
