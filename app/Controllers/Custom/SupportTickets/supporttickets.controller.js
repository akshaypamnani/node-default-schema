const mysql = require('mysql');
const db = require('../../../models');
var config = require('../../../config/config.js');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const Cryptr = require('cryptr');
const cryptr = new Cryptr('R@@tSeC');
var sequelize = new Sequelize(config.DB.Livedatabase, config.DB.user, config.DB.password, {
    host: config.DB.host,
    dialect: "mysql",
    logging: function () { },
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },
});

exports.GetLatestTicketCode = async (req, res) => {
    var newCode = '';
    try {
        var Ticket = await db.Tickets.findOne({
            include: [{ all: true, nested: false }],
            where: { IsActive: true },
            order: [['id', 'DESC']]
        });

        if (Ticket) {
            newCode = GenerateTicketCode(Ticket.TicketCode);
        } else {
            newCode = GenerateTicketCode('NEW');
        }
        return res.status(200).send({ status: 1, message: 'Code generated Successfully', data: { Code: newCode } });
    }
    catch (err) {
        return res.status(500).send({ status: 0, message: "failed", data: err });
    }
};

exports.TicketTrends = (req, res) => {
    var object = req.body;
    if (object) {
        var query = "SELECT count(*) as total, `Status`, ";

        if (object.TrendsFilter && (object.TrendsFilter == 2 || object.TrendsFilter == 3)) {
            //Month -- Week
            query = query + "DAYNAME(tt.CreatedAt) as day, DATE_FORMAT(tt.CreatedAt, '%d-%b') as 'DateFormat' ";
        } else {
            //Year
            query = query + "MONTHNAME(tt.CreatedAt) as month, DATE_FORMAT(tt.CreatedAt, '%b-%y') as 'DateFormat' ";
        }

        query = query + "FROM `tblTickets` tt Where "

        if (object.userid) {
            query = query + " tt.CreatedBy=" + object.userid + " and";
        }
        if (object.currentdate) {
            query = query + " tt.CreatedAt <='" + object.currentdate + "' and ";
            if (object.TrendsFilter && (object.TrendsFilter == 2 || object.TrendsFilter == 3)) {
                query = query + "tt.CreatedAt >= CAST(DATE_ADD('" + object.currentdate + "', INTERVAL ";
                if (object.TrendsFilter == 2) {
                    //Month
                    query = query + "-30"
                } else {
                    //Week
                    query = query + "-7"
                }
                query = query + " DAY) AS CHAR) and tt.IsActive = 1 group by DAYNAME(tt.CreatedAt), `Status`"
            } else {
                //Year
                query = query + "tt.CreatedAt >= CAST(DATE_ADD('" + object.currentdate + "', INTERVAL -12 MONTH) AS CHAR) and tt.IsActive = 1 group by MONTHNAME(tt.CreatedAt), `Status`"
            }
        }
        console.log('query', query);
        sequelize.query(query, { type: Sequelize.QueryTypes.SELECT })
            .then(data => {
                console.log("success")
                console.log(data)
                return res.status(200).send({ status: 1, message: 'Success', data: data });
            }).catch(err => {
                return res.status(500).send({ status: 0, message: 'Failed', err });
            });
    } else {
        return res.status(500).send({ status: 0, message: 'Failed', err });
    }
}

const GenerateTicketCode = (TicketCode) => {
    var Ticket = 'TCK000';
    var latestYear = ((new Date()).getFullYear()).toString().substring(2);

    Ticket += latestYear;
    if (TicketCode == 'NEW') {
        Ticket += '001';
    } else {
        var lastYear = TicketCode.substring(6, 8);
        var OldTicket = TicketCode.substring(8);

        console.log('lastYear', lastYear, latestYear);
        if (lastYear == latestYear) {
            if (OldTicket && parseInt(OldTicket)) {
                OldTicket = (parseInt(OldTicket) + 1).toString();
                var pad = "000";
                OldTicket = pad.substring(0, pad.length - OldTicket.length) + OldTicket;
                console.log('OldTicket', OldTicket);
                Ticket += OldTicket;
            }
        } else {
            OldTicket = '001';
            var pad = "000";
            OldTicket = pad.substring(0, pad.length - OldTicket.length) + OldTicket;
            console.log('OldTicket', OldTicket);
            Ticket += OldTicket;
        }
    }
    return Ticket;
}

