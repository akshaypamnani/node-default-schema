const mysql = require('mysql');
const db = require('../../../models');
var config = require('../../../config/config.js');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const Cryptr = require('cryptr');
const cryptr = new Cryptr('R@@tSeC');
var sequelize = new Sequelize(config.DB.Livedatabase, config.DB.user, config.DB.password, {
    host: config.DB.host,
    dialect: "mysql",
    logging: function () { },
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },
});

exports.AddChatRoom = (req, res) => {
    var lang = req.headers['lang'];

    if (!req.body) {
        return res.status(200).send({
            status: 0,
            message: "content can not be empty"
        });
    }

    // Create a record
    console.log('req.body', req.body);
    const object = req.body;

    // Save Object in the database
    object.CreatedAt = new Date();
    db.Chatroom.create(object)
        .then(data => {
            let ChatroomUsersData = {
                UserId: object.CreatedBy,
                RoomId: data.id,
                CreatedAt: new Date(),
                CreatedBy: object.CreatedBy
            }
            db.ChatroomUsers.create(ChatroomUsersData);

            var message = '';
            if (lang == 'nl') {
                message = 'Toegevoegd succesvol!';
            } else {
                message = 'Created Successfully!';
            }

            res.json({ status: 1, message: message, data: data });
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating."
            });
        });
};

