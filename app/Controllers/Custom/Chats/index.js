'use strict';

var express = require('express');
var Chatcontroller = require('./chats.controller');
var router = express.Router();
var auth = require('../../../Auth/auth.service');

router.post('/AddChatRoom', Chatcontroller.AddChatRoom);
module.exports = router;