const mysql = require('mysql');
const db = require('../../../models');
var config = require('../../../config/config.js');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const Cryptr = require('cryptr');
const cryptr = new Cryptr('R@@tSeC');
var sequelize = new Sequelize(config.DB.Livedatabase, config.DB.user, config.DB.password, {
    host: config.DB.host,
    dialect: "mysql",
    logging: function () { },
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },
});

exports.GetNotificationsbyUserId = (req, res) => {
    const decryptedID = cryptr.decrypt(req.params.id);
    console.log('decryptedID', decryptedID);
    db.NotificationUsers.findAll({
        include: [{
            model: db.Notifications,
            include: [db.PenTest, {
                model: db.PenTestComments,
                include: [db.LanguageTranslations, db.User, {
                    model: db.Vulnerabilities
                }]
            }]
        }],
        where: {
            UserId: decryptedID,
            IsActive: true
        }
    }).then(data => {
        return res.status(200).send({ status: 1, message: 'Success', data: data });
    });
}

exports.ReadNotificationsbyUserId = (req, res) => {
    const decryptedID = cryptr.decrypt(req.params.id);
    console.log('decryptedID', decryptedID);
    sequelize.query("UPDATE `tblNotificationUsers` SET `IsRead`=1 WHERE `UserId` =" + decryptedID, { type: Sequelize.QueryTypes.UPDATE })
        .then(data => {
            console.log("success")
            console.log(data)
            return res.status(200).send({ status: 1, message: 'Success', data: data });
        }).catch(err => {
            return res.status(500).send({ status: 0, message: 'Failed', err });
        });
}


exports.AddNotification = (req, res) => {
    var lang = req.headers['lang'];
    var object = req.body;
    upsert(db.Notifications, object, { id: object.id ? object.id : null }).
        then(function (notificationresult) {
            console.log("notificationresult..", notificationresult);
            sequelize.query("SELECT tu.id FROM `tblUser` tu left join tblUserRole tur on tu.id = tur.UserId left join tblUserCompany tuc on tu.id = tuc.UserId left join tblPenTest tp on tuc.CompanyId = tp.CompanyId and tp.IsActive = 1 left join tblPenTestComments tpc on tp.id = tpc.PenTestId Where tu.IsActive = 1 and tu.id != tpc.CommentorId and tp.IsPublished=1 and tur.RoleId=1 and tp.id=" + object.PenTestId + " and tpc.id=" + notificationresult.CommentId, { type: Sequelize.QueryTypes.SELECT })
                .then(users => {
                    console.log('users', users);
                    users.forEach(user => {
                        db.NotificationUsers.create({
                            NotificationId: notificationresult.id,
                            UserId: user.id,
                            IsRead: 0,
                            CreatedAt: new Date(),
                            ModifiedAt: new Date(),
                            CreatedBy: 1,
                            ModifiedBy: 1
                        });
                    });
                    sequelize.query("SELECT tu.id FROM `tblUser` tu left join tblUserRole tur on tu.id = tur.UserId left join tblPenTestComments tpc on tu.id != tpc.CommentorId where tur.RoleId != 1 and tpc.id =" + notificationresult.CommentId, { type: Sequelize.QueryTypes.SELECT })
                        .then(users => {
                            console.log('users', users);
                            users.forEach(user => {
                                db.NotificationUsers.create({
                                    NotificationId: notificationresult.id,
                                    UserId: user.id,
                                    IsRead: 0,
                                    CreatedAt: new Date(),
                                    ModifiedAt: new Date(),
                                    CreatedBy: 1,
                                    ModifiedBy: 1
                                });
                            });
                            var message = '';
                            if (lang == 'nl') {
                                message = 'Succesvol Gemaakt/Bijgewerkt!';
                            } else {
                                message = 'Successfully Created/Updated!';
                            }
                            return res.status(200).send({ status: 1, message: message, data: notificationresult });
                        }).catch(err => {
                            return res.status(500).send({ status: 0, message: 'Failed', err });
                        });
                }).catch(err => {
                    return res.status(500).send({ status: 0, message: 'Failed', err });
                });
        }).catch(err => {
            return res.status(500).send({ status: 0, message: "Create/update Notification failed", data: err });
        });
}

function upsert(modal, values, condition) {
    console.log("modal...", modal);
    console.log("values...", values);
    console.log("condition...", condition);
    return modal
        .findOne({ where: condition })
        .then(function (obj) {
            console.log("obj ...", obj);
            if (obj) {
                console.log("obj update...", obj);
                values.CreatedAt = new Date();
                // values.CreatedBy=1;
                return obj.update(values);
            }
            else {
                console.log("Create...", values);
                values.ModifiedAt = new Date();
                // values.ModifiedBy=1;
                return modal.create(values);
            }
        });
}
