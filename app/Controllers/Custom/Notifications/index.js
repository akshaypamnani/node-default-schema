'use strict';

var express = require('express');
var Notificationcontroller = require('./notification.controller');
var router = express.Router();
var auth = require('../../../Auth/auth.service');

router.put('/AddNotification/',auth.isAuthenticated(),  Notificationcontroller.AddNotification);
router.post('/ReadNotifications/:id',auth.isAuthenticated(),  Notificationcontroller.ReadNotificationsbyUserId);
router.get('/GetNotifications/:id',auth.isAuthenticated(),  Notificationcontroller.GetNotificationsbyUserId);
// router.get('/GetNotificationsByUser/:id',auth.isAuthenticated(),  Notificationcontroller.GetNotificationsbyUser);
module.exports = router;