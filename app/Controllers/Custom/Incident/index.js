'use strict';

var express = require('express');
var SupportIncidentcontroller = require('./incident.controller');
var router = express.Router();
var auth = require('../../../Auth/auth.service');

router.get('/GetLatestIncidentCode/', SupportIncidentcontroller.GetLatestIncidentCode);

module.exports = router;
