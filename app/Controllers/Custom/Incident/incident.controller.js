const mysql = require('mysql');
const db = require('../../../models');
var config = require('../../../config/config.js');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const Cryptr = require('cryptr');
const cryptr = new Cryptr('R@@tSeC');

var sequelize = new Sequelize(config.DB.Livedatabase, config.DB.user, config.DB.password, {
    host: config.DB.host,
    dialect: "mysql",
    logging: function () { },
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },
});

exports.GetLatestIncidentCode = async (req, res) => {
    var newCode = '';
    try {
        console.log('GetLatestIncidentCode');
        var Incident = await db.Incidents.findOne({
            include: [{ all: true, nested: false }],
            where: { IsActive: true },
            order: [['id', 'DESC']]
        });

        if (Incident) {
            newCode = GenerateIncidentCode(Incident.IncidentCode);
        } else {
            newCode = GenerateIncidentCode('NEW');
        }
        return res.status(200).send({ status: 1, message: 'Code generated Successfully', data: { Code: newCode } });
    }
    catch (err) {
        return res.status(500).send({ status: 0, message: "failed", data: err });
    }
};

const GenerateIncidentCode = (IncidentCode) => {
    var Incident = 'SIT000';
    var latestYear = ((new Date()).getFullYear()).toString().substring(2);

    Incident += latestYear;
    if (IncidentCode == 'NEW') {
        Incident += '001';
    } else {
        var lastYear = IncidentCode.substring(6, 8);
        var OldCode = IncidentCode.substring(8);

        console.log('lastYear', lastYear, latestYear);
        if (lastYear == latestYear) {
            if (OldCode && parseInt(OldCode)) {
                OldCode = (parseInt(OldCode) + 1).toString();
                var pad = "000";
                OldCode = pad.substring(0, pad.length - OldCode.length) + OldCode;
                console.log('OldTicket', OldCode);
                Incident += OldCode;
            }
        } else {
            OldCode = '001';
            var pad = "000";
            OldCode = pad.substring(0, pad.length - OldCode.length) + OldCode;
            console.log('OldTicket', OldCode);
            Incident += OldCode;
        }
    }
    return Incident;
}

