'use strict';

var express = require('express');
var Languagecontroller = require('./languagetranslations.controller');
var router = express.Router();
var auth = require('../../../Auth/auth.service');

router.post('/AddTranslations/', auth.isAuthenticated(), Languagecontroller.AddLanguageTranslations);
module.exports = router;