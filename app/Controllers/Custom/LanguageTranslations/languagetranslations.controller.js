const mysql = require('mysql');
const db = require('../../../models');
var config = require('../../../config/config.js');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const Cryptr = require('cryptr');
const cryptr = new Cryptr('R@@tSeC');
var sequelize = new Sequelize(config.DB.Livedatabase, config.DB.user, config.DB.password, {
    host: config.DB.host,
    dialect: "mysql",
    logging: function () { },
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },
});
var googleTranslate = require('google-translate')(config.Google.ApiKey);

exports.AddLanguageTranslations = async (req, res) => {
    var lang = req.headers['lang'];
    try {
        var object = req.body;
        if (object && object.Translations) {

            var Translations = object.Translations;

            if (object.IsUpdated && object.IsUpdated != 0) {
                Translations.forEach((trans) => {
                    if (object.lang && object.lang == 'nl-NL') {
                        trans.TitleNL = trans.Title;
                    } else {
                        trans.TitleEN = trans.Title;
                    }
                    trans.IsActive = 1;
                });
                db.LanguageTranslations.bulkCreate(Translations,
                    { updateOnDuplicate: ["TitleNL", "TitleEN"] }).then(function (languagetranslationresult) {
                        console.log('Translationsddddddfgfg', Translations);
                        var message = '';
                        if (lang == 'nl') {
                            message = 'Succesvol Taalvertaling Maken/Bijwerken';
                        } else {
                            message = 'Successfully Create/update Language Translation';
                        }
                        return res.status(200).send({ status: 1, message: message, data: languagetranslationresult });
                    });
            } else {
                Promise.all(Translations.map(async (trans) => {
                    return new Promise((resolve, reject) => {
                        googleTranslate.translate(trans.Title, 'nl', function (err, translation) {
                            if (err) {
                                trans.TitleNL = trans.Title;
                            } else {
                                trans.TitleNL = translation.translatedText;
                            }
                            googleTranslate.translate(trans.Title, 'en', function (err, translation) {
                                if (err) {
                                    trans.TitleEN = trans.Title;
                                } else {
                                    trans.TitleEN = translation.translatedText;
                                }
                                resolve(trans.TitleEN);
                            });
                        });
                    });
                })).then(function (data) {
                    db.LanguageTranslations.bulkCreate(Translations,
                        { updateOnDuplicate: ["TitleNL", "TitleEN"] }).then(function (languagetranslationresult) {
                            console.log('Translationsddddddfgfg', Translations);
                            var message = '';
                            if (lang == 'nl') {
                                message = 'Succesvol Taalvertaling Maken/Bijwerken';
                            } else {
                                message = 'Successfully Create/update Language Translation';
                            }
                            return res.status(200).send({ status: 1, message: message, data: languagetranslationresult });
                        });
                });
            }
        } else {
            return res.status(500).send({ status: 0, message: "Create/update Language Translation failed", data: err });
        }
    }
    catch (err) {
        return res.status(500).send({ status: 0, message: "Create/update Language Translation failed", data: err });
    }
};

