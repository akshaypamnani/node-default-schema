const mysql = require('mysql');
const db = require('../../../models');
var config = require('../../../config/config.js');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const Cryptr = require('cryptr');
const cryptr = new Cryptr('R@@tSeC');
var sequelize = new Sequelize(config.DB.Livedatabase, config.DB.user, config.DB.password, {
    host: config.DB.host,
    dialect: "mysql",
    logging: function () { },
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },
});
var googleTranslate = require('google-translate')(config.Google.ApiKey);

exports.AddLabels = async (req, res) => {
    var lang = req.headers['lang'];
    var object = req.body;
    try {
        if (object && object.Labels) {
            var Labels = object.Labels;
            var TranslatedLabels = [];
            Promise.all(Labels.map(async (label) => {

                let translatedLabel = {
                    DefaultKey: GeneratedKey(label),
                    CreatedAt: new Date(),
                    CreatedBy: 1,
                    ModifiedAt: new Date(),
                    ModifiedBy: 1
                }
                return new Promise((resolve, reject) => {
                    googleTranslate.translate(label, 'nl', function (err, translation) {
                        if (err) {
                            translatedLabel.TitleNL = label;
                        } else {
                            translatedLabel.TitleNL = translation.translatedText;
                        }
                        googleTranslate.translate(label, 'en', function (err, translation) {
                            if (err) {
                                translatedLabel.TitleEN = label;
                            } else {
                                translatedLabel.TitleEN = translation.translatedText;
                            }
                            TranslatedLabels.push(translatedLabel);
                            resolve(translatedLabel);
                        });
                    });
                });
            })).then(function (data) {
                db.LabelsTranslations.bulkCreate(TranslatedLabels,
                    { updateOnDuplicate: ["TitleNL", "TitleEN"] }).then(function (languagetranslationresult) {
                        var message = '';
                        if (lang == 'nl') {
                            message = 'Succesvol Taalvertaling Maken/Bijwerken';
                        } else {
                            message = 'Successfully Create/update Language Translation';
                        }
                        return res.status(200).send({ status: 1, message: message, data: languagetranslationresult });
                    });
            });
        }
    }
    catch (err) {

    }
}

exports.EditBulkLabels = async (req, res) => {
    var lang = req.headers['lang'];
    try {
        var object = req.body;
        if (object && object.Translations) {

            var Translations = object.Translations;

            db.LabelsTranslations.bulkCreate(Translations,
                { updateOnDuplicate: ["TitleNL", "TitleEN"] }).then(function (labeltranslationresult) {
                    var message = '';
                    if (lang == 'nl') {
                        message = 'Succesvol labelvertaling Maken/Bijwerken';
                    } else {
                        message = 'Successfully Create/update Label Translation';
                    }
                    return res.status(200).send({ status: 1, message: message, data: labeltranslationresult });
                });
        } else {
            return res.status(500).send({ status: 0, message: "Create/update Label Translation failed", data: err });
        }
    }
    catch (err) {
        return res.status(500).send({ status: 0, message: "Create/update Label Translation failed", data: err });
    }
};

const GeneratedKey = (S) => {
    var s_arr = S.split(" ");
    if (s_arr && s_arr.length > 0) {
        s_arr.forEach(s => {
            if (typeof s !== 'string') {
                s = '';
            } else {
                s = s.charAt(0).toUpperCase() + s.slice(1);
            }
        });
        S = s_arr.join('_');
    } else {
        S = S.charAt(0).toUpperCase() + s.slice(1);
    }
    return S;
}

exports.GetLabelsTranslations = async (req, res) => {
    var lang = req.headers['lang'];
    var object = req.body;
    var TranslatedLabels = {};
    try {
        if (object) {
            var Labels = await db.LabelsTranslations.findAll({
                include: [{ all: true, nested: false }],
                where: { IsActive: true }
            });

            if (Labels && Labels.length > 0) {
                if (object.Language == 'nl') {
                    Labels.forEach(label => {
                        TranslatedLabels[label.DefaultKey] = label.TitleNL;
                    });
                } else {
                    Labels.forEach(label => {
                        TranslatedLabels[label.DefaultKey] = label.TitleEN;
                    });
                }
            } else {
                return res.status(500).send({ status: 0, message: "Labels Translation failed", data: err });
            }
            var message = '';
            if (lang == 'nl') {
                message = 'Succesvol labelvertaling Maken/Bijwerken';
            } else {
                message = 'Successfully Create/update Label Translation';
            }
            return res.status(200).send({ status: 1, message: message, data: TranslatedLabels });
        }
    }
    catch (err) {
        return res.status(500).send({ status: 0, message: "Create/update Language Translation failed", data: err });
    }
};

