'use strict';

var express = require('express');
var Labelcontroller = require('./labeltranslations.controller');
var router = express.Router();
var auth = require('../../../Auth/auth.service');

router.post('/TranslatedLabels/', Labelcontroller.GetLabelsTranslations);
router.post('/AddLabels/', Labelcontroller.AddLabels);
router.post('/EditBulkLabels/', auth.isAuthenticated(), Labelcontroller.EditBulkLabels);
module.exports = router;