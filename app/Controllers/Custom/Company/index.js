'use strict';

var express = require('express');
var companycontroller = require('./company.controller');
var router = express.Router();
var auth = require('../../../Auth/auth.service');

//router.get('/getClientuser', usercontroller.getClientuser); 

//router.get('/getRootSecuser', usercontroller.getRootSecuser); 
//router.post('/UpdateUser/:id', usercontroller.updateUser);
router.put('/bulkaddcompany/',auth.isAuthenticated(), companycontroller.BulkAddCompany);

//router.post('/EditRootSecuser/', usercontroller.EditRootSecuser);
module.exports = router;