'use strict';

var express = require('express');
var usercontroller = require('./user.controller');
var router = express.Router();
var auth = require('../../../Auth/auth.service');

router.get('/getClientuser', auth.isAuthenticated(), usercontroller.getClientuser);

router.get('/getRootSecuser', auth.isAuthenticated(), usercontroller.getRootSecuser);

// router.get('/getUserLogins', usercontroller.getUserLogins);

router.post('/getUserIps', auth.isAuthenticated(), usercontroller.getUserIps);
//router.post('/UpdateUser/:id', usercontroller.updateUser);
router.put('/AddRootSecuser/', auth.isAuthenticated(), usercontroller.AddRootSecuser);

router.post('/EditRootSecuser/', auth.isAuthenticated(), usercontroller.EditRootSecuser);

router.post('/WhiteBlockIps/', auth.isAuthenticated(), usercontroller.WhiteorBlockIps);

router.post('/checkDuplicateEmailAddress/', auth.isAuthenticated(), usercontroller.checkDuplicateEmailAddress);

router.post('/checkDuplicateUserIp/', auth.isAuthenticated(), usercontroller.checkDuplicateUserIps);

router.get('/logout', auth.isAuthenticated(), usercontroller.logout);

router.get('/getdownloadtoken', auth.isAuthenticated(), usercontroller.GetdownloadToken);

router.get('/getusercompanywise', auth.isAuthenticated(), usercontroller.GetUserCompanywise);

router.delete('/deleteuserips/:id', auth.isAuthenticated(), usercontroller.destroy);
module.exports = router;