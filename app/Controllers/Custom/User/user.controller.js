//var mongoose = require("mongoose");
const mysql = require('mysql');
//const User = require('../../../models/user');
const User = require('../../../models/user');
const Helper = require('../../../Helper/index');
const db = require('../../../models');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const saltRounds = 16;
var CustomMail = require('../../../SendMail/Mail');
var moment = require('moment');
var crypto = require('crypto');
var config = require('../../../config/config.js');
const Cryptr = require('cryptr');
const cryptr = new Cryptr('R@@tSeC');

const jwt = require('jsonwebtoken');
const SECRET_KEY = "secretkey12345";

var sequelize = new Sequelize(config.DB.Livedatabase, config.DB.user, config.DB.password, {
    host: config.DB.host,
    dialect: "mysql",
    logging: function () { },
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },
});

exports.getClientuser = (req, res) => {
    db.User.findAll({
        where: {
            IsActive: {
                [Op.ne]: 0
            }
        },
        include: [
            { all: true, nested: true },
            {
                model: db.UserRole,
                where: { RoleId: [1, 4] },
                include: [{ all: true, nested: false }]
            }
        ]
    }).then(User => {
        res.json({ status: 1, message: "Success", data: User });
        // res.json(User)
    });
};

exports.getRootSecuser = (req, res) => {
    db.User.findAll({
        where: {
            IsActive: {
                [Op.ne]: 0
            }
        },
        include: [
            { all: true, nested: true },
            {
                model: db.UserRole,
                where: {
                    RoleId: {
                        [Op.ne]: 1
                    }
                },
                include: [{ all: true, nested: false }]
            }
        ]
    }).then(User => {
        res.json({ status: 1, message: "Success", data: User });
        // res.json(User)
    });
}


exports.AddRootSecuser = (req, res) => {
    var lang = req.headers['lang'];
    console.log("User controller....create")
    console.log('req.body', req.body);

    const object = req.body;
    return db.User.findAll({
        where: {
            Email: req.body.Email,
            IsActive: true
        }
    }).then(userdata => {
        console.log(userdata.length);
        if (userdata.length == 0) {

            object.salt = crypto.randomBytes(16).toString('hex');
            object.hashedPassword = crypto.pbkdf2Sync(object.Password, object.salt,
                1000, 64, `sha512`).toString(`hex`);
            let constPassword = object.Password;
            object.Password = "";
            db.User.create(object)
                .then(data => {
                    //send Email
                    var ToEmail = data.Email;
                    var Subject = "Uw account voor Rootdash.";
                    var Message = '<p style="color:#000000;">Beste ' + data.FirstName + ',</p>' +
                        '<p style="color:#000000;">Bedankt voor het vertrouwen in Rootsec!</p>' +
                        '<p style="color:#000000;">Wij hebben uw online omgeving geopend.</p>' +
                        '<p style="color:#000000;">U kunt inloggen met de volgende gegevens:</p>' +
                        '<p style="color:#000000;">E-mailadres: <strong>' + data.Email + '</strong></p>' +
                        '<p style="color:#000000;">Password: <strong>' + constPassword + '</strong></p>' +
                        '<p style="color:#000000;">U dient een nieuw veilig wachtwoord in te stellen na het inloggen.</p>' +
                        '<p style="color:#000000;">Mocht u niet kunnen inloggen of onverhoopt tegen problemen aanlopen, dan kunt u contact opnemen met de technische support van Rootdash via: support@rootdash.nl.</p>' +
                        '<p style="color:#000000;">Met vriendelijke groet,</p>' +
                        '<p style="color:#000000;">Team Rootdash</p>' +
                        '<p style="color:#000000;">Date : ' + moment().format('MMMM Do YYYY, h:mm:ss a') + '</p>';
                    CustomMail.sendmail(ToEmail, Subject, Message);
                    if (object.compnies) {
                        object.compnies.forEach(function (companyid) {
                            console.log(companyid);
                            db.UserCompany.findAll({
                                where: {
                                    UserId: data.id,
                                    CompanyId: companyid
                                }
                            }).then(companydata => {
                                console.log(companydata.length);
                                if (companydata.length == 0) {
                                    var compy = {
                                        CompanyId: companyid,
                                        UserId: data.id,
                                        CreatedAt: new Date(),
                                        CreatedBy: data.id
                                    }
                                    db.UserCompany.create(compy).then(cmpnyuserdata => {
                                        console.log(cmpnyuserdata);
                                    });
                                    //   res.json({ status: 1, message: 'Created Successfully!' })    
                                }
                                else {
                                    console.log("Already Exsist UserCompany.." + Userid + ".." + companyid);
                                }
                            }).catch();
                        });

                    }
                    if (object.Roles) {
                        object.Roles.forEach(function (roleid) {
                            console.log(roleid);
                            db.UserRole.findAll({
                                where: {
                                    UserId: data.id,
                                    RoleId: roleid
                                }
                            }).then(Roledata => {
                                console.log(Roledata.length);
                                if (Roledata.length == 0) {
                                    var R = {
                                        RoleId: roleid,
                                        UserId: data.id,
                                        CreatedAt: new Date(),
                                        CreatedBy: data.id
                                    }
                                    db.UserRole.create(R).then(RoleUserdata => {
                                        console.log(RoleUserdata);
                                    });
                                    //  res.json({ status: 1, message: 'Created Successfully!' })    
                                }
                            }).catch();
                        });
                    }

                    if (object.UserIps) {
                        object.UserIps.forEach(function (ip) {
                            ip.UserId = data.id;
                            db.UserIps.findOne({
                                where: { IP: ip.IP }
                            }).then(function (obj) {
                                if (!obj) {
                                    db.UserIps.create(ip).then(IpUserdata => {
                                        console.log(IpUserdata);
                                    });
                                }
                            });
                        });
                    }
                    var message = '';
                    if (lang == 'nl') {
                        message = 'Toegevoegd succesvol!';
                    } else {
                        message = 'Created Successfully!';
                    }
                    res.json({ status: 1, message: message, data: data })
                }).catch(err => {
                    res.status(500).send({
                        message: err.message || "Some error occurred while creating."
                    });
                });
        }
        else {
            console.log("User Exsist");
            var Userid = 0;
            userdata.forEach(function (user) {
                Userid = user.id;
            }),
                console.log(Userid);
            console.log(req.body.compnies);

            req.body.compnies.forEach(function (companyid) {
                console.log(companyid);
                db.UserCompany.findAll({
                    where: {
                        UserId: Userid,
                        CompanyId: companyid
                    }
                }).then(companydata => {
                    console.log(companydata.length);
                    if (companydata.length == 0) {
                        var compy = {
                            CompanyId: companyid,
                            UserId: Userid,
                            CreatedAt: new Date(),
                            CreatedBy: Userid
                        }
                        db.UserCompany.create(compy).then(cmpnyuserdata => {
                            console.log(cmpnyuserdata);
                        });

                        var message = '';
                        if (lang == 'nl') {
                            message = 'Toegevoegd succesvol!';
                        } else {
                            message = 'Successfully Created!';
                        }

                        return res.status(200).send({ status: 1, message: message });
                        //    res.json({ status: 1, message: 'Created Successfully!' })    
                    }
                    else {
                        console.log("Already Exsist UserCompany.." + Userid + ".." + companyid);
                    }
                }).catch();
            });
            object.Roles.forEach(function (roleid) {
                console.log(roleid);
                db.UserRole.findAll({
                    where: {
                        UserId: Userid
                        // ,
                        // RoleId: roleid
                    }
                }).then(Roledata => {
                    console.log(Roledata.length);
                    if (Roledata.length == 0) {
                        var R = {
                            RoleId: roleid,
                            UserId: Userid,
                            CreatedAt: new Date(),
                            CreatedBy: Userid
                        }
                        db.UserRole.create(R).then(RoleUserdata => {
                            console.log(RoleUserdata);
                        });
                        var message = '';
                        if (lang == 'nl') {
                            message = 'Toegevoegd succesvol!';
                        } else {
                            message = 'Created Successfully!';
                        }
                        res.json({ status: 1, message: message })
                    }
                    else {
                        console.log("Already Exsist UserRole.." + Userid + ".." + roleid);
                    }
                }).catch();
            });
            var message = '';
            if (lang == 'nl') {
                message = 'Gebruiker bestaat al!';
            } else {
                message = 'User Already Exsist!';
            }
            return res.status(200).send({ status: 0, message: message, data: null });
            //  return res.send({ status: 0, message: "User Already Exsist!!", data: contacts })
        }

    }).catch(err => {

    });
}

exports.EditRootSecuser = (req, res) => {
    var lang = req.headers['lang'];
    console.log("User controller....EditRootSecuser")
    // console.log('req.body', req.body);
    const object = req.body;
    //  console.log('req.body.compnies', req.body.compnies);
    return db.User.findOne({
        where: {
            id: req.body.id,
            IsActive: true
        }
    }).then(userdata => {
        object.ModifiedAt = new Date();
        object.ModifiedBy = 1;
        if (object.Password && object.Password.length > 0) {
            object.salt = crypto.randomBytes(16).toString('hex');
            object.hashedPassword = crypto.pbkdf2Sync(object.Password, object.salt,
                1000, 64, `sha512`).toString(`hex`);
            object.Password = "";
        }
        userdata.update(object)
            .then(data => {
                if (object.compnies) {
                    object.compnies.forEach(function (companyid) {
                        db.UserCompany.findAll({
                            where: {
                                UserId: object.id,
                                CompanyId: companyid
                            }
                        }).then(companydata => {
                            if (companydata.length == 0) {
                                var compy = {
                                    CompanyId: companyid,
                                    UserId: object.id,
                                    CreatedAt: new Date(),
                                    CreatedBy: data.id
                                }
                                db.UserCompany.create(compy).then(cmpnyuserdata => {
                                });
                                //   res.json({ status: 1, message: 'Created Successfully!' })    
                            }
                            else {
                                //   console.log("Already Exsist UserCompany.." + object.id + ".." + companyid);
                            }
                        }).catch();
                    });
                }

                if (object.Roles) {
                    object.Roles.forEach(function (roleid) {
                        db.UserRole.findOne({
                            where: {
                                UserId: object.id,
                            }
                        }).then(Roledata => {
                            if (Roledata) {
                                Roledata.RoleId = parseInt(roleid);
                                console.log('Roledata.RoleIdsss', Roledata);
                                Roledata.update({ RoleId: parseInt(roleid) });
                            }
                        }).catch();
                        // db.UserRole.findAll({
                        //     where: {
                        //         UserId: object.id,
                        //         RoleId: roleid
                        //     }
                        // }).then(Roledata => {
                        //     if (Roledata.length == 0) {
                        //         var R = {
                        //             RoleId: roleid,
                        //             UserId: object.id,
                        //             CreatedAt: new Date(),
                        //             CreatedBy: data.id
                        //         }
                        //         db.UserRole.create(R).then(RoleUserdata => {
                        //         });
                        //         //  res.json({ status: 1, message: 'Created Successfully!' })    
                        //     }
                        // }).catch();
                    });
                }
                if (object.UserIps) {
                    object.UserIps.forEach(function (ip) {
                        ip.UserId = data.id;
                        if (ip.id) {
                            upsert(db.UserIps, ip, {
                                id: ip.id
                            }).then(IpUserdata => {
                                console.log(IpUserdata);
                            });
                        } else {

                            db.UserIps.findOne({
                                where: { IP: ip.IP }
                            }).then(function (obj) {
                                if (!obj) {
                                    db.UserIps.create(ip).then(IpUserdata => {
                                        console.log(IpUserdata);
                                    });
                                }
                            });
                        }
                    });
                }

                var message = '';
                if (lang == 'nl') {
                    message = 'Succesvol Geüpdatet';
                } else {
                    message = 'Successfully Updated!';
                }

                return res.status(200).send({ status: 1, message: message, data: data });
            }).catch(err => {
                console.log('err', err);
                if (err.kind === 'ObjectId') {
                    return res.status(404).send({
                        message: "Record not found with id " + req.params.id
                    });
                }
                return res.status(500).send({
                    message: "Error updating record with id " + req.params.id
                });
            });
    }).catch(err => {

    });
}

exports.WhiteorBlockIps = (req, res) => {
    const object = req.body;

    var strQuery = "UPDATE `tblUserIps` SET IsIPblocked=" + object.IsIPblocked;

    if (object.Ids && object.Ids.length > 0) {
        strQuery = strQuery + " Where Id in (" + object.Ids.join() + ")";
    } else {
        return res.status(500).send({ status: 0, message: 'Failed', err });
    }
    sequelize.query(strQuery, { type: Sequelize.QueryTypes.UPDATE })
        .then(data => {
            var blockunblockarr = [];
            object.Ids.forEach(id => {
                let objblockunblock = {
                    IP: object.IP,
                    UserIPId: id,
                    Type: object.IsIPblocked,
                    BlockUnblockDate: new Date(),
                    BlockUnblockBy: 1
                };
                blockunblockarr.push(objblockunblock);
            });
            db.BlockUnblockIPlog.bulkCreate(blockunblockarr);

            return res.status(200).send({ status: 1, message: 'Success', data: data });
            // res.json({ status: 1, message: "Success", data: data });
        }).catch(err => {
            return res.status(500).send({ status: 0, message: 'Failed', err });
        });
}

exports.checkDuplicateEmailAddress = async (req, res) => {
    var lang = req.headers['lang'];
    var object = req.body;
    try {
        if (object && object.email) {
            db.RequestNewUser.findAll({
                where: {
                    Email: object.email,
                    IsAccepted: [1],
                    IsActive: true
                }
            }).then(data => {

                if (!object.IsAccepted && data && data.length > 0) {
                    return res.status(200).send({ status: 0, message: 'Email already exist', data: { IsDuplicate: true } });
                } else {
                    db.User.findAll({
                        where: {
                            Email: object.email,
                            IsActive: true
                        }
                    }).then(userdata => {
                        if (userdata && userdata.length > 0) {
                            return res.status(200).send({ status: 0, message: 'Email already exist', data: { IsDuplicate: true } });
                        } else {
                            return res.status(200).send({ status: 1, message: '', data: { IsDuplicate: false } });
                        }
                    }).catch(err => {
                        return res.status(500).send({ status: 0, message: 'Failed', data: err });
                    })
                }
            }).catch(err => {
                return res.status(500).send({ status: 0, message: 'Failed', data: err });
            });
        } else {
            return res.status(200).send({ status: 0, message: '', data: { IsDuplicate: false } });
        }
    } catch (err) {
        return res.status(500).send({ status: 0, message: 'Failed', data: err });
    }
}

exports.checkDuplicateUserIps = async (req, res) => {
    var lang = req.headers['lang'];
    var object = req.body;
    try {
        if (object && object.UserIPs && object.UserIPs.length > 0) {

            object.UserIPs.forEach(userip => {
                db.UserIps.findOne({
                    where: {
                        IP: userip
                    }
                }).then((UserIP) => {
                    console.log('UserIPsss', UserIP);

                    if (UserIP) {
                        var message = '';
                        if (lang == 'nl') {
                            message = 'Dit ' + userip + ' bestaat al';
                        } else {
                            message = 'This ' + userip + ' is already exist';
                        }
                        return res.status(200).send({ status: 0, message: message, data: { IsDuplicate: true } });
                    } else {
                        var message = '';
                        if (lang == 'nl') {
                            message = 'IP succesvol toegevoegd';
                        } else {
                            message = 'IP Added successfully';
                        }
                        return res.status(200).send({ status: 1, message: message, data: { IsDuplicate: false } });
                    }
                });
            });
        } else {
            var message = '';
            if (lang == 'nl') {
                message = 'IP succesvol toegevoegd';
            } else {
                message = 'IP Added successfully';
            }
            return res.status(200).send({ status: 1, message: message, data: { IsDuplicate: false } });
        }
    } catch (err) {
        return res.status(500).send({ status: 0, message: 'Failed', data: err });
    }
}

exports.GetUserCompanywise = async (req, res) => {

    var token = req.headers['authorization'];
    token = token.toString().substring(7);
    var VerifiedUser = await Helper.VerifyToken(token);
    if (VerifiedUser && VerifiedUser.companies && VerifiedUser.companies.length > 0) {
        console.log('Tested True');
        db.User.findAll({
            include: [{
                model: db.UserCompany,
                where: {
                    CompanyId: VerifiedUser.companies[0].CompanyId
                }
            }, {
                model: db.UserRole,
                where: {
                    RoleId: 1 //client users
                }
            }],
            where: {
                IsActive: true
            }
        }).then(data => {
            return res.status(200).send({ status: 1, message: 'Success', data: data });
        }).catch(err => {
            return res.status(500).send({ status: 0, message: 'Faileddd', data: err });
        });
    } else {
        return res.status(500).send({ status: 0, message: 'Failed', data: null });
    }
}

exports.destroy = (req, res) => {
    var lang = req.headers['lang'];
    const decryptedID = cryptr.decrypt(req.params.id);

    db.UserIps.destroy({
        where: {
            UserId: decryptedID
        }
    }).then(data => {
        console.log("sucess");
        if (!data) {
            var message = '';
            if (lang == 'nl') {
                message = '';
            } else {
                message = '';
            }
            return res.status(200).send({
                status: 0,
                message: message + decryptedID
            });
        }
        res.json({ status: 1, message: '' });
    }).catch(err => {
        if (err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Record not found with id " + decryptedID
            });
        }
        return res.status(500).send({
            message: "Could not delete record with id " + decryptedID
        });
    });
};

exports.getUserIps = (req, res) => {

    var object = req.body;

    if (object && object.Where) {

        console.log('object.Whereeee', object.Where);

        db.UserIps.findAll({
            include: [
                {
                    model: db.User,
                    include: [{
                        model: db.UserCompany,
                        include: [db.Company]
                    }]
                }],
            where: object.Where
        }).then(data => {
            return res.status(200).send({ status: 1, message: 'Success', data: data });
        }).catch(err => {
            return res.status(500).send({ status: 0, message: 'Failed', data: err });
        });
    } else {
        return res.status(500).send({ status: 0, message: 'Failed', data: null });
    }

}

exports.GetdownloadToken = (req, res) => {
    const token = jwt.sign({}, SECRET_KEY, { expiresIn: '2s' }); // 1m expiry for JWT token
    return res.json({
        status: 1,
        message: "Get token successfully!",
        data: token
    });
}

exports.logout = async (req, res) => {
    try {
        var token = req.headers.authorization;
        token = token.slice(7);
        console.log("...header ..", token);

        if (token) {
            var UserLog = await db.UserLoginLogs.findOne({
                where: { AccessToken: token, IsActive: 1 }
            });

            if (UserLog) {

                var logoutTime = new Date();
                var loginTime = new Date(UserLog.loginTime.getTime() + UserLog.loginTime.getTimezoneOffset() * 60000);
                var durationTime = diff_minutes(logoutTime, loginTime);

                console.log('durationTimessss', durationTime);

                UserLog.update({
                    logoutTime: new Date(),
                    IsOnline: 0,
                    durationTime: durationTime,
                    ModifiedAt: new Date()
                });

                res.send({
                    status: 1,
                    data: UserLog,
                    message: ""
                });
            } else {
                return res.status(200).send({
                    status: 0,
                    message: ""
                });
            }
        } else {
            return res.status(200).send({
                status: 0,
                message: ""
            });
        }
    }
    catch (error) {
        return res.status(500).send({
            status: 0,
            message: ""
        });
    }
}

function upsert(modal, values, condition) {
    console.log("modal...", modal);
    console.log("values...", values);
    console.log("condition...", condition);
    return modal
        .findOne({ where: condition })
        .then(function (obj) {
            console.log("obj ...", obj);
            if (obj) {
                console.log("obj update...", obj);
                values.ModifiedAt = new Date();
                // values.ModifiedBy=1;
                return obj.update(values);
            }
            else {
                console.log("Create...", values);
                values.CreatedAt = new Date();
                // values.CreatedBy=1;
                return modal.create(values);
            }
        })
}

function diff_minutes(dt2, dt1) {
    var diff = (dt2.getTime() - dt1.getTime()) / 1000;
    diff /= 60;
    return Math.abs(Math.round(diff));
}