const mysql = require('mysql');
const db = require('../../../models');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const readXlsxFile = require('read-excel-file/node');
var path = require('path');
var fs = require('fs');
const multer = require('multer');
var config = require('../../../config/config.js');
var crypto = require('crypto');
var helper = require('../../../Helper');
var helperschema = require('../../../Helper/schema');
var googleTranslate = require('google-translate')(config.Google.ApiKey);

var destinationPath = path.join(__dirname, '../../../../Downloads');

var storage = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, destinationPath)
    },
    filename: function (req, file, callback) {
        callback(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }
})

const upload = multer({ storage: storage, dest: destinationPath });

function VulnerabilityTypeErrorMessage(object) {

    var VulnerabilityErrorMessage = '';

    if (!object.Facing) {
        VulnerabilityErrorMessage += ',Facing';
    }
    if (!object.Risk) {
        VulnerabilityErrorMessage += ',Risk';
    }
    if (!object.Complexity) {
        VulnerabilityErrorMessage += ',Complexity';

    }
    if (!object.Effort) {
        VulnerabilityErrorMessage += ',Effort';
    }
    if (!object.IssueType) {
        VulnerabilityErrorMessage += ',IssueType';
    }

    VulnerabilityErrorMessage = VulnerabilityErrorMessage.substr(1);

    return VulnerabilityErrorMessage;

}
// mapping sheet in array 
async function MappingJSONformat(array, importType, res) {

    let lstarray = array;

    let objFormat =
    {
        importedcompany: [],
        importedsystem: [],
        importedasset: [],
        importeduser: [],
        importedVulnerabilities: [],
        rejectedcompany: [],
        rejectedsystem: [],
        rejectedasset: [],
        rejecteduser: [],
        rejectedVulnerabilities: []
    };

    if (array && array.length > 0) {
        lstarray.forEach(obj => {

            var objcompany =
            {
                id: 0,
                Code: obj.companyCode,
                Name: obj.companyName,
                LegalEntity: obj.companyLegalEntity,
                Logo: obj.companyLogo,
                CommerceNo: obj.companyCommerceNo,
                ContactPersonEmail: obj.companyContactPersonEmail,
                IpAddress: obj.companyIpAddress,
                CreatedBy: 1,
                CreatedAt: new Date(),
                ModofiedBy: 1,
                ModifiedAt: new Date(),
            }

            var objsystem =
            {
                id: 0,
                Code: obj.systemCode,
                Name: obj.systemName,
                Remarks: obj.systemRemarks,
                CompanyId: 0,
                CompanyName: obj.companyName,
                CompanyCode: obj.companyCode,
                CreatedBy: 1,
                CreatedAt: new Date(),
                ModifiedBy: 1,
                ModifiedAt: new Date(),
            }

            var objasset =
            {
                id: 0,
                Code: obj.assetCode,
                Name: obj.assetName,
                Urls: obj.assetUrls,
                IpRange: obj.assetIpRange,
                Software: obj.assetSoftware,
                Remarks: obj.assetRemarks,
                CompanyId: 0,
                CompanyName: obj.companyName,
                CompanyCode: obj.companyCode,
                SystemId: 0,
                SystemName: obj.systemName,
                SystemCode: obj.systemCode,
                CreatedBy: 1,
                CreatedAt: new Date(),
                ModifiedBy: 1,
                ModifiedAt: new Date(),
            }

            var objUser =
            {
                id: 0,
                Code: obj.userCode,
                FirstName: obj.userFirstname,
                LastName: obj.userLastname,
                Email: obj.userEmail,
                MobileNo: obj.userMobileNo,
                IP: obj.userIP,
                CompanyId: 0,
                CompanyName: obj.companyName,
                CompanyCode: obj.companyCode,
                RoleId: 0,
                RoleName: obj.userRole,
                CreatedBy: 1,
                CreatedAt: new Date(),
                ModifiedBy: 1,
                ModifiedAt: new Date()
            }

            var objVulnerabilities =
            {
                PentestCode: obj.PentestCode,
                PenTestId: obj.PenTestId,
                id: 0,
                Code: obj.VulnerabilityCode,
                Name: obj.VulnerabilityName,

                Facing: config.VulTypes[obj.Facing],
                Risk: config.VulTypes[obj.Risk],
                Complexity: config.VulTypes[obj.Complexity],
                Effort: config.VulTypes[obj.Effort],

                SystemsDesc: obj.SystemDescription,
                RiskDesc: obj.RiskDescription,
                VulnSolution: obj.VulnSolution,

                IssueType: config.VulIssueTypes[obj.IssueType],

                CreatedBy: 1,
                CreatedAt: new Date(),
                ModifiedBy: 1,
                ModifiedAt: new Date()
            }

            if (objcompany.Name) {
                if (importType == config.ImportType.Company || importType == config.ImportType.CombineStuff) {
                    let companyObject = null;

                    if (objFormat.importedcompany && objFormat.importedcompany.length > 0) {
                        companyObject = objFormat.importedcompany.filter(x => x.Name == objcompany.Name && x.Code == objcompany.Code)[0];
                    }

                    if (!companyObject) {
                        objFormat.importedcompany.push(objcompany);
                    }
                }
                else {

                    return res.status(200).send({ status: 0, message: 'Invalid import type ', data: {} });
                }


            }

            if (objsystem.Name) {
                if (importType == config.ImportType.SystemAsset || importType == config.ImportType.CombineStuff) {
                    let systemObject = null;

                    if (objFormat.importedsystem && objFormat.importedsystem.length > 0) {
                        systemObject = objFormat.importedsystem.filter(x => x.Name == objsystem.Name && x.Code == objsystem.Code)[0];
                    }

                    if (!systemObject) {
                        if (objcompany.Name || objcompany.Code) {
                            if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(objsystem.Name)) {
                                objFormat.importedsystem.push(objsystem);
                            }
                            else {
                                let objerror =
                                {
                                    message: '',
                                    data: {}
                                }
                                objerror.message = 'Invalid system ip !';
                                objerror.columnName = 'System Name';
                                objerror.data = objsystem;
                                objFormat.rejectedsystem.push(objerror);
                            }
                        }
                        else {
                            let objerror =
                            {
                                message: '',
                                columnName: '',
                                data: {}
                            }


                            objerror.message = 'Company Code required !';
                            objerror.columnName = 'Company Code';


                            objerror.data = objsystem;
                            objFormat.rejectedsystem.push(objerror);
                        }
                    }
                }
                else {
                    return res.status(200).send({ status: 0, message: 'Invalid import type ', data: {} });
                }

            }

            if (objasset.Name) {
                if (importType == config.ImportType.SystemAsset || importType == config.ImportType.CombineStuff) {
                    let assetObject = null;

                    if (objFormat.importedasset && objFormat.importedasset.length > 0) {
                        assetObject = objFormat.importedasset.filter(x => x.Name == objasset.Name && x.Code == objasset.Code)[0];
                    }
                    if (!assetObject) {
                        if (objcompany.Name || objcompany.Code) {
                            objFormat.importedasset.push(objasset);
                        }
                        else {
                            let objerror =
                            {
                                message: '',
                                columnName: '',
                                data: {}
                            }

                            objerror.message = 'Company Code required !';
                            objerror.columnName = 'Company Code';


                            objerror.data = objasset;
                            objFormat.rejectedasset.push(objerror);
                        }
                    }
                }
                else {
                    return res.status(200).send({ status: 0, message: 'Invalid import type ', data: {} });
                }

            }

            if (objUser.Email) {
                if (importType == config.ImportType.User || importType == config.ImportType.CombineStuff) {
                    let userObject = null;

                    if (objFormat.importeduser && objFormat.importeduser.length > 0) {
                        userObject = objFormat.importeduser.filter(x => x.Email == objUser.Email)[0];
                    }

                    if (!userObject) {
                        if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(objUser.IP)) {
                            objFormat.importeduser.push(objUser);
                        }
                        else {
                            let objerror =
                            {
                                message: '',
                                columnName: '',
                                data: {}
                            }

                            objerror.columnName = 'User IP';
                            objerror.message = 'Invalid ip!';
                            objerror.data = objUser;

                            objFormat.rejecteduser.push(objerror);
                        }
                    }
                }
                else {
                    return res.status(200).send({ status: 0, message: 'Invalid import type ', data: {} });
                }
            }

            if (objVulnerabilities.Name) {
                if (importType == config.ImportType.Vulnerabilities) {
                    let VulnerabilitiesObject = null;

                    if (objFormat.importedVulnerabilities && objFormat.importedVulnerabilities.length > 0) {
                        VulnerabilitiesObject = objFormat.importedVulnerabilities.filter(x => x.Name == objVulnerabilities.Name && x.Code == objVulnerabilities.Code)[0];
                    }

                    if (!VulnerabilitiesObject) {
                        if (objVulnerabilities.Facing && objVulnerabilities.Risk && objVulnerabilities.Complexity && objVulnerabilities.Effort && objVulnerabilities.IssueType) {
                            objFormat.importedVulnerabilities.push(objVulnerabilities);
                        }
                        else {
                            let objerror =
                            {
                                message: '',
                                columnName: '',
                                data: {}
                            }

                            var errorcolumn = VulnerabilityTypeErrorMessage(objVulnerabilities);

                            objerror.columnName = errorcolumn;
                            objerror.message = 'Invalid coluumn type : ' + errorcolumn;
                            objerror.data = objVulnerabilities;
                            objFormat.rejectedVulnerabilities.push(objerror);
                        }

                    }
                }
                else {
                    return res.status(200).send({ status: 0, message: 'Invalid import type ', data: {} });
                }
            }

        });
    }

    return objFormat;
}

async function ImportedRejectedLogger(importType, filename, data, res) {

    var arrayrejecteddata = [];
    var Status = 0;


    if (data.rejectedcompany.length > 0 || data.rejectedsystem.length > 0 || data.rejectedasset.length > 0 || data.rejectedasset.length > 0 || data.rejecteduser.length > 0 || data.rejectedVulnerabilities.length > 0) {
        Status = 0
    }
    else {
        Status = 1
    }

    // ImportReject Master

    var objImportMaster =
    {
        ImportType: importType,
        Status: Status,
        Filename: filename,
        CreatedAt: new Date(),
        CreatedBy: 1,
        ModifiedAt: new Date(),
        ModifiedBy: 1
    }



    db.ImportMasters.create(objImportMaster).then(loggerresponse => {
        if (Status == 0) {
            var ImportMasterId = loggerresponse.dataValues.id;

            // mapping & combining all rejected record in one array for bulk create

            if (data) {
                if (data.rejectedcompany && data.rejectedcompany.length > 0) {
                    data.rejectedcompany.forEach(company => {
                        var objectLogger =
                        {
                            ImportMasterId: ImportMasterId,
                            columnName: company.columnName,
                            message: company.message,

                            CreatedAt: new Date(),
                            CreatedBy: 1,
                            ModifiedAt: new Date(),
                            ModifiedBy: 1,
                        }

                        arrayrejecteddata.push(objectLogger);

                    });
                }

                if (data.rejectedsystem && data.rejectedsystem.length > 0) {
                    data.rejectedsystem.forEach(system => {
                        var objectLogger =
                        {
                            ImportMasterId: ImportMasterId,
                            columnName: system.columnName,
                            message: system.message,

                            CreatedAt: new Date(),
                            CreatedBy: 1,
                            ModifiedAt: new Date(),
                            ModifiedBy: 1,
                        }

                        arrayrejecteddata.push(objectLogger);

                    });
                }

                if (data.rejectedasset && data.rejectedasset.length > 0) {
                    data.rejectedasset.forEach(asset => {
                        var objectLogger =
                        {
                            ImportMasterId: ImportMasterId,
                            columnName: asset.columnName,
                            message: asset.message,

                            CreatedAt: new Date(),
                            CreatedBy: 1,
                            ModifiedAt: new Date(),
                            ModifiedBy: 1,
                        }

                        arrayrejecteddata.push(objectLogger);

                    });
                }

                if (data.rejecteduser && data.rejecteduser.length > 0) {
                    data.rejecteduser.forEach(user => {
                        var objectLogger =
                        {
                            ImportMasterId: ImportMasterId,
                            columnName: user.columnName,
                            message: user.message,

                            CreatedAt: new Date(),
                            CreatedBy: 1,
                            ModifiedAt: new Date(),
                            ModifiedBy: 1,
                        }

                        arrayrejecteddata.push(objectLogger);

                    });
                }

                if (data.rejectedVulnerabilities && data.rejectedVulnerabilities.length > 0) {
                    data.rejectedVulnerabilities.forEach(Vulnerabilities => {
                        var objectLogger =
                        {
                            ImportMasterId: ImportMasterId,
                            columnName: Vulnerabilities.columnName,
                            message: Vulnerabilities.message,

                            CreatedAt: new Date(),
                            CreatedBy: 1,
                            ModifiedAt: new Date(),
                            ModifiedBy: 1,
                        }

                        arrayrejecteddata.push(objectLogger);

                    });
                }

                if (arrayrejecteddata && arrayrejecteddata.length > 0) {
                    var rejectedloggerhistory = db.ImportRejectHistory.bulkCreate(arrayrejecteddata).then(function (createdrejectedloggerhistory) {


                    });
                }
            }
        }

    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating Import Reject Logger"
        });
    });
}
// translate vulnerability translation logic
async function Mappinglanguagetranslation(array) {
    var arryreferenceId = [];

    arryreferenceId = array.map(x => { return x.id });

    var objLanguageTranslations = await db.LanguageTranslations.findAll
        ({
            where: { IsActive: 1, ReferenceId: [arryreferenceId], PageRefId: 2 }
        });

    var mainArray = [];

    array.forEach(object => {
        var FieldsArray =
            [
                { columnName: 'Name', Title: object.Name },
                { columnName: 'SystemsDesc', Title: object.SystemsDesc },
                { columnName: 'RiskDesc', Title: object.RiskDesc },
                { columnName: 'VulnSolution', Title: object.VulnSolution }
            ];

        if (FieldsArray && FieldsArray.length > 0) {
            FieldsArray.forEach(field => {

                var objfield = mainArray.filter(x => x.DefaultKey == field.columnName && x.ReferenceId == object.id)[0];

                var LanguageTranslationId = 0;

                if (objLanguageTranslations && objLanguageTranslations.length > 0) {
                    var LanguageTranslation = objLanguageTranslations.filter(x => x.ReferenceId == object.id && x.DefaultKey == field.columnName)[0];
                    if (LanguageTranslation) {
                        LanguageTranslationId = LanguageTranslation.id;
                    }
                }

                if (!objfield) {
                    mainArray.push
                        ({
                            id: LanguageTranslationId,
                            ReferenceId: object.id,
                            DefaultKey: field.columnName,
                            Title: field.Title,
                            PageRefId: 2,
                            PageRefName: 'Vulnerability',
                        });

                } else {

                    objfield.Title = field.Title;
                }
            });
        }
    });

    Promise.all(mainArray.map(async (trans) => {
        return new Promise((resolve, reject) => {
            googleTranslate.translate(trans.Title, 'nl', function (err, translation) {
                if (err) {
                    trans.TitleNL = trans.Title;
                } else {
                    trans.TitleNL = translation.translatedText;
                }
                googleTranslate.translate(trans.Title, 'en', function (err, translation) {
                    if (err) {
                        trans.TitleEN = trans.Title;
                    } else {
                        trans.TitleEN = translation.translatedText;
                    }

                    resolve(trans);
                });
            });
        });
    })).then(async function (data) {
        db.LanguageTranslations.bulkCreate(mainArray,
            { updateOnDuplicate: ["TitleNL", "TitleEN"] }).then(function (languagetranslationresult) {
                return mainArray;
            });
    });

}

function getFileExtension(filename) {
    return filename.slice((filename.lastIndexOf(".") - 1 >>> 0) + 2);
}

exports.importBulkData = async (req, res) => {
    try {
        upload.single('excelFile')(req, res, function (err) {
            if (err) {
                console.log("Error uploading file." + err);
            }
            if (req.file) {

                var fileextension = getFileExtension(req.file.filename);

                if (fileextension == 'xlsx') {
                    var importType = req.body.importType;

                    if (importType == 0 || importType > 5) {
                        return res.status(200).send({ status: 0, message: 'Invalid import type ', data: {} });
                    }

                    var file = destinationPath + '/' + req.file.filename;

                    var schema = helperschema.schema;

                    if (file) {
                        console.log("File uploaded successfully...");
                        readXlsxFile(file, { schema }).then(async (data, errors) => {

                            console.log('data.rows', data);
                            if (data.errors && data.errors.length > 0) {
                                data.errors.forEach(item => {
                                    var object = lstdata[item.row - 1]
                                    data.splice(item.row - 1, 1);
                                });
                            };
                            var lstdata = data.rows;

                            let jsonMappingFormat = await MappingJSONformat(lstdata, importType, res);

                            // for Company reference
                            let arryCompanyCode = [];
                            var filtercompany = [];


                            arryCompanyCode = lstdata.filter(x => x.companyCode != undefined);

                            if (arryCompanyCode && arryCompanyCode.length > 0) {
                                arryCompanyCode = arryCompanyCode.map(x => { return x.companyCode });

                                filtercompany = await db.Company.findAll
                                    ({
                                        where: { IsActive: 1, Code: [arryCompanyCode] }
                                    });
                            }

                            let filterMappingFormat =
                            {
                                importedcompany: [],
                                importedsystem: [],
                                importedasset: [],
                                importeduser: [],
                                importedVulnerabilities: [],
                                rejectedcompany: [],
                                rejectedsystem: [],
                                rejectedasset: [],
                                rejecteduser: [],
                                rejectedVulnerabilities: []
                            };

                            let jsonAffectedRecord =
                            {
                                importedcompany: [],
                                importedsystem: [],
                                importedasset: [],
                                importeduser: [],
                                importedVulnerabilities: [],
                                rejectedcompany: [],
                                rejectedsystem: [],
                                rejectedasset: [],
                                rejecteduser: [],
                                rejectedVulnerabilities: []
                            };

                            // company
                            if (jsonMappingFormat.importedcompany && jsonMappingFormat.importedcompany.length) {

                                let CompanyLastCode = await helper.GetLatestCode(config.Tables.Company);

                                jsonMappingFormat.importedcompany.forEach(elementcompnay => {
                                    if (elementcompnay.Name) {
                                        if (!elementcompnay.Code) {

                                            let objcompanyelement = elementcompnay;


                                            if (!CompanyLastCode) {
                                                objcompanyelement.Code = helper.GenerateCode('NEW', config.TablePrefix.Company);
                                                CompanyLastCode = helper.GenerateCode(objcompanyelement.Code, config.TablePrefix.Company);
                                            }
                                            else {
                                                objcompanyelement.Code = CompanyLastCode;
                                                CompanyLastCode = helper.GenerateCode(CompanyLastCode, config.TablePrefix.Company);
                                            }

                                            filterMappingFormat.importedcompany.push(objcompanyelement);

                                        }
                                        else {
                                            var objCompany = filtercompany.filter(x => x.Code == elementcompnay.Code)[0];

                                            if (objCompany) {

                                                let objcompanyelement = elementcompnay;
                                                objcompanyelement.id = objCompany.id;
                                                // objcompanyelement.Code = '000' + objcompanyelement.Code
                                                filterMappingFormat.importedcompany.push(objcompanyelement);

                                            }
                                            else {

                                                let objerror =
                                                {
                                                    message: '',
                                                    columnName: '',
                                                    data: {}
                                                }


                                                objerror.message = 'Company not found of Code : ' + elementcompnay.Code;
                                                objerror.columnName = 'Company Code';

                                                objerror.data = elementcompnay;
                                                filterMappingFormat.rejectedcompany.push(objerror);
                                            }
                                        }
                                    }


                                });

                                // company bulk create or update code     

                                if (filterMappingFormat.importedcompany && filterMappingFormat.importedcompany.length > 0) {
                                    var createdcompany = await db.Company.bulkCreate(filterMappingFormat.importedcompany,
                                        // { returning: true },
                                        {
                                            updateOnDuplicate: ["Name", "LegalEntity", "Logo", "CommerceNo", "ContactPersonEmail", "IpAddress", "CreatedAt", "CreatedBy", "ModifiedAt", "ModifiedBy", "Code"]
                                        }).then(function (createdcomapny) {

                                            if (createdcomapny && createdcomapny.length > 0) {
                                                createdcomapny.forEach(cmp => {
                                                    jsonAffectedRecord.importedcompany.push(cmp.dataValues);
                                                });

                                            }
                                        });
                                }
                            }

                            // system
                            if (jsonMappingFormat.importedsystem && jsonMappingFormat.importedsystem.length > 0) {
                                let arrySystemCode = [];
                                var filtersystem = [];
                                arrySystemCode = jsonMappingFormat.importedsystem.filter(x => x.Code != undefined);

                                if (arrySystemCode && arrySystemCode.length > 0) {
                                    arrySystemCode = arrySystemCode.map(x => { return x.Code });

                                    filtersystem = await db.System.findAll
                                        ({
                                            where: { IsActive: 1, Code: [arrySystemCode] }
                                        });
                                }

                                let SystemLastCode = await helper.GetLatestCode(config.Tables.System);

                                jsonMappingFormat.importedsystem.forEach(elementsystem => {
                                    // add company reference
                                    let objsystemelement = elementsystem;

                                    let objCompany = null;

                                    if (elementsystem.Name) {
                                        if (objsystemelement.CompanyCode) {
                                            objCompany = filtercompany.filter(x => x.Code == objsystemelement.CompanyCode)[0];
                                        }
                                        else {
                                            if (importType == config.ImportType.CombineStuff) {
                                                objCompany = jsonAffectedRecord.importedcompany.filter(x => x.Name == elementsystem.CompanyName)[0];
                                            }

                                        }

                                        if (objCompany) {
                                            objsystemelement.CompanyId = objCompany.id;

                                            if (!objsystemelement.Code) {
                                                if (!SystemLastCode) {
                                                    objsystemelement.Code = helper.GenerateCode('NEW', config.TablePrefix.System);
                                                    SystemLastCode = helper.GenerateCode(objsystemelement.Code, config.TablePrefix.System);
                                                }
                                                else {
                                                    objsystemelement.Code = SystemLastCode;
                                                    SystemLastCode = helper.GenerateCode(SystemLastCode, config.TablePrefix.System);
                                                }

                                                filterMappingFormat.importedsystem.push(objsystemelement);
                                            }
                                            else {
                                                var objSystem = filtersystem.filter(x => x.Code == elementsystem.Code)[0];

                                                if (objSystem) {


                                                    let objsystemelement = elementsystem;
                                                    objsystemelement.id = objSystem.id;

                                                    let objCompany = filtercompany.filter(x => x.Code == elementsystem.CompanyCode)[0];

                                                    if (objCompany) {

                                                        //company reference should be same existing company

                                                        //check relationship between system and company

                                                        if (objSystem.CompanyId == objCompany.id) {
                                                            objsystemelement.CompanyId = objCompany.id;
                                                            filterMappingFormat.importedsystem.push(objsystemelement);
                                                        }
                                                        else {
                                                            let objerror =
                                                            {
                                                                message: '',
                                                                columnName: '',
                                                                data: {}
                                                            }
                                                            objerror.columnName = 'Company Code';
                                                            objerror.message = 'System belong to another company ' + elementsystem.CompanyCode;
                                                            objerror.data = elementsystem;
                                                            filterMappingFormat.rejectedsystem.push(objerror);
                                                        }

                                                    }
                                                }
                                                else {
                                                    let objerror =
                                                    {
                                                        message: '',
                                                        columnName: '',
                                                        data: {}
                                                    }
                                                    objerror.columnName = 'System Code';
                                                    objerror.message = 'System not found of Code : ' + elementsystem.Code;
                                                    objerror.data = elementsystem;
                                                    filterMappingFormat.rejectedsystem.push(objerror);
                                                }


                                            }
                                        }
                                        else {
                                            let objerror =
                                            {
                                                message: '',
                                                columnName: '',
                                                data: {}
                                            }
                                            objerror.columnName = 'Company Code';
                                            objerror.message = 'Company not found of Code : ' + elementsystem.CompanyCode;
                                            objerror.data = elementsystem;
                                            filterMappingFormat.rejectedsystem.push(objerror);
                                        }


                                    }
                                });
                                // system bulk create or update code     

                                var createdsystem = await db.System.bulkCreate(filterMappingFormat.importedsystem,
                                    // { returning: true },
                                    {
                                        updateOnDuplicate: ["Name", "Remarks", "CompanyId", "CreatedAt", "CreatedBy", "ModifiedAt", "ModifiedBy", "Code"]
                                    }).then(function (createdsystem) {
                                        if (createdsystem && createdsystem.length > 0) {
                                            createdsystem.forEach(sys => {
                                                jsonAffectedRecord.importedsystem.push(sys.dataValues);
                                            });
                                        }
                                    });
                            }

                            // asset
                            if (jsonMappingFormat.importedasset && jsonMappingFormat.importedasset.length > 0) {
                                let arryAssetCode = [];
                                var filterasset = [];
                                arryAssetCode = jsonMappingFormat.importedasset.filter(x => x.Code != undefined);

                                if (arryAssetCode && arryAssetCode.length > 0) {
                                    arryAssetCode = arryAssetCode.map(x => { return x.Code });

                                    filterasset = await db.Asset.findAll
                                        ({
                                            where: { IsActive: 1, Code: [arryAssetCode] }
                                        });
                                }

                                let AssetLastCode = await helper.GetLatestCode(config.Tables.Asset);

                                jsonMappingFormat.importedasset.forEach(elementasset => {
                                    // add company 
                                    let objassetelement = elementasset;

                                    if (elementasset.Name) {
                                        let objCompany = null;

                                        if (objassetelement.CompanyCode) {
                                            objCompany = filtercompany.filter(x => x.Code == objassetelement.CompanyCode)[0];
                                        }
                                        else {
                                            if (importType == config.ImportType.CombineStuff) {
                                                objCompany = jsonAffectedRecord.importedcompany.filter(x => x.Name == objassetelement.CompanyName)[0];
                                            }
                                        }

                                        if (objCompany) {
                                            if (!objassetelement.Code) {
                                                if (!AssetLastCode) {
                                                    objassetelement.Code = helper.GenerateCode('NEW', config.TablePrefix.Asset);
                                                    AssetLastCode = helper.GenerateCode(objassetelement.Code, config.TablePrefix.Asset);
                                                }
                                                else {
                                                    objassetelement.Code = AssetLastCode;
                                                    AssetLastCode = helper.GenerateCode(AssetLastCode, config.TablePrefix.Asset);
                                                }
                                                objassetelement.CompanyId = objCompany.id;
                                                objassetelement.SystemId = null;
                                                filterMappingFormat.importedasset.push(objassetelement);
                                            }
                                            else {
                                                var objAsset = filterasset.filter(x => x.Code == objassetelement.Code)[0];

                                                if (objAsset) {
                                                    let objassetelement = elementasset;

                                                    if (objCompany) {

                                                        //company reference should be same existing company

                                                        //check relationship between asset and company

                                                        if (objAsset.CompanyId == objCompany.id) {
                                                            objassetelement.id = objAsset.id;
                                                            objassetelement.CompanyId = objCompany.id;
                                                            filterMappingFormat.importedasset.push(objassetelement);
                                                        }
                                                        else {
                                                            let objerror =
                                                            {
                                                                message: '',
                                                                columnName: '',
                                                                data: {}
                                                            }

                                                            objerror.coumnName = 'Company Code';
                                                            objerror.message = 'Asset not belong to company : ' + elementasset.CompanyCode;
                                                            objerror.data = elementasset;

                                                            filterMappingFormat.rejectedasset.push(objerror);
                                                        }
                                                    }
                                                }
                                                else {
                                                    let objerror =
                                                    {
                                                        message: '',
                                                        columnName: '',
                                                        data: {}
                                                    }

                                                    objerror.coumnName = 'Asset Code';
                                                    objerror.message = 'Asset not found of Code : ' + objassetelement.Code;
                                                    objerror.data = objassetelement;
                                                    filterMappingFormat.rejectedasset.push(objerror);
                                                }
                                            }
                                        }
                                        else {
                                            let objerror =
                                            {
                                                message: '',
                                                columnName: '',
                                                data: {}
                                            }

                                            objerror.message = 'Company Code required !';
                                            objerror.columnName = 'Company Code';

                                            objerror.data = elementasset;
                                            filterMappingFormat.rejectedasset.push(objerror);
                                        }


                                    }



                                });
                                // asset bulk create or update code     

                                var createdasset = await db.Asset.bulkCreate(filterMappingFormat.importedasset,
                                    // { returning: true },
                                    {
                                        updateOnDuplicate: ["Name", "Urls", "IpRange", "Software", "Remarks", "CompanyId", "CreatedAt", "CreatedBy", "ModifiedAt", "ModifiedBy", "Code"]
                                    }).then(function (createdasset) {
                                        if (createdasset && createdasset.length > 0) {
                                            createdasset.forEach(asset => {
                                                jsonAffectedRecord.importedasset.push(asset.dataValues);
                                            });
                                        }
                                    });
                            }

                            // user
                            if (jsonMappingFormat.importeduser && jsonMappingFormat.importeduser.length > 0) {
                                let arryUserCode = [];
                                let arryUserId = [];
                                var filteruser = [];
                                var filterUserCompany = [];

                                arryUserCode = jsonMappingFormat.importeduser.filter(x => x.Code != undefined);

                                if (arryUserCode && arryUserCode.length > 0) {
                                    arryUserCode = arryUserCode.map(x => { return x.Code });


                                    filteruser = await db.User.findAll
                                        ({
                                            where: { IsActive: 1, Code: [arryUserCode] }
                                        });


                                    arryUserId = filteruser.map(x => { return x.id });

                                    filterUserCompany = await db.UserCompany.findAll
                                        ({
                                            where: { UserId: [arryUserId] }
                                        });
                                }

                                let arryuserEmail = [];

                                arryuserEmail = jsonMappingFormat.importeduser.map(x => { return x.Email });

                                let ExistUsers = await db.User.findAll
                                    ({
                                        where: { IsActive: 1, Email: [arryuserEmail] }
                                    });


                                let UserLastCode = await helper.GetLatestCode(config.Tables.User);

                                jsonMappingFormat.importeduser.forEach(elementuser => {
                                    if (elementuser.Email) {
                                        let objuserelement = elementuser;

                                        let objCompany = null;

                                        if (objuserelement.CompanyCode) {
                                            objCompany = filtercompany.filter(x => x.Code == objuserelement.CompanyCode)[0];
                                        }
                                        else {
                                            if (importType == config.ImportType.CombineStuff) {
                                                objCompany = jsonAffectedRecord.importedcompany.filter(x => x.Name == objuserelement.CompanyName)[0];
                                            }

                                        }

                                        if (!objuserelement.Code) {
                                            // bulk insert code
                                            let objexistuser = ExistUsers.filter(x => x.Email == objuserelement.Email)[0];

                                            if (!objexistuser) {
                                                if (!UserLastCode) {
                                                    objuserelement.Code = helper.GenerateCode('NEW', config.TablePrefix.User);
                                                    UserLastCode = helper.GenerateCode(objuserelement.Code, config.TablePrefix.User);
                                                }
                                                else {
                                                    objuserelement.Code = UserLastCode;
                                                    UserLastCode = helper.GenerateCode(UserLastCode, config.TablePrefix.User);
                                                }

                                                objuserelement.Password = 'Login12*';
                                                objuserelement.salt = crypto.randomBytes(16).toString('hex');
                                                objuserelement.hashedPassword = crypto.pbkdf2Sync(objuserelement.Password, objuserelement.salt,
                                                    1000, 64, `sha512`).toString(`hex`); objuserelement.Password = '';

                                                filterMappingFormat.importeduser.push(objuserelement);

                                            }
                                            else {

                                                let objerror =
                                                {
                                                    message: '',
                                                    columnName: '',
                                                    data: {}
                                                }
                                                objerror.columnName = 'Company Contact Person Email';
                                                objerror.message = 'User already exist with same email ' + objuserelement.Email;

                                                objerror.data = objuserelement;
                                                filterMappingFormat.rejecteduser.push(objerror);
                                            }

                                        }
                                        else {
                                            //bulk update code

                                            var objUser = filteruser.filter(x => x.Code == objuserelement.Code)[0];

                                            if (objUser) {

                                                if (objCompany) {

                                                    //company reference should be same existing company
                                                    //check relationship between user and company

                                                    let objusercompany = filterUserCompany.filter(x => x.UserId == objUser.id && x.CompanyId == objCompany.id)[0];
                                                    let objusercompany2 = filterUserCompany.filter(x => x.UserId == objUser.id)[0];

                                                    if (!objusercompany2) {
                                                        let objuserelement = elementuser;
                                                        objuserelement.id = objUser.id;
                                                        filterMappingFormat.importeduser.push(objuserelement);
                                                    }
                                                    else if (objusercompany) {
                                                        let objuserelement = elementuser;
                                                        objuserelement.id = objUser.id;
                                                        filterMappingFormat.importeduser.push(objuserelement);
                                                    }
                                                    else {

                                                        let objerror =
                                                        {
                                                            message: '',
                                                            columnName: '',
                                                            data: {}
                                                        }

                                                        objerror.coumnName = 'CompanyCode';
                                                        objerror.message = 'User belong to another company ' + elementuser.CompanyCode;

                                                        objerror.data = elementuser;
                                                        filterMappingFormat.rejecteduser.push(objerror);
                                                    }

                                                }
                                                else {

                                                    var objNewCompany = filtercompany.filter(x => x.Code == elementuser.CompanyCode)[0];

                                                    if (!objNewCompany) {
                                                        let objerror =
                                                        {
                                                            message: '',
                                                            columnName: '',
                                                            data: {}
                                                        }
                                                        objerror.columnName = 'CompanyCode';
                                                        objerror.message = 'Company Code is invalid :' + elementuser.CompanyCode;
                                                        objerror.data = elementuser;
                                                        filterMappingFormat.rejecteduser.push(objerror);
                                                    } else {

                                                        let objuserelement = objUser.dataValues;

                                                        if (objuserelement.Email == elementuser.Email) {

                                                            objuserelement.FirstName = elementuser.FirstName;
                                                            objuserelement.LastName = elementuser.LastName;
                                                            objuserelement.MobileNo = elementuser.MobileNo;
                                                            objuserelement.IP = elementuser.IP;

                                                            filterMappingFormat.importeduser.push(objuserelement);
                                                        }

                                                        else {
                                                            let objerror =
                                                            {
                                                                message: '',
                                                                columnName: '',
                                                                data: {}
                                                            }
                                                            objerror.coumnName = 'CompanyCode';
                                                            objerror.message = 'Email not Associated with Code: ' + elementuser.Code;
                                                            objerror.data = objuserelement;
                                                            filterMappingFormat.rejecteduser.push(objerror);
                                                        }
                                                    }
                                                }

                                            }
                                            else {
                                                let objerror =
                                                {
                                                    message: '',
                                                    columnName: '',
                                                    data: {}
                                                }

                                                objerror.columnName = 'User Code';
                                                objerror.message = 'User not found of Code : ' + objuserelement.Code;
                                                objerror.data = objuserelement;
                                                filterMappingFormat.rejecteduser.push(objerror);
                                            }
                                        }

                                    }
                                });

                                // user bulk create or update code   

                                var createduser = await db.User.bulkCreate(filterMappingFormat.importeduser,
                                    {
                                        updateOnDuplicate: ["FirstName", "LastName", "Email", "Password", "hashedPassword", "salt", "IP", "CreatedAt", "CreatedBy", "ModifiedAt", "ModifiedBy", "Code"]
                                    }).then(async function (createduser) {
                                        if (createduser && createduser.length > 0) {
                                            let userids = createduser.map(x => { return x.id });

                                            let userRoles = await db.UserRole.findAll
                                                ({
                                                    where: { UserId: userids }
                                                });


                                            let userCompanies = await db.UserCompany.findAll
                                                ({
                                                    where: { UserId: userids }
                                                });

                                            createduser.forEach(user => {
                                                var tempUser = filterMappingFormat.importeduser.filter(x => x.Email == user.Email)[0];

                                                if (tempUser) {
                                                    user.CompanyCode = tempUser.CompanyCode;
                                                    user.CompanyName = tempUser.CompanyName;
                                                    user.CompanyId = tempUser.CompanyId;
                                                }

                                                jsonAffectedRecord.importeduser.push(user.dataValues);
                                            });

                                            let arrayUserRole = [];
                                            let arrayUserCompany = [];

                                            createduser.forEach(user => {

                                                var usercomp = null;

                                                if (userCompanies && userCompanies.length > 0) {
                                                    usercomp = userCompanies.filter(x => x.UserId == user.id)[0];
                                                }

                                                if (!usercomp) {
                                                    let objCompany = null;

                                                    if (user.CompanyCode) {
                                                        objCompany = filtercompany.filter(x => x.Code == user.CompanyCode)[0];
                                                    }
                                                    else {
                                                        if (importType == config.ImportType.CombineStuff) {
                                                            objCompany = jsonAffectedRecord.importedcompany.filter(x => x.Name == user.CompanyName && x.Code == user.CompanyCode)[0];
                                                        }
                                                    }

                                                    if (objCompany) {
                                                        let objuserCompany =
                                                        {
                                                            id: 0,
                                                            UserId: user.id,
                                                            CompanyId: objCompany.id,
                                                            CreatedAt: new Date(),
                                                            CreatedBy: 1,
                                                            ModifiedAt: new Date(),
                                                            ModifiedBy: 1
                                                        }
                                                        arrayUserCompany.push(objuserCompany);
                                                    }

                                                }

                                                //User Roles
                                                var userrole = null;
                                                if (userRoles && userRoles.length > 0) {
                                                    userrole = userRoles.filter(x => x.UserId == user.id)[0];
                                                }

                                                if (!userrole) {
                                                    let objuserRole =
                                                    {
                                                        id: 0,
                                                        UserId: user.id,
                                                        RoleId: 1,
                                                        CreatedAt: new Date(),
                                                        CreatedBy: 1,
                                                        ModifiedAt: new Date(),
                                                        ModifiedBy: 1
                                                    }
                                                    arrayUserRole.push(objuserRole);
                                                }
                                            });

                                            // user role mapping

                                            var userrolemapping = await db.UserRole.bulkCreate(arrayUserRole,
                                                {
                                                    updateOnDuplicate: ["UserId", "RoleId", "CreatedAt", "CreatedBy", "ModifiedAt", "ModifiedBy"]
                                                }).then(function (rolemapping) {
                                                    return;
                                                });

                                            // user company mapping
                                            var usercompanymapping = await db.UserCompany.bulkCreate(arrayUserCompany,
                                                {
                                                    updateOnDuplicate: ["UserId", "CompanyId", "CreatedAt", "CreatedBy", "ModifiedAt", "ModifiedBy"]
                                                }).then(function (companymapping) {

                                                });
                                        }
                                    });
                            }

                            // Vulnerabilities
                            if (jsonMappingFormat.importedVulnerabilities && jsonMappingFormat.importedVulnerabilities.length > 0) {

                                let arryVulnerabilitiesCode = [];
                                let arryPenTestCode = [];

                                var filterVulnerabilities = [];
                                var filterPentest = [];

                                arryVulnerabilitiesCode = jsonMappingFormat.importedVulnerabilities.filter(x => x.Code != undefined);

                                arryPenTestCode = jsonMappingFormat.importedVulnerabilities.filter(x => x.PentestCode != undefined);


                                if (arryVulnerabilitiesCode && arryVulnerabilitiesCode.length > 0) {
                                    arryVulnerabilitiesCode = arryVulnerabilitiesCode.map(x => { return x.Code });

                                    filterVulnerabilities = await db.Vulnerabilities.findAll
                                        ({
                                            where: { IsActive: 1, Code: [arryVulnerabilitiesCode] }
                                        });
                                }

                                if (arryPenTestCode && arryPenTestCode.length > 0) {
                                    arryPenTestCode = arryPenTestCode.map(x => { return x.PentestCode });

                                    filterPentest = await db.PenTest.findAll
                                        ({
                                            where: { IsActive: 1, Code: [arryPenTestCode] }
                                        });
                                }

                                let VulnerabilitiesLastCode = await helper.GetLatestCode(config.Tables.Vulnerabilities);

                                jsonMappingFormat.importedVulnerabilities.forEach(elementVulnerabilities => {
                                    if (elementVulnerabilities.Name) {

                                        if (!elementVulnerabilities.Code) {
                                            if (elementVulnerabilities.PentestCode) {
                                                let objelementVulnerabilitieselemement = elementVulnerabilities;

                                                if (!VulnerabilitiesLastCode) {
                                                    objelementVulnerabilitieselemement.Code = helper.GenerateCode('NEW', config.TablePrefix.Vulnerabilities);
                                                    VulnerabilitiesLastCode = helper.GenerateCode(objelementVulnerabilitieselemement.Code, config.TablePrefix.Vulnerabilities);
                                                }
                                                else {
                                                    objelementVulnerabilitieselemement.Code = VulnerabilitiesLastCode;
                                                    VulnerabilitiesLastCode = helper.GenerateCode(VulnerabilitiesLastCode, config.TablePrefix.Vulnerabilities);
                                                }

                                                let objPentest = filterPentest.filter(x => x.Code == objelementVulnerabilitieselemement.PentestCode)[0];

                                                if (objPentest) {
                                                    objelementVulnerabilitieselemement.PenTestId = objPentest.id;
                                                    filterMappingFormat.importedVulnerabilities.push(objelementVulnerabilitieselemement);
                                                }
                                                else {
                                                    let objerror =
                                                    {
                                                        message: '',
                                                        columnName: '',
                                                        data: {}
                                                    }
                                                    objerror.columnName = 'Pentest Code';
                                                    objerror.message = 'Pentest not found of Code : ' + objelementVulnerabilitieselemement.PentestCode;
                                                    objerror.data = objelementVulnerabilitieselemement;
                                                    filterMappingFormat.rejectedVulnerabilities.push(objerror);
                                                }
                                            }
                                            else {

                                                let objerror =
                                                {
                                                    message: '',
                                                    columnName: '',
                                                    data: {}
                                                }
                                                objerror.columnName = 'Pentest Code';
                                                objerror.message = 'Pentest Code Required during Vulnerability Creation';
                                                objerror.data = elementVulnerabilities;
                                                filterMappingFormat.rejectedVulnerabilities.push(objerror);
                                            }

                                        }
                                        else {
                                            var objVulnerabilities = filterVulnerabilities.filter(x => x.Code == elementVulnerabilities.Code)[0];

                                            if (objVulnerabilities) {
                                                let objVulnerabilitieselement = elementVulnerabilities;
                                                objVulnerabilitieselement.id = objVulnerabilities.id;
                                                filterMappingFormat.importedVulnerabilities.push(objVulnerabilitieselement);

                                            }

                                            else {

                                                let objerror =
                                                {
                                                    message: '',
                                                    columnName: '',
                                                    data: {}
                                                }
                                                objerror.columnName = 'Vulnerability Code';
                                                objerror.message = 'Vulnerabilities not found of Code : ' + elementVulnerabilities.Code;
                                                objerror.data = elementVulnerabilities;
                                                filterMappingFormat.rejectedVulnerabilities.push(objerror);
                                            }
                                        }
                                    }


                                });

                                // Vulnerabilities bulk create or update code     

                                if (filterMappingFormat.importedVulnerabilities && filterMappingFormat.importedVulnerabilities.length > 0) {
                                    var createdVulnerabilities = await db.Vulnerabilities.bulkCreate(filterMappingFormat.importedVulnerabilities,
                                        // { returning: true },
                                        {
                                            updateOnDuplicate: ["PenTestId", "Name", "Facing", "Risk", "Complexity", "Effort", "SystemsDesc", "RiskDesc", "VulnSolution", "IssueType", "CreatedBy", "ModifiedAt", "ModifiedBy", "Code"]
                                        }).then(async function (createdVulnerabilities) {
                                            if (createdVulnerabilities && createdVulnerabilities.length > 0) {
                                                createdVulnerabilities.forEach(vul => {
                                                    jsonAffectedRecord.importedVulnerabilities.push(vul.dataValues);
                                                });

                                                // language translation approach

                                                var translationResponse = await Mappinglanguagetranslation(jsonAffectedRecord.importedVulnerabilities);

                                            }
                                        });
                                }
                            }

                            // error server side add in same object
                            if (jsonMappingFormat.rejectedcompany.length > 0) {
                                jsonMappingFormat.rejectedcompany.forEach(error => {
                                    filterMappingFormat.rejectedcompany.push(error);
                                })
                            }

                            if (jsonMappingFormat.rejectedsystem.length > 0) {
                                jsonMappingFormat.rejectedsystem.forEach(error => {
                                    filterMappingFormat.rejectedsystem.push(error);
                                })
                            }

                            if (jsonMappingFormat.rejectedasset.length > 0) {
                                jsonMappingFormat.rejectedasset.forEach(error => {
                                    filterMappingFormat.rejectedasset.push(error);
                                })
                            }

                            if (jsonMappingFormat.rejecteduser.length > 0) {
                                jsonMappingFormat.rejecteduser.forEach(error => {
                                    filterMappingFormat.rejecteduser.push(error);
                                })
                            }

                            if (jsonMappingFormat.rejectedVulnerabilities.length) {
                                jsonMappingFormat.rejectedVulnerabilities.forEach(error => {
                                    filterMappingFormat.rejectedVulnerabilities.push(error);
                                })
                            }



                            // error db side add in same object
                            if (filterMappingFormat.rejectedcompany.length > 0) {
                                filterMappingFormat.rejectedcompany.forEach(error => {
                                    jsonAffectedRecord.rejectedcompany.push(error);
                                })
                            }

                            if (filterMappingFormat.rejectedsystem.length > 0) {
                                filterMappingFormat.rejectedsystem.forEach(error => {
                                    jsonAffectedRecord.rejectedsystem.push(error);
                                })
                            }

                            if (filterMappingFormat.rejectedasset.length > 0) {
                                filterMappingFormat.rejectedasset.forEach(error => {
                                    jsonAffectedRecord.rejectedasset.push(error);
                                })
                            }

                            if (filterMappingFormat.rejecteduser.length > 0) {
                                filterMappingFormat.rejecteduser.forEach(error => {
                                    jsonAffectedRecord.rejecteduser.push(error);
                                })
                            }

                            if (filterMappingFormat.rejectedVulnerabilities.length > 0) {
                                filterMappingFormat.rejectedVulnerabilities.forEach(error => {
                                    jsonAffectedRecord.rejectedVulnerabilities.push(error);
                                })
                            }

                            var filename = req.file.filename;
                            var Logger = await ImportedRejectedLogger(importType, filename, filterMappingFormat, res);

                            //file removed
                            fs.unlinkSync(destinationPath + '/' + req.file.filename);

                            return res.json
                                ({
                                    status: true,
                                    message: 'Imported record details',
                                    data: jsonAffectedRecord
                                });

                        }).catch((ex

                        ) => {
                            console.log('errorrrrrrrrrrr', ex);
                        })
                    }
                }
                else {
                    console.log('Invalid file format !');
                    return res.status(500).send({ status: 0, message: 'Invalid file format !', data: err });
                }

            }
            else {
                console.log('Please select file !');
                return res.status(500).send({ status: 0, message: 'Please select file !', data: err });
            }
        });

    } catch (err) {
        console.log(err);
        return res.status(500).send({ status: 0, message: 'Bulk Import Failed', data: err });
    }
}