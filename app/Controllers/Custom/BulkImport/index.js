'use strict';

var express = require('express');
var BulkImportcontroller  = require('./bulkimport.controller');
var router = express.Router();
var auth = require('../../../Auth/auth.service');

router.post('/importbulkdata', BulkImportcontroller.importBulkData);
// router.post('/importVulnerabilitiesData', BulkImportcontroller.importVulnerabilitiesData);

module.exports = router;