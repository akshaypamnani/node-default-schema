//var mongoose = require("mongoose");
const mysql = require('mysql');
var config = require('../../../config/config.js');
const Cryptr = require('cryptr');
const cryptr = new Cryptr('R@@tSeC');
//const User = require('../../../models/user');

var jwt = require('jsonwebtoken');
const SECRET_KEY = "secretkey12345";

const db = require('../../../models');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
var sequelize = new Sequelize(config.DB.Livedatabase, config.DB.user, config.DB.password, {
    host: config.DB.host,
    dialect: "mysql",
    logging: function () { },
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },
});

exports.GetDashboardPentests = (req, res) => {
    db.PenTest.findAll({
        // include: [{ all: true, nested: false }],
        include: [db.LanguageTranslations, db.Company, {
            model: db.Vulnerabilities,
            include: [db.LanguageTranslations,
            {
                model: db.VulnerabilityAsset,
                include: [{
                    model: db.System,
                    where: {
                        IsActive: true
                    }
                }]
            }, {
                model: db.PenTestCheckListStatus,
                include: [{
                    model: db.PenTestChecklist
                }]
            }],
            // where: {
            //     IsActive: true
            // }
        }],
        where: {
            // IsPublished: 1,
            IsActive: true
        }
    }).then(PentestData => {
        var Vulnerabilities = [];
        if (req.body.CompanyId) {
            PentestData = PentestData.filter(x => x.CompanyId == req.body.CompanyId);
        }

        if (req.body.PentestId) {
            PentestData = PentestData.filter(x => x.id == req.body.PentestId);
        }

        if (req.body.AssetId) {
            PentestData = PentestData.filter(function (pntst) {
                var vulnerabilities = pntst.Vulnerabilities.filter(function (vuln) {
                    var vulnerabilityAssets = vuln.VulnerabilityAssets.filter(x => x.AssetId == req.body.AssetId);
                    if (vulnerabilityAssets && vulnerabilityAssets.length > 0) {
                        return vuln;
                    }
                });

                console.log('test', vulnerabilities.length);
                if (vulnerabilities && vulnerabilities.length > 0) {
                    return pntst;
                }
            });
        }

        console.log("PentestData.", PentestData);

        if (req.body.daterange && req.body.daterange.from && req.body.daterange.to) {
            PentestData = PentestData.filter(function (pentest) {
                console.log("from..", new Date(req.body.daterange.from));
                console.log("to..", new Date(req.body.daterange.to));
                var date = new Date(pentest.DateofTest);
                console.log("date..", date);
                return (date >= new Date(req.body.daterange.from) && date <= new Date(req.body.daterange.to));
            });
        }

        if (PentestData && PentestData.length > 0) {
            PentestData.forEach(function (pentest) {
                if (pentest.Vulnerabilities.length > 0) {
                    pentest.Vulnerabilities.forEach(function (vulnerability) {
                        if (vulnerability && vulnerability.IsActive == true) {
                            if (req.body.AssetId) {
                                var vulnerabilityAssets = vulnerability.VulnerabilityAssets.filter(x => x.AssetId == req.body.AssetId);
                                if (vulnerabilityAssets && vulnerabilityAssets.length > 0) {
                                    Vulnerabilities.push(vulnerability);
                                }
                            } else {
                                Vulnerabilities.push(vulnerability);
                            }
                        }
                    });
                }

            }, this);
        }

        return res.status(200).send({ status: 1, message: 'Success', data: { PenTests: PentestData, Vulnerabilities: Vulnerabilities } });

    }).catch(err => {
        return res.status(200).send({ status: 0, message: 'Failed', data: err });
    });
}

exports.GetDashboardclientPentests = (req, res) => {

    var token = req.headers['authorization'];
    token = token.toString().substring(7);
    if (!token)
        return res.status(403).send({ auth: false, message: 'No token provided.' });
    jwt.verify(token, SECRET_KEY, function (err, decoded) {
        if (err) {
            return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
        }

        UserData = {
            id: decoded.id,
            companies: decoded.companies
        };

        console.log('UserInfoUserInfo', UserData);
        var companyId = 0;
        if (UserData.companies && UserData.companies.length > 0) {
            companyId = UserData.companies[0].CompanyId;
        }

        db.PenTest.findAll({
            include: [db.LanguageTranslations, db.Company, {
                model: db.Vulnerabilities,
                include: [db.LanguageTranslations,
                {
                    model: db.VulnerabilityAsset,
                    include: [{
                        model: db.System,
                        where: {
                            IsActive: true
                        }
                    }]
                }, {
                    model: db.PenTestCheckListStatus,
                    include: [{
                        model: db.PenTestChecklist
                    }]
                }],
                // where: {
                //     IsActive: true
                // }
            }],
            where: {
                // IsPublished: 1,
                CompanyId: companyId,
                IsActive: true
            }
        }).then(PentestData => {
            var Vulnerabilities = [];
            if (req.body.CompanyId) {
                PentestData = PentestData.filter(x => x.CompanyId == req.body.CompanyId);
            }

            if (req.body.PentestId) {
                PentestData = PentestData.filter(x => x.id == req.body.PentestId);
            }

            if (req.body.AssetId) {
                PentestData = PentestData.filter(function (pntst) {
                    var vulnerabilities = pntst.Vulnerabilities.filter(function (vuln) {
                        var vulnerabilityAssets = vuln.VulnerabilityAssets.filter(x => x.AssetId == req.body.AssetId);
                        if (vulnerabilityAssets && vulnerabilityAssets.length > 0) {
                            return vuln;
                        }
                    });

                    console.log('test', vulnerabilities.length);
                    if (vulnerabilities && vulnerabilities.length > 0) {
                        return pntst;
                    }
                });
            }

            console.log("PentestData.", PentestData);

            if (req.body.daterange && req.body.daterange.from && req.body.daterange.to) {
                PentestData = PentestData.filter(function (pentest) {
                    console.log("from..", new Date(req.body.daterange.from));
                    console.log("to..", new Date(req.body.daterange.to));
                    var date = new Date(pentest.DateofTest);
                    console.log("date..", date);
                    return (date >= new Date(req.body.daterange.from) && date <= new Date(req.body.daterange.to));
                });
            }

            if (PentestData && PentestData.length > 0) {
                PentestData.forEach(function (pentest) {
                    if (pentest.Vulnerabilities.length > 0) {
                        pentest.Vulnerabilities.forEach(function (vulnerability) {
                            if (vulnerability && vulnerability.IsActive == true) {
                                if (req.body.AssetId) {
                                    var vulnerabilityAssets = vulnerability.VulnerabilityAssets.filter(x => x.AssetId == req.body.AssetId);
                                    if (vulnerabilityAssets && vulnerabilityAssets.length > 0) {
                                        Vulnerabilities.push(vulnerability);
                                    }
                                } else {
                                    Vulnerabilities.push(vulnerability);
                                }
                            }
                        });
                    }

                }, this);
            }

            return res.status(200).send({ status: 1, message: 'Success', data: { PenTests: PentestData, Vulnerabilities: Vulnerabilities } });

        }).catch(err => {
            return res.status(200).send({ status: 0, message: 'Failed', data: err });
        });

    });
}

exports.GetClientDashboardPentests = (req, res) => {
    db.PenTest.findAll({
        // include: [{ all: true, nested: false }],
        include: [db.LanguageTranslations, db.Company, {
            model: db.Vulnerabilities,
            include: [db.LanguageTranslations,
            {
                model: db.VulnerabilityAsset,
                include: [{
                    model: db.System,
                    where: {
                        IsActive: true
                    }
                }]
            }, {
                model: db.PenTestCheckListStatus,
                include: [{
                    model: db.PenTestChecklist
                }]
            }],
            // where: {
            //     IsActive: true
            // }
        }],
        where: {
            // IsPublished: 1,
            IsActive: true
        }
    }).then(PentestData => {
        var Vulnerabilities = [];
        if (req.body.CompanyId) {
            PentestData = PentestData.filter(x => x.CompanyId == req.body.CompanyId);
        }

        if (req.body.PentestId) {
            PentestData = PentestData.filter(x => x.id == req.body.PentestId);
        }

        if (req.body.AssetId) {
            PentestData = PentestData.filter(function (pntst) {
                var vulnerabilities = pntst.Vulnerabilities.filter(function (vuln) {
                    var vulnerabilityAssets = vuln.VulnerabilityAssets.filter(x => x.AssetId == req.body.AssetId);
                    if (vulnerabilityAssets && vulnerabilityAssets.length > 0) {
                        return vuln;
                    }
                });

                console.log('test', vulnerabilities.length);
                if (vulnerabilities && vulnerabilities.length > 0) {
                    return pntst;
                }
            });
        }

        console.log("PentestData.", PentestData);

        if (req.body.daterange && req.body.daterange.from && req.body.daterange.to) {
            PentestData = PentestData.filter(function (pentest) {
                console.log("from..", new Date(req.body.daterange.from));
                console.log("to..", new Date(req.body.daterange.to));
                var date = new Date(pentest.DateofTest);
                console.log("date..", date);
                return (date >= new Date(req.body.daterange.from) && date <= new Date(req.body.daterange.to));
            });
        }

        if (PentestData && PentestData.length > 0) {
            PentestData.forEach(function (pentest) {
                if (pentest.IsPublished == 1 && pentest.RecurrenceType != 3 && pentest.Vulnerabilities.length > 0) {
                    pentest.Vulnerabilities.forEach(function (vulnerability) {
                        if (vulnerability && vulnerability.IsActive == true) {
                            if (req.body.AssetId) {
                                var vulnerabilityAssets = vulnerability.VulnerabilityAssets.filter(x => x.AssetId == req.body.AssetId);
                                if (vulnerabilityAssets && vulnerabilityAssets.length > 0) {
                                    Vulnerabilities.push(vulnerability);
                                }
                            } else {
                                Vulnerabilities.push(vulnerability);
                            }
                        }
                    });
                }

            }, this);
        }

        return res.status(200).send({ status: 1, message: 'Success', data: { PenTests: PentestData, Vulnerabilities: Vulnerabilities } });

    }).catch(err => {
        return res.status(200).send({ status: 0, message: 'Failed', data: err });
    });
}

exports.TopFiveCompanies = (req, res) => {
    sequelize.query("SELECT tpt.`CreatedAt`, tc.id, tc.Name, tc.`Logo` FROM `tblCompany` tc left join tblPenTest tpt on tc.id = tpt.Companyid where tc.Isactive = true group by tc.id, tc.Name Order by tpt.`CreatedAt` desc limit 5", { type: Sequelize.QueryTypes.SELECT })
        .then(data => {
            console.log("success")
            console.log(data)
            return res.status(200).send({ status: 1, message: 'Success', data: data });
        }).catch(err => {
            return res.status(500).send({ status: 0, message: 'Failed', err });
        });
}

exports.DashboardTrends = (req, res) => {
    var object = req.body;
    if (object) {
        var query = "SELECT count(*) as total, `IssueType`, ";

        if (object.TrendsFilter && (object.TrendsFilter == 2 || object.TrendsFilter == 3)) {
            //Month -- Week
            query = query + "DAYNAME(tv.CreatedAt) as day, DATE_FORMAT(tv.CreatedAt, '%d-%b') as 'DateFormat' ";
        } else {
            //Year
            query = query + "MONTHNAME(tv.CreatedAt) as month, DATE_FORMAT(tv.CreatedAt, '%b-%y') as 'DateFormat' ";
        }

        query = query + "FROM `tblVulnerabilities` tv left join tblPenTest tp on tv.`PenTestId` = tp.Id where"

        if (object.pentestid) {
            query = query + " tv.PenTestId=" + object.pentestid + " and";
        }
        if (object.companyid) {
            query = query + " tp.CompanyId=" + object.companyid + " and";
        }
        if (object.currentdate) {
            query = query + " tv.CreatedAt <='" + object.currentdate + "' and ";
            if (object.TrendsFilter && (object.TrendsFilter == 2 || object.TrendsFilter == 3)) {
                query = query + "tv.CreatedAt >= CAST(DATE_ADD('" + object.currentdate + "', INTERVAL ";
                if (object.TrendsFilter == 2) {
                    //Month
                    query = query + "-30"
                } else {
                    //Week
                    query = query + "-7"
                }
                query = query + " DAY) AS CHAR) and tv.IsActive = 1 and tp.IsActive = 1 group by DAYNAME(tv.CreatedAt), `IssueType`"
            } else {
                //Year
                query = query + "tv.CreatedAt >= CAST(DATE_ADD('" + object.currentdate + "', INTERVAL -12 MONTH) AS CHAR) and tv.IsActive = 1 and tp.IsActive = 1 group by MONTHNAME(tv.CreatedAt), `IssueType`"
            }
        }
        console.log('query', query);
        sequelize.query(query, { type: Sequelize.QueryTypes.SELECT })
            .then(data => {
                console.log("success")
                console.log(data)
                return res.status(200).send({ status: 1, message: 'Success', data: data });
            }).catch(err => {
                return res.status(500).send({ status: 0, message: 'Failed', err });
            });
    } else {
        return res.status(500).send({ status: 0, message: 'Failed', err });
    }
}

exports.ClientDashboardTrends = (req, res) => {
    var object = req.body;
    if (object) {

        var query = "SELECT count(*) as total, `IssueType`, MONTHNAME(tv.CreatedAt) as month, DATE_FORMAT(tv.CreatedAt, '%b-%y') as 'MonthYearFormat' FROM `tblVulnerabilities` tv left join tblPenTest tp on tv.`PenTestId` = tp.Id where";

        if (object.pentestid) {
            query = query + " tv.PenTestId=" + object.pentestid + " and";
        }
        if (object.companyid) {
            query = query + " tp.CompanyId=" + object.companyid + " and";
        }
        if (object.currentdate) {
            query = query + " tv.CreatedAt <='" + object.currentdate + "' and tv.CreatedAt >= CAST(DATE_ADD('" + object.currentdate + "', INTERVAL -12 MONTH) AS CHAR) and tp.`RecurrenceType` in (1,2) and tv.IsActive = 1 and tp.IsActive = 1 group by MONTHNAME(tv.CreatedAt), `IssueType`"
        }
        console.log('query', query);
        sequelize.query(query, { type: Sequelize.QueryTypes.SELECT })
            .then(data => {
                console.log("success")
                console.log(data)
                return res.status(200).send({ status: 1, message: 'Success', data: data });
            }).catch(err => {
                return res.status(500).send({ status: 0, message: 'Failed', err });
            });
    } else {
        return res.status(500).send({ status: 0, message: 'Failed', err });
    }
}

exports.GetPentestByUserID = (req, res) => {
    console.log("GetPentestByUserID....");
    db.PenTest.findAll({
        // include: [{ all: true, nested: false }],
        include: [db.LanguageTranslations, db.Company, {
            model: db.Vulnerabilities,
            include: [db.LanguageTranslations,
            {
                model: db.VulnerabilityAsset,
                include: [db.Asset, {
                    model: db.System,
                    where: {
                        IsActive: true
                    }
                }]
            }, {
                model: db.PenTestCheckListStatus,
                include: [{
                    model: db.PenTestChecklist
                }]
            }],
            // where: {
            //     IsActive: true
            // }
        }],
        where: {
            // [Op.or]: {
            //     [Op.and]: {
            //         RecurrenceType: [1, 2],
            //         IsPublished: 1,
            //     },
            //     RecurrenceType: 3
            // },
            IsPublished: 1,
            IsActive: true
        }
    }).then(PentestData => {
        var companies = [];
        var Vulnerabilities = [];
        var object = req.body;
        if (object && object.UserId) {
            console.log("object.UserId", object.UserId);
            db.UserCompany.findAll({
                where: {
                    UserId: object.UserId
                }
            }).then(UserCompanies => {
                console.log("UserCompanies..", UserCompanies.length);
                if (UserCompanies && UserCompanies.length > 0) {
                    companies.push(UserCompanies[0].CompanyId);
                }

                if (companies.length > 0) {
                    companies = companies.filter(onlyUnique);
                    console.log("companies", companies);
                    var UserPentests = [];
                    PentestData.forEach(function (pentest) {
                        if (companies.includes(pentest.CompanyId)) {
                            UserPentests.push(pentest);
                        }
                    });
                    console.log("UserPentests", UserPentests.length);
                    PentestData = UserPentests;
                    // if (UserPentests.length > 0) {
                    //     PentestData = UserPentests;
                    // }

                    if (PentestData && PentestData.length > 0) {
                        PentestData.forEach(function (pentest) {
                            if (pentest.Vulnerabilities.length > 0) {
                                pentest.Vulnerabilities.forEach(function (Vulnerability) {
                                    if (Vulnerability && Vulnerability.IsActive == true) {
                                        Vulnerabilities.push(Vulnerability);
                                    }
                                });
                            }

                        }, this);
                    }

                    return res.status(200).send({ status: 1, message: 'Success', data: { PenTests: PentestData, Vulnerabilities: Vulnerabilities } });
                } else {
                    return res.status(200).send({ status: 1, message: 'Success', data: { PenTests: [], Vulnerabilities: [] } });
                }

            });
        }

    }).catch(err => {
        return res.status(200).send({ status: 0, message: 'Failed', data: err });
    });
}

exports.GetUserwiseCheckPentest = (req, res) => {
    const decryptedID = cryptr.decrypt(req.params.id);
    console.log('decryptedID', decryptedID);
    sequelize.query("SELECT Distinct tp.Id FROM `tblPenTest` tp left join tblUserCompany tuc on tp.CompanyId = (select CompanyId from tblUserCompany where UserId=" + decryptedID + " LIMIT 0 , 1) where tp.RecurrenceType in (1,2) and tp.IsActive=1 and tuc.UserId=" + decryptedID, { type: Sequelize.QueryTypes.SELECT })
        .then(data => {
            console.log("success")
            console.log(data)
            return res.status(200).send({ status: 1, message: 'Success', data: data });
        }).catch(err => {
            return res.status(500).send({ status: 0, message: 'Failed', err });
        });
}

function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}