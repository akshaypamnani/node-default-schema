'use strict';

var express = require('express');
var controller = require('./dashboard.controller');
var router = express.Router();
// var auth = require('../../Auth/auth.service');

// router.post('/pivot/:collection/:id', auth.isAuthenticated(), controller.Updatepivot);GetClientDashboardPentests
router.post('/GetDashboardPentests',  controller.GetDashboardPentests);
router.post('/GetDashboardclientPentests',  controller.GetDashboardclientPentests);
router.post('/GetClientDashboardPentests',  controller.GetClientDashboardPentests);
router.post('/GetPentestByUserID',  controller.GetPentestByUserID);
router.get('/TopFiveCompanies',  controller.TopFiveCompanies);
router.get('/CheckPentests/:id',  controller.GetUserwiseCheckPentest);
router.post('/dashboardtrends',  controller.DashboardTrends);
router.post('/clientdashboardtrends',  controller.ClientDashboardTrends);

module.exports = router;
