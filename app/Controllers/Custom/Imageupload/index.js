
var express = require('express');
var router = express.Router();
const multer = require('multer');
var path = require('path');
var fs = require('fs');

const jwt = require('jsonwebtoken');
const SECRET_KEY = "secretkey12345";

var storage = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, path.join(__dirname, '../../../uploads'))
    },
    filename: function (req, file, callback) {
        callback(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }
});

var signstorage = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, path.join(__dirname, '../../../uploads/signatures'))
    },
    filename: function (req, file, callback) {
        callback(null, file.fieldname + '-' + Date.now() + '.png')
    }
})

const upload = multer({ storage: storage, dest: path.join(__dirname, '../../../uploads') });
const signupload = multer({ storage: signstorage, dest: path.join(__dirname, '../../../uploads/signatures') });


router.post('/imageupload', upload.single('file'), (req, res) => {
    console.log(`new upload = ${req.file.filename}\n`);
    res.json({ status: 1, message: 'Uploaded Successfully!!', data: { fileName: req.file.filename } });
});

router.post('/signatureupload', signupload.single('file'), (req, res) => {
    console.log(`new upload = ${req.file.filename}\n`);
    res.json({ status: 1, message: 'Uploaded Successfully!!', data: { fileName: req.file.filename, filesize: req.file.size } });
});

router.get('/getlogoimage/:filename', (req, res) => {
    var filepath = path.join(__dirname, '../../../uploads/', req.params.filename);
    // var filepath = path.join('uploads/', req.params.filename);
    console.log(filepath);
    fs.stat(filepath, function (err, stat) {
        if (err == null) {
            res.setHeader('content-type', 'image/*');
            fs.createReadStream(filepath).pipe(res)

        } else if (err.code == 'ENOENT') {
            var nfilepath = path.join(__dirname, '../../../uploads/Default/avtar');
            console.log("file does not exist");
            fs.stat(nfilepath, function (err, stat) {
                if (err == null) {
                    res.setHeader('content-type', 'image/*');
                    fs.createReadStream(nfilepath).pipe(res)

                }
            });
            // return res.status(200).send({
            //     status: 0,
            //     message: "file does not exist " + req.params.filename
            // });
            // res.status(404).send();
        } else {
            console.log('Some other error: ', err.code);
            var nfilepath = path.join(__dirname, '../../../uploads/avtar');
            console.log("file does not exist");
            fs.stat(nfilepath, function (err, stat) {
                if (err == null) {
                    res.setHeader('content-type', 'image/*');
                    fs.createReadStream(nfilepath).pipe(res)

                }
            });
        }
    });
});

router.get('/getimage', (req, res) => {
    const token = req.query.token;
    if (!token) {
        return res.status(200).json({ status: 0, message: 'token not available' });
    }

    jwt.verify(token, SECRET_KEY, function (err, decoded) {
        if (err) {
            return res.status(200).send({ status: 0, message: 'Failed to authenticate token.' });
        } else {
            var fileName = req.query.filename;
            if (fileName) {
            } else {
                var fileName = 'Default/avtar.png';
            }
            var filepath = path.join(__dirname, '../../../uploads/', req.query.filename);
            // var filepath = path.join('uploads/', req.params.filename);
            console.log(filepath);
            fs.stat(filepath, function (err, stat) {
                if (err == null) {
                    res.setHeader('content-type', 'image/*');
                    res.setHeader('Content-Disposition', 'attachment; filename=' + fileName);
                    fs.createReadStream(filepath).pipe(res)

                } else if (err.code == 'ENOENT') {
                    var nfilepath = path.join(__dirname, '../../../uploads/Default/avtar.png');
                    console.log("file does not exist");
                    fs.stat(nfilepath, function (err, stat) {
                        if (err == null) {
                            res.setHeader('content-type', 'image/*');
                            res.setHeader('Content-Disposition', 'attachment; filename=Default.png');
                            fs.createReadStream(nfilepath).pipe(res)

                        }
                    });
                    // return res.status(200).send({
                    //     status: 0,
                    //     message: "file does not exist " + req.params.filename
                    // });
                    // res.status(404).send();
                } else {
                    console.log('Some other error: ', err.code);
                    var nfilepath = path.join(__dirname, '../../../uploads/avtar.png');
                    console.log("file does not exist");
                    fs.stat(nfilepath, function (err, stat) {
                        if (err == null) {
                            res.setHeader('content-type', 'image/*');
                            fs.createReadStream(nfilepath).pipe(res)

                        }
                    });
                }
            });
        }
        // if everything good, save to request for use in other routes
        // next();
    });
});

router.get('/getpdf/:id', (req, res) => {

    const token = req.query.token;
    if (!token) {
        return res.status(200).json({ status: 0, message: 'token not available' });
    }

    jwt.verify(token, SECRET_KEY, function (err, decoded) {
        if (err) {
            return res.status(200).send({ status: 0, message: 'Failed to authenticate token.' });
        } else {
            var filepath = path.join(__dirname, '../../../../Downloads/Pentests/Penetratierapport_' + req.params.id + '.pdf');
            // var filepath = path.join('uploads/', req.params.filename);
            console.log(filepath);
            fs.stat(filepath, function (err, stat) {
                if (!err) {
                    res.setHeader('content-type', 'application/pdf');
                    res.setHeader('Content-Disposition', 'attachment; filename=Penetratierapport_' + req.params.id + '.pdf');
                    var readStream = fs.createReadStream(filepath);
                    readStream.on('open', function () {
                        // This just pipes the read stream to the response object (which goes to the client)
                        readStream.pipe(res);
                    });
                    readStream.on('error', function (err) {
                        return res.status(500).send({ status: 0, message: "pdfs failed", data: err });
                    });
                } else {
                    return res.status(500).send({ status: 0, message: "pdf failed", data: err });
                }
            });
        }
    });
});

router.get('/getsamplefiles', (req, res) => {
    const token = req.query.token;
    if (!token) {
        return res.status(200).json({ status: 0, message: 'token not available' });
    }

    jwt.verify(token, SECRET_KEY, function (err, decoded) {
        if (err) {
            return res.status(200).send({ status: 0, message: 'Failed to authenticate token.' });
        } else {
            var fileName = req.query.filename;
            if (fileName) {
            } else {
                var fileName = 'Default/avtar.png';
            }
            var filepath = path.join(__dirname, '../../../uploads/SampleImportfiles/', req.query.filename);
            // var filepath = path.join('uploads/', req.params.filename);
            console.log(filepath);
            fs.stat(filepath, function (err, stat) {
                if (err == null) {
                    res.setHeader('content-type', 'image/*');
                    res.setHeader('Content-Disposition', 'attachment; filename=' + fileName);
                    fs.createReadStream(filepath).pipe(res)

                } else if (err.code == 'ENOENT') {
                    var nfilepath = path.join(__dirname, '../../../uploads/Default/avtar.png');
                    console.log("file does not exist");
                    fs.stat(nfilepath, function (err, stat) {
                        if (err == null) {
                            res.setHeader('content-type', 'image/*');
                            res.setHeader('Content-Disposition', 'attachment; filename=Default.png');
                            fs.createReadStream(nfilepath).pipe(res)

                        }
                    });
                    // return res.status(200).send({
                    //     status: 0,
                    //     message: "file does not exist " + req.params.filename
                    // });
                    // res.status(404).send();
                } else {
                    console.log('Some other error: ', err.code);
                    var nfilepath = path.join(__dirname, '../../../uploads/Default/avtar.png');
                    console.log("file does not exist");
                    fs.stat(nfilepath, function (err, stat) {
                        if (err == null) {
                            res.setHeader('content-type', 'image/*');
                            fs.createReadStream(nfilepath).pipe(res)

                        }
                    });
                }
            });
        }
        // if everything good, save to request for use in other routes
        // next();
    });
});

module.exports = router;