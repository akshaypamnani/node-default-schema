'use strict';

var express = require('express');
var Systemcontroller = require('./system.controller');
var router = express.Router();
var auth = require('../../../Auth/auth.service');

//router.get('/getClientuser', usercontroller.getClientuser); 

//router.get('/getRootSecuser', usercontroller.getRootSecuser); 
//router.post('/UpdateUser/:id', usercontroller.updateUser);
router.put('/AddOrEditSystem/',auth.isAuthenticated(), Systemcontroller.AddOrEditSystem);

//router.post('/EditRootSecuser/', usercontroller.EditRootSecuser);
module.exports = router;