const mysql = require('mysql');
const db = require('../../../models');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

exports.AddOrEditSystem = (req, res) => {
    var lang = req.headers['lang'];
    console.log("System controller....create")
    console.log('req.body', req.body);
    const object = req.body;
    if (object.id && object.id != 0) {
        object.ModifiedAt = new Date();
        object.ModifiedBy = 1;
        db.System.findByPk(object.id).then(systemdata => {
            systemdata.update(object).then(data => {
                console.log("System updated..", data.id);
                req.body.Hosts.forEach(function (Host) {
                    Host.SystemId = data.id;
                    if (Host.id && Host.id != 0) {
                        console.log("Exsist", Host);
                        db.Host.findByPk(Host.id).then(data => {
                            if (data) {
                                data.ModifiedAt = new Date();
                                data.ModifiedBy = 1;
                                data.update(Host);
                            }
                            else {
                                console.log("Host not found with id :", Host.id);
                            }

                        }).catch(err => {
                            console.log(err);
                        });

                    }
                    else {
                        console.log("new", Host);
                        db.Host.create(Host).then(data => {
                            console.log("Host Created..", data);
                        }).catch(err => {
                            console.log(err);
                        });
                    }
                });
            })

            var message = '';
            if (lang == 'nl') {
                message = 'Systeem geupdate succesvol!';
            } else {
                message = 'System Updated successfully!';
            }

            res.json({ status: 1, message: message, data: systemdata })
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating."
            });
        });
    }
    else {
        object.CreatedAt = new Date();
        object.CreatedBy = 1;
        return db.System.create(object).then(systemdata => {
            req.body.Hosts.forEach(function (Host) {
                Host.SystemId = systemdata.id;
                if (Host.id && Host.id != 0) {
                    console.log("Exsist", Host);
                    db.Host.findByPk(Host.id).then(data => {
                        if (data) {
                            data.ModifiedAt = new Date();
                            data.ModifiedBy = 1;
                            data.update(Host);
                        }
                        else {
                            console.log("Host not found with id :", Host.id);
                        }

                    }).catch(err => {
                        console.log(err);
                    });

                }
                else {
                    console.log("new", Host);
                    db.Host.create(Host).then(data => {
                        console.log("Host Created..", data);
                    }).catch(err => {
                        console.log(err);
                    });
                }
            });
            var message = '';
            if (lang == 'nl') {
                message = 'Systeem toegevoegd succesvol!';
            } else {
                message = 'System Created successfully!';
            }
            res.json({ status: 1, message: message, data: systemdata })
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating."
            });
        });
    }
}