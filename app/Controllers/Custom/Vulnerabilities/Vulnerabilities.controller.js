const mysql = require('mysql');
const db = require('../../../models');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const Cryptr = require('cryptr');
const cryptr = new Cryptr('R@@tSeC');

var jwt = require('jsonwebtoken');
const SECRET_KEY = "secretkey12345";

var PDFGenerator = require('../../../PDFGeneration/index');
const readXlsxFile = require('read-excel-file/node');
const signer = require('node-signpdf').default;
const { pdfkitAddPlaceholder } = require('node-signpdf/dist/helpers');
const PDFDocument = require('pdfkit');
var path = require('path');
var fs = require('fs');
var config = require('../../../config/config.js');
var sequelize = new Sequelize(config.DB.Livedatabase, config.DB.user, config.DB.password, {
    host: config.DB.host,
    dialect: "mysql",
    logging: function () { },
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },
});

exports.AddOrEditVulnerabilitiesOld = (req, res) => {
    var lang = req.headers['lang'];
    // console.log('req.body', req.body);
    var object = req.body;
    upsert(db.Vulnerabilities, object, { id: object.id ? object.id : null }).
        then(function (Vulnerabilitiesresult) {
            console.log("Vulnerabilitiesresult..", Vulnerabilitiesresult);
            if (object.Hosts) {
                object.Hosts.forEach(function (Host) {
                    console.log(Host)
                    upsert(db.Host, Host, { id: Host.id }).
                        then(function (Hostresult) {
                            console.log("Hostresult.id..", Hostresult.id)
                            console.log("Vulnerabilitiesresult.id..", Vulnerabilitiesresult.id)
                            db.VulnerabilityHosts.findAll({
                                where: {
                                    HostId: Hostresult.id,
                                    VulnerabilityId: Vulnerabilitiesresult.id
                                }
                            }).then(vdata => {
                                console.log(vdata);
                                if (vdata.length == 0) {
                                    db.VulnerabilityHosts.create({
                                        HostId: Hostresult.id,
                                        VulnerabilityId: Vulnerabilitiesresult.id,
                                        CreatedAt: new Date(),
                                        CreatedBy: 1
                                    });
                                }
                            }).catch();

                        }).catch(err => {
                            return res.status(500).send({ status: 0, message: "Create/update Host failed", data: {} });
                        });
                });
            }
            if (object.Systems) {

                object.Systems.forEach(function (System) {
                    console.log(System)
                    db.VulnerabilitySystem.findAll({
                        where: {
                            SystemId: System.SystemId,
                            VulnerabilityId: Vulnerabilitiesresult.id
                        }
                    }).then(vdata => {
                        console.log(vdata);
                        if (vdata.length == 0) {
                            db.VulnerabilitySystem.create({
                                SystemId: System.SystemId,
                                VulnerabilityId: Vulnerabilitiesresult.id,
                                CreatedAt: new Date(),
                                CreatedBy: 1
                            });
                        }
                    }).catch();

                });
            }
            if (object.Assets) {
                object.Assets.forEach(function (Asset) {
                    console.log(Asset)
                    db.VulnerabilityAsset.findAll({
                        where: {
                            AssetId: Asset.AssetId,
                            SystemId: Asset.SystemId,
                            VulnerabilityId: Vulnerabilitiesresult.id
                        }
                    }).then(vdata => {
                        console.log(vdata);
                        if (vdata.length == 0) {
                            db.VulnerabilityAsset.create({
                                AssetId: Asset.AssetId,
                                SystemId: Asset.SystemId,
                                VulnerabilityId: Vulnerabilitiesresult.id,
                                CreatedAt: new Date(),
                                CreatedBy: 1
                            });
                        }
                    }).catch();

                });
            }

            if (object.Opportunities) {

                object.Opportunities.forEach(function (Opportunity) {
                    console.log(Opportunity)
                    let ObjOpportunity = {
                        OpportunityId: Opportunity.OpportunityId,
                        VulnerabilityId: Vulnerabilitiesresult.id,
                        AnswerId: Opportunity.AnswerId,
                        CreatedAt: new Date(),
                        CreatedBy: 1
                    }
                    upsert(db.VulnerabilityOpportunities, ObjOpportunity, {
                        OpportunityId: Opportunity.OpportunityId,
                        VulnerabilityId: Vulnerabilitiesresult.id
                    }).
                        then(function (vdata) {

                        }).catch();

                });
            }

            if (object.Damages) {

                object.Damages.forEach(function (Damage) {
                    console.log(Damage)
                    // db.VulnerabilityDamages.findAll({
                    //     where: {
                    //         DamageId: Damage.DamageId,
                    //         VulnerabilityId: Vulnerabilitiesresult.id
                    //     }
                    // }).then(vdata => {
                    //     console.log(vdata);
                    //     if (vdata.length == 0) {
                    //         db.VulnerabilityDamages.create({
                    //             DamageId: Damage.DamageId,
                    //             VulnerabilityId: Vulnerabilitiesresult.id,
                    //             AnswerId: Damage.AnswerId,
                    //             CreatedAt: new Date(),
                    //             CreatedBy: 1
                    //         });
                    //     }
                    // }).catch();
                    let ObjDamage = {
                        DamageId: Damage.DamageId,
                        VulnerabilityId: Vulnerabilitiesresult.id,
                        AnswerId: Damage.AnswerId,
                        CreatedAt: new Date(),
                        CreatedBy: 1
                    }
                    upsert(db.VulnerabilityDamages, ObjDamage, {
                        DamageId: Damage.DamageId,
                        VulnerabilityId: Vulnerabilitiesresult.id
                    }).
                        then(function (vdata) {

                        }).catch();

                });
            }
            var message = '';
            if (lang == 'nl') {
                message = 'Gemaakt/Bijgewerkt kwetsbaarheden succesvol';
            } else {
                message = 'Successfully Created/updated Vulnerabilities';
            }

            return res.status(200).send({ status: 1, message: message, data: Vulnerabilitiesresult });
        }).catch(err => {
            return res.status(500).send({ status: 0, message: "Create/update Vulnerabilities failed", data: err });
        });

}

exports.AddOrEditVulnerabilities = async (req, res) => {
    var lang = req.headers['lang'];
    // console.log('req.body', req.body);
    var object = req.body;
    if (!object.id || object.id == 0 || object.id == "0") {
        var LastRecord = await db.Vulnerabilities.findAll({
            limit: 1,
            where: {
                IsActive: true
            },
            order: [['id', 'DESC']]
        });

        if (LastRecord && LastRecord.length > 0) {
            console.log('LastRecordLastRecord', LastRecord[0].Order + 1);
            object.Order = (LastRecord[0].Order + 1);
        }
    }

    console.log('reqbodyobjectsad', object);
    var Vulnerabilitiesresult = await upsert(db.Vulnerabilities, object, { id: object.id ? object.id : null });
    if (Vulnerabilitiesresult) {

        if (object.Hosts) {
            object.Hosts.forEach(function (Host) {
                console.log(Host)
                upsert(db.Host, Host, { id: Host.id }).
                    then(function (Hostresult) {
                        console.log("Hostresult.id..", Hostresult.id)
                        console.log("Vulnerabilitiesresult.id..", Vulnerabilitiesresult.id)
                        db.VulnerabilityHosts.findAll({
                            where: {
                                HostId: Hostresult.id,
                                VulnerabilityId: Vulnerabilitiesresult.id
                            }
                        }).then(vdata => {
                            console.log(vdata);
                            if (vdata.length == 0) {
                                db.VulnerabilityHosts.create({
                                    HostId: Hostresult.id,
                                    VulnerabilityId: Vulnerabilitiesresult.id,
                                    CreatedAt: new Date(),
                                    CreatedBy: 1
                                });
                            }
                        }).catch();

                    }).catch(err => {
                        return res.status(500).send({ status: 0, message: "Create/update Host failed", data: {} });
                    });
            });
        }
        if (object.Systems) {

            object.Systems.forEach(function (System) {
                console.log(System)
                db.VulnerabilitySystem.findAll({
                    where: {
                        SystemId: System.SystemId,
                        VulnerabilityId: Vulnerabilitiesresult.id
                    }
                }).then(vdata => {
                    console.log(vdata);
                    if (vdata.length == 0) {
                        db.VulnerabilitySystem.create({
                            SystemId: System.SystemId,
                            VulnerabilityId: Vulnerabilitiesresult.id,
                            CreatedAt: new Date(),
                            CreatedBy: 1
                        });
                    }
                }).catch();

            });
        }
        if (object.Assets) {
            object.Assets.forEach(function (Asset) {
                console.log(Asset)
                db.VulnerabilityAsset.findAll({
                    where: {
                        AssetId: Asset.AssetId,
                        SystemId: Asset.SystemId,
                        VulnerabilityId: Vulnerabilitiesresult.id
                    }
                }).then(vdata => {
                    console.log(vdata);
                    if (vdata.length == 0) {
                        db.VulnerabilityAsset.create({
                            AssetId: Asset.AssetId,
                            SystemId: Asset.SystemId,
                            VulnerabilityId: Vulnerabilitiesresult.id,
                            CreatedAt: new Date(),
                            CreatedBy: 1
                        });
                    }
                }).catch();

            });
        }

        if (object.Opportunities) {

            object.Opportunities.forEach(function (Opportunity) {
                console.log(Opportunity)
                let ObjOpportunity = {
                    OpportunityId: Opportunity.OpportunityId,
                    VulnerabilityId: Vulnerabilitiesresult.id,
                    AnswerId: Opportunity.AnswerId,
                    CreatedAt: new Date(),
                    CreatedBy: 1
                }
                upsert(db.VulnerabilityOpportunities, ObjOpportunity, {
                    OpportunityId: Opportunity.OpportunityId,
                    VulnerabilityId: Vulnerabilitiesresult.id
                }).
                    then(function (vdata) {

                    }).catch();

            });
        }

        if (object.Damages) {

            object.Damages.forEach(function (Damage) {
                console.log(Damage)
                // db.VulnerabilityDamages.findAll({
                //     where: {
                //         DamageId: Damage.DamageId,
                //         VulnerabilityId: Vulnerabilitiesresult.id
                //     }
                // }).then(vdata => {
                //     console.log(vdata);
                //     if (vdata.length == 0) {
                //         db.VulnerabilityDamages.create({
                //             DamageId: Damage.DamageId,
                //             VulnerabilityId: Vulnerabilitiesresult.id,
                //             AnswerId: Damage.AnswerId,
                //             CreatedAt: new Date(),
                //             CreatedBy: 1
                //         });
                //     }
                // }).catch();
                let ObjDamage = {
                    DamageId: Damage.DamageId,
                    VulnerabilityId: Vulnerabilitiesresult.id,
                    AnswerId: Damage.AnswerId,
                    CreatedAt: new Date(),
                    CreatedBy: 1
                }
                upsert(db.VulnerabilityDamages, ObjDamage, {
                    DamageId: Damage.DamageId,
                    VulnerabilityId: Vulnerabilitiesresult.id
                }).
                    then(function (vdata) {

                    }).catch();

            });
        }
        var message = '';
        if (lang == 'nl') {
            message = 'Gemaakt/Bijgewerkt kwetsbaarheden succesvol';
        } else {
            message = 'Successfully Created/updated Vulnerabilities';
        }

        return res.status(200).send({ status: 1, message: message, data: Vulnerabilitiesresult });
    } else {
        return res.status(500).send({ status: 0, message: "Create/update Vulnerabilities failed", data: err });
    }
}

exports.UpdateVulnOrder = async (req, res) => {
    var object = req.body;

    if (object && object.vulnerabilities) {
        db.Vulnerabilities.bulkCreate(object.vulnerabilities,
            { updateOnDuplicate: ["Order"] })
            .then(data => {
                var message = '';
                return res.status(200).send({ status: 1, message: message, data: data });
            }).catch(err => {
                if (err.kind === 'ObjectId') {
                    return res.status(404).send({
                        message: "Records not found"
                    });
                }
                return res.status(500).send({
                    message: "Error retrieving records"
                });
            });
    } else {
        return res.status(500).send({
            message: "Error retrieving records"
        });
    }

}

exports.getVulnerabilitiesbyquery = (req, res) => {
    const decryptedID = cryptr.decrypt(req.params.id);
    console.log('decryptedID', decryptedID);

    db.Vulnerabilities.findAll({
        include: [db.LanguageTranslations,
        {
            model: db.VulnerabilityAsset,
            include: [{
                model: db.System,
                where: {
                    IsActive: true
                }
            }]
        }],
        where: {
            PenTestId: decryptedID,
            IsActive: true
        }
    }).then(data => {
        console.log("success")
        console.log(data)
        return res.status(200).send({ status: 1, message: 'Success', data: data });
        // res.json({ status: 1, message: "Success", data: data });
    }).catch(err => {
        return res.status(500).send({ status: 0, message: 'Failed', err });
    });

}

exports.getVulnerabilitiesbyId = (req, res) => {
    // const decryptedID = cryptr.decrypt(req.params.id);
    // console.log('decryptedID', decryptedID);

    var object = req.body;
    db.Vulnerabilities.findAll({
        include: [db.VlunerabilityAttachments, db.LanguageTranslations,
        {
            model: db.VulnerabilityHosts,
            include: [db.Host]
        },
        {
            model: db.VulnerabilityOpportunities,
            include: [db.Opportunities, db.OpportunityOptions]
        },
        {
            model: db.VulnerabilityDamages,
            include: [db.Damages, db.DamageOptions]
        },
        {
            model: db.VulnerabilityAsset,
            include: [db.Asset,
            {
                model: db.System,
                where: {
                    IsActive: true
                }
            }
            ]
        }
        ],
        where: {
            id: object.ids,
            IsActive: true
        }
    }).then(data => {
        return res.status(200).send({ status: 1, message: 'Success', data: data });
    }).catch(err => {
        return res.status(500).send({ status: 0, message: 'Failed', data: err });
    });

}

exports.getclientVulnerabilities = (req, res) => {
    var UserData = null;
    var token = req.headers['authorization'];
    token = token.toString().substring(7);
    if (!token)
        return res.status(403).send({ auth: false, message: 'No token provided.' });
    jwt.verify(token, SECRET_KEY, function (err, decoded) {
        if (err) {
            return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
        }

        UserData = {
            id: decoded.id,
            companies: decoded.companies
        };

        console.log('UserInfoUserInfo', UserData);
        var companyId = 0;
        if (UserData.companies && UserData.companies.length > 0) {
            companyId = UserData.companies[0].CompanyId;
        }

        if (companyId != 0) {
            db.Vulnerabilities.findAll({
                include: [db.VlunerabilityAttachments, db.LanguageTranslations, db.VulnerabilityUser,
                {
                    model: db.VulnerabilityHosts,
                    include: [db.Host]
                },
                {
                    model: db.VulnerabilityOpportunities,
                    include: [db.Opportunities, db.OpportunityOptions]
                },
                {
                    model: db.VulnerabilityDamages,
                    include: [db.Damages, db.DamageOptions]
                },
                {
                    model: db.VulnerabilityAsset,
                    include: [db.Asset,
                    {
                        model: db.System,
                        where: {
                            IsActive: true
                        }
                    }
                    ]
                },
                {
                    model: db.PenTest,
                    where: {
                        IsActive: true,
                        CompanyId: companyId
                    }
                }
                ],
                where: {
                    IsActive: true
                }
            }).then(data => {
                return res.status(200).send({ status: 1, message: 'Success', data: data });
            }).catch(err => {
                return res.status(500).send({ status: 0, message: 'Failed', data: err });
            });
        } else {
            return res.status(500).send({ status: 0, message: 'Failed', data: err });
        }
    });
}

exports.getclientVulnerabilitiesbyId = (req, res) => {
    // const decryptedID = cryptr.decrypt(req.params.id);
    // console.log('decryptedID', decryptedID);
    var object = req.body;
    var UserData = null;
    var token = req.headers['authorization'];
    token = token.toString().substring(7);
    if (!token)
        return res.status(403).send({ auth: false, message: 'No token provided.' });
    jwt.verify(token, SECRET_KEY, function (err, decoded) {
        if (err) {
            return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
        }

        UserData = {
            id: decoded.id,
            companies: decoded.companies
        };

        console.log('UserInfoUserInfo', UserData);
        var companyId = 0;
        if (UserData.companies && UserData.companies.length > 0) {
            companyId = UserData.companies[0].CompanyId;
        }

        if (companyId != 0) {
            db.Vulnerabilities.findAll({
                include: [db.VlunerabilityAttachments, db.LanguageTranslations,
                {
                    model: db.VulnerabilityHosts,
                    include: [db.Host]
                },
                {
                    model: db.VulnerabilityOpportunities,
                    include: [db.Opportunities, db.OpportunityOptions]
                },
                {
                    model: db.VulnerabilityDamages,
                    include: [db.Damages, db.DamageOptions]
                },
                {
                    model: db.VulnerabilityAsset,
                    include: [db.Asset,
                    {
                        model: db.System,
                        where: {
                            IsActive: true
                        }
                    }
                    ]
                },
                {
                    model: db.PenTest,
                    where: {
                        IsActive: true,
                        CompanyId: companyId
                    }
                }
                ],
                where: {
                    id: object.ids,
                    IsActive: true
                }
            }).then(data => {
                return res.status(200).send({ status: 1, message: 'Success', data: data });
            }).catch(err => {
                return res.status(500).send({ status: 0, message: 'Failed', data: err });
            });
        } else {
            return res.status(500).send({ status: 0, message: 'Failed', data: err });
        }
    });
}

exports.getAllVulnerabilities = (req, res) => {
    db.Vulnerabilities.findAll({
        include: [db.LanguageTranslations,
        {
            model: db.VulnerabilityAsset,
            include: [db.Asset,
            {
                model: db.System,
                where: {
                    IsActive: true
                }
            }
            ]
        }
        ],
        where: {
            IsActive: true
        },
        order: [
            ['order', 'ASC']
        ],
    }).then(data => {
        return res.status(200).send({ status: 1, message: 'Success', data: data });
    }).catch(err => {
        return res.status(500).send({ status: 0, message: 'Failed', data: err });
    });
}

exports.BulkAcceptVulnerabilities = (req, res) => {
    var object = req.body;
    if (object && object.vulnerabilities) {
        db.Vulnerabilities.bulkCreate(object.vulnerabilities,
            { updateOnDuplicate: ["IssueType", "IsAccepted", "AcceptedRemarks", "AcceptedSignature", "AcceptedBy", "ArgumentId"] })
            .then(data => {
                var message = '';
                return res.status(200).send({ status: 1, message: message, data: data });
            }).catch(err => {
                if (err.kind === 'ObjectId') {
                    return res.status(404).send({
                        message: "Records not found"
                    });
                }
                return res.status(500).send({
                    message: "Error retrieving records"
                });
            });
    } else {
        return res.status(500).send({
            message: "Error retrieving records"
        });
    }
}

exports.CheckDuplicationAssignedVuln = (req, res) => {
    var object = req.body;

    if (object && object.vulnerabilities && object.users) {

        db.VulnerabilityUser.findAll({
            where: {
                ClientUserId: object.users
            }
        }).then(data => {

            var VulnerabilityUsers = [];

            object.users.forEach(usrid => {
                object.vulnerabilities.forEach(vulnid => {
                    var checkVuln = data.filter(x => x.VulnerabilityId == vulnid && x.ClientUserId == usrid);
                    if (checkVuln.length <= 0) {
                        VulnerabilityUsers.push({
                            VulnerabilityId: vulnid,
                            ClientUserId: usrid,
                            CreatedAt: new Date(),
                            CreatedBy: 1,
                            ModifiedAt: new Date(),
                            ModifiedBy: 1
                        });
                    }
                });
            });

            return res.status(200).send({ status: 1, message: 'Success', data: VulnerabilityUsers });
        }).catch(err => {
            return res.status(500).send({ status: 0, message: 'Failed', data: err });
        });
    } else {
        return res.status(500).send({ status: 0, message: 'Failed', data: {} });
    }
}

exports.AssignVulnerabilities = (req, res) => {
    var object = req.body;

    if (object && object.vulnerabilityusers && object.vulnerabilityusers.length > 0) {
        db.VulnerabilityUser.bulkCreate(object.vulnerabilityusers)
            .then(data => {
                var message = '';
                return res.status(200).send({ status: 1, message: message, data: data });
            }).catch(err => {
                if (err.kind === 'ObjectId') {
                    return res.status(404).send({
                        message: "Records not found"
                    });
                }
                return res.status(500).send({
                    message: "Error retrieving records"
                });
            });
    } else {
        return res.status(200).send({ status: 0, message: 'Please select users and vulnerabilities', data: {} });
    }
}

exports.UnassignVulnerabilities = (req, res) => {
    var object = req.body;

    if (object && object.vulnerabilities && object.users && object.vulnerabilities.length > 0 && object.users.length > 0) {

        db.VulnerabilityUser.findAll({
            where: {
                ClientUserId: object.users
            }
        }).then(data => {

            var VulnerabilityUsers = [];

            object.users.forEach(usrid => {
                object.vulnerabilities.forEach(vulnid => {
                    var checkVuln = data.filter(x => x.VulnerabilityId == vulnid && x.ClientUserId == usrid);
                    if (checkVuln.length > 0) {
                        checkVuln.forEach(vuln => {
                            VulnerabilityUsers.push(vuln);
                        });
                    }
                });
            });

            if (VulnerabilityUsers.length > 0) {
                var VulnerabilityUserIds = VulnerabilityUsers.map(x => { return x.id });

                db.VulnerabilityUser.destroy({
                    where: {
                        id: VulnerabilityUserIds
                    }
                }).then(data => {
                    return res.status(200).send({ status: 1, message: 'Success', data: VulnerabilityUsers });
                }).catch(err => {
                    return res.status(500).send({ status: 0, message: 'Failed', data: err });
                });
            } else {
                return res.status(200).send({ status: 0, message: 'Please select users and vulnerabilities', data: {} });
            }
        }).catch(err => {
            return res.status(500).send({ status: 0, message: 'Failed', data: err });
        });
    } else {
        return res.status(200).send({ status: 0, message: 'Please select users and vulnerabilities', data: {} });
    }
}

exports.DownloadVulnPDF = async (req, res) => {
    try {
        var object = req.body;
        var file = '';
        if (object) {
            return new Promise((resolve, reject) => {
                resolve(PDFGenerator.GeneratePDF(object));
            }).then((data) => {
                var file = path.join(__dirname, '../../../../Downloads/Pentests/Penetratierapport_' + object.PentestId + '.pdf');
                return res.status(200).send({ status: 1, message: file, data: { IsGenerated: true } });
            }).catch((err) => {
                console.log('errrrrrrrrrr', err);
                return res.status(500).send({ status: 0, message: 'PDF Generation Failed', data: err });
            });
        } else {
            return res.status(500).send({ status: 0, message: file, data: { IsGenerated: false } });
        }
    } catch (err) {
        return res.status(500).send({ status: 0, message: 'PDF Generation Failed', data: err });
    }
}

exports.DigitalSignVulnPDF = async (req, res) => {
    try {

        const action = async () => {
            let pdfBuffer = fs.readFileSync(path.join(__dirname, '../../../../Downloads/businesscard.pdf'));
            let p12Buffer = fs.readFileSync(path.join(__dirname, '../../../../Downloads/Akshay.pfx'));

            // pdfBuffer = plainAddPlaceholder(
            //     {
            //         reason: 'I have reviewed it.',
            //         signatureLength: 1612,
            //     });

            let pdf = await signer.sign(pdfBuffer, p12Buffer, { passphrase: 'Login12*', asn1StrictParsing: false });
            fs.writeFileSync(__dirname + '/demo.pdf', pdf);
            console.log('newsignature', pdf);
        }
        action();
    } catch (err) {
        console.log('err', err);
        return res.status(500).send({ status: 0, message: 'PDF Generation Failed', data: err });
    }
}

exports.TestUploadExcel = async (req, res) => {
    try {
        var file = path.join(__dirname, '../../../../Downloads/TestUpload3.xlsx');

        const schema = {
            'START DATE': {
                prop: 'date',
                type: Date
            },
            'NUMBER OF STUDENTS': {
                prop: 'numberOfStudents',
                type: Number,
                required: true
            },
            'COURSE': {
                prop: 'course',
                type: {
                    'IS FREE': {
                        prop: 'isFree',
                        type: Boolean
                    },
                    'COURSE TITLE': {
                        prop: 'title',
                        type: String
                    }
                }
            },
            'CONTACT': {
                prop: 'contact',
                type: Number,
                required: true
            }
        }

        readXlsxFile(file, { sheet: 'Sheet2' }).then((data, errors) => {
            console.log('rowss', data);
        }).catch((ex) => {
            console.log('errorrrrrrrrrrr', ex);
        })
    } catch (err) {
        return res.status(500).send({ status: 0, message: 'PDF Generation Failed', data: err });
    }
}

exports.VulnerabilityTrends = (req, res) => {
    var object = req.body;
    if (object) {
        var query = "SELECT count(*) as total, `IssueType`, ";

        if (object.TrendsFilter && (object.TrendsFilter == 2 || object.TrendsFilter == 3)) {
            //Month -- Week
            query = query + "DAYNAME(tv.CreatedAt) as day, DATE_FORMAT(tv.CreatedAt, '%d-%b') as 'DateFormat' ";
        } else {
            //Year
            query = query + "MONTHNAME(tv.CreatedAt) as month, DATE_FORMAT(tv.CreatedAt, '%b-%y') as 'DateFormat' ";
        }

        query = query + "FROM `tblVulnerabilities` tv "

        if (object.userType == 4) {
            query = query + "left join tblPenTest tp on tv.`PenTestId` = tp.Id where";
            if (object.companyid) {
                query = query + " tp.CompanyId=" + object.companyid + " and tp.IsActive = 1 and";
            }
        } else {
            query = query + "left join tblVulnerabilityUser tvu on tv.Id = tvu.VulnerabilityId where";
            if (object.clientUserId) {
                query = query + " tvu.ClientUserId=" + object.clientUserId + " and";
            }
        }

        if (object.currentdate) {
            query = query + " tv.CreatedAt <='" + object.currentdate + "' and ";
            if (object.TrendsFilter && (object.TrendsFilter == 2 || object.TrendsFilter == 3)) {
                query = query + "tv.CreatedAt >= CAST(DATE_ADD('" + object.currentdate + "', INTERVAL ";
                if (object.TrendsFilter == 2) {
                    //Month
                    query = query + "-30"
                } else {
                    //Week
                    query = query + "-7"
                }
                query = query + " DAY) AS CHAR) and tv.IsActive = 1 group by DAYNAME(tv.CreatedAt), `IssueType`"
            } else {
                //Year
                query = query + "tv.CreatedAt >= CAST(DATE_ADD('" + object.currentdate + "', INTERVAL -12 MONTH) AS CHAR) and tv.IsActive = 1 group by MONTHNAME(tv.CreatedAt), `IssueType`"
            }
        }
        console.log('query', query);
        sequelize.query(query, { type: Sequelize.QueryTypes.SELECT })
            .then(data => {
                console.log("success")
                console.log(data)
                return res.status(200).send({ status: 1, message: 'Success', data: data });
            }).catch(err => {
                return res.status(500).send({ status: 0, message: 'Failed', err });
            });
    } else {
        return res.status(500).send({ status: 0, message: 'Failed', err });
    }
}

function upsert(modal, values, condition) {
    console.log("modal...", modal);
    console.log("values...", values);
    console.log("condition...", condition);
    return modal
        .findOne({ where: condition })
        .then(function (obj) {
            console.log("obj ...", obj);
            if (obj) {
                console.log("obj update...", obj);
                values.CreatedAt = new Date();
                // values.CreatedBy=1;
                return obj.update(values);
            }
            else {
                console.log("Create...", values);
                values.ModifiedAt = new Date();
                values.ModifiedBy = 1;
                return modal.create(values);
            }
        })
}