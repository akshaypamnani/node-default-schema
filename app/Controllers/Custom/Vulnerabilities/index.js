'use strict';

var express = require('express');
var Vulnerabilitiescontroller = require('./Vulnerabilities.controller');
var router = express.Router();
var auth = require('../../../Auth/auth.service');


router.put('/AddOrEditVulnerabilities/', auth.isAuthenticated(), Vulnerabilitiescontroller.AddOrEditVulnerabilities);

router.get('/getVulnerabilitiesbyquery/:id', auth.isAuthenticated(), Vulnerabilitiescontroller.getVulnerabilitiesbyquery);

router.post('/GeneratePDF', auth.isAuthenticated(), Vulnerabilitiescontroller.DownloadVulnPDF);

router.post('/TestUploadExcel', Vulnerabilitiescontroller.TestUploadExcel);

router.post('/DigitalSignVulnPDF', Vulnerabilitiescontroller.DigitalSignVulnPDF);

router.post('/UpdateVulnOrder', Vulnerabilitiescontroller.UpdateVulnOrder);

router.post('/getVulnerabilitiesbyId', Vulnerabilitiescontroller.getVulnerabilitiesbyId);

router.get('/getAllVulnerabilities', Vulnerabilitiescontroller.getAllVulnerabilities);

router.get('/getclientVulnerabilities', Vulnerabilitiescontroller.getclientVulnerabilities);

router.post('/getclientVulnerabilitiesbyId', Vulnerabilitiescontroller.getclientVulnerabilitiesbyId);

router.post('/AssignVulnerabilities', Vulnerabilitiescontroller.AssignVulnerabilities);

router.post('/UnassignVulnerabilities', Vulnerabilitiescontroller.UnassignVulnerabilities);

router.post('/CheckDuplicationAssignedVuln', Vulnerabilitiescontroller.CheckDuplicationAssignedVuln);

router.post('/BulkAcceptVulnerabilities', Vulnerabilitiescontroller.BulkAcceptVulnerabilities);

router.post('/VulnerabilityTrends', Vulnerabilitiescontroller.VulnerabilityTrends);
module.exports = router;