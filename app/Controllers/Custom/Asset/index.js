'use strict';

var express = require('express');
var Assetcontroller = require('./asset.controller');
var router = express.Router();
var auth = require('../../../Auth/auth.service');

//router.get('/getClientuser', usercontroller.getClientuser); 

//router.get('/getRootSecuser', usercontroller.getRootSecuser); 
//router.post('/UpdateUser/:id', usercontroller.updateUser);
router.put('/AddOrEditAsset/',auth.isAuthenticated(),  Assetcontroller.AddOrEditAsset);

//router.post('/EditRootSecuser/', usercontroller.EditRootSecuser);
module.exports = router;