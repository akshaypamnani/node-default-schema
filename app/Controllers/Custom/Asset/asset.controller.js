const mysql = require('mysql');
const db = require('../../../models');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

exports.AddOrEditAsset = (req, res) => {
    var lang = req.headers['lang'];
    console.log("Asset controller....AddOrEditAsset")
    console.log('req.body', req.body);
    const object = req.body;
    if (object.id && object.id != 0) {
        object.ModifiedAt = new Date();
        object.ModifiedAt = 1;
        db.Asset.findByPk(object.id).then(Assetdata => {
            Assetdata.update(object).then(data => {
                console.log("Asset updated..", data);
                req.body.Hosts.forEach(function (Host) {
                    Host.AssetId = object.id;
                    if (Host.id && Host.id != 0) {
                        console.log("Exsist", Host);
                        db.Host.findByPk(Host.id).then(data => {
                            if (data) {
                                data.ModifiedAt = new Date();
                                data.ModifiedBy = 1;
                                data.update(Host);
                            }
                            else {
                                console.log("Host not found with id :", Host.id);
                            }

                        }).catch(err => {
                            console.log(err);
                        });

                    }
                    else {
                        console.log("new", Host);

                        db.Host.create(Host).then(data => {
                            console.log("Host Created..", data);
                        }).catch(err => {
                            console.log(err);
                        });
                    }
                });
            })

            var message = '';
            if (lang == 'nl') {
                message = 'Asset geupdate succesvol!';
            } else {
                message = 'Asset updated successfully!';
            }
            res.json({ status: 1, message: message, data: Assetdata })
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating."
            });
        });

    }
    else {
        object.CreatedAt = new Date();
        object.CreatedBy = 1;
        return db.Asset.create(object).then(Assetdata => {
            object.Hosts.forEach(function (Host) {
                Host.AssetId = Assetdata.id;
                if (Host.id && Host.id != 0) {
                    console.log("Exsist", Host);
                    db.Host.findByPk(Host.id).then(data => {
                        if (data) {
                            data.ModifiedAt = new Date();
                            data.ModifiedBy = 1;
                            data.update(Host);
                        }
                        else {
                            console.log("Host not found with id :", Host.id);
                        }

                    }).catch(err => {
                        console.log(err);
                    });

                }
                else {
                    console.log("new", Host);
                    db.Host.create(Host).then(data => {
                        console.log("Host Created..", data);
                    }).catch(err => {
                        console.log(err);
                    });
                }
            });
            var message = '';
            if (lang == 'nl') {
                message = 'Asset toegevoegd succesvol!';
            } else {
                message = 'Asset Created successfully!';
            }
            res.json({ status: 1, message: message, data: Assetdata })
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating."
            });
        });

    }
}