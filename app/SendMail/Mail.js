var config = require('../config/config.js');
const nodemailer = require('nodemailer');



function sendmail(ToEmail,Subject,Message)
{
    const transport = nodemailer.createTransport({
        // service: config.Mail.Service, 
        host: config.Mail.host,
        port:  config.Mail.port,
        auth: {
            user: config.Mail.User, 
            pass: config.Mail.Password,
        }
    });
    const mailOptions = {
        from: config.Mail.from,
        to: ToEmail,
        subject: Subject,
        html: Message,
    };
    transport.sendMail(mailOptions, (error, info) => {
        if (error) {
            console.log(error);
        }
        // console.log(`Message sent: ${info.response}`);
    });
    
 
}

exports.sendmail=sendmail;
//module.exports = CustomMail;

//const transport = nodemailer.createTransport({
    //     service: config.Mail.Service, 
    //     auth: {
    //         user: config.Mail.User, 
    //         pass: config.Mail.Password,
    //     },
    // });
    // const mailOptions = {
    //     from: config.Mail.from,
    //     to: req.body.Email,
    //     subject: '2FA Code',
    //     html: '<h3>Your 2FA Code is..</h3>' + TFA,
    // };
    // transport.sendMail(mailOptions, (error, info) => {
    //     if (error) {
    //         console.log(error);
    //     }
    //     console.log(`Message sent: ${info.response}`);
    // });