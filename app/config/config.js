var config = {};

config.Mail = {
    User: 'support@rootdash.nl',
    Password: '^xx2f6$6$K*hTTbg8aBw', //Zof89895989 // Zof89598
    Service: 'Gmail',
    from: 'support@rootdash.nl',
    port: 587,
    host: 'smtp.office365.com'
};

config.Google = {
    ApiKey: 'AIzaSyCB_gZxHKYrObH6mhVA6QGsqiDpIQ-f2JU'
}

config.DB = {
    host: '192.168.1.17',
    user: 'root',
    password: 'admin41**00',
    Localdatabase: 'RootSec_Test_05_12',
    Testdatabase: 'RootSec_Test_05_12',
    Livedatabase: 'RootSec_Test_05_12',
    dialect: 'mysql'
}

config.ResetMail = {
    url: 'http://rootdash.nl/#/session/resetpassword?Email='
}

config.Tables =
{
    Company: 'Company',
    User: 'User',
    Asset: 'Asset',
    System: 'System',
    Pentest: 'Pentest',
    Vulnerabilities: 'Vulnerabilities',
    Ticket: 'Ticket'
}


config.TablePrefix =
{
    Company: 'CMP',
    User: 'USR',
    Asset: 'AST',
    System: 'SYS',
    Pentest: 'PEN',
    Vulnerabilities: 'VUL',
    Ticket: 'TCK'
}

config.VulTypes =
{
    Correct: 1,
    High: 2,
    Medium: 3,
    Low: 4,
}

config.VulIssueTypes =
{
    Known: 1,
    Unknown: 2,
    Solved: 3,
    Accepted: 4,
}

config.ImportType =
{

    SystemAsset: 1,
    Company: 2,
    User: 3,
    Vulnerabilities: 4,
    CombineStuff:5
}

module.exports = config;