'use strict';

var jwt = require('jsonwebtoken');
var expressJwt = require('express-jwt');
var compose = require('composable-middleware');
const db = require('../models');
const SECRET_KEY = "secretkey12345";
var validateJwt = expressJwt({ secret: "secretkey12345" });
var get_ip = require('ipware')().get_ip;
const publicIp = require('public-ip');

/**
 * Attaches the user object to the request if authenticated
 * Otherwise returns 403
 */
function isAuthenticated() {
    return compose()
        // Validate jwt
        .use(function (req, res, next) {
            console.log('req.headers.authorization', req.headers.authorization);
            if (req.headers.authorization) {
                validateJwt(req, res, next);
            }
            else {
                console.log("req.headers.authorization ..else");
                return res.status(500).send({
                    message: "No token provided."
                });
            }
        })
        // Attach user to request
        .use(function (err, req, res, next) {
            console.log(req.user);
            if (!req.user) {
                return res.status(401).send({
                    message: "Invalid Token."
                });
            }

            if (!checkIPConfig()) {
                return res.status(401).send({
                    message: "Unauthorized IP Address."
                });
            }

            db.User.findByPk(req.user.id).then(data => {
                if (!data) {
                    return res.status(401).send({
                        message: "Unauthorized"
                    });
                }
                req.user = data;
                next();
                // res.send(data);
            }).catch(err => {
                if (err.kind === 'ObjectId') {
                    return res.status(404).send({
                        message: "Record not found with id " + req.params.id
                    });
                }
                return res.status(500).send({
                    message: "Error retrieving record with id " + req.params.id
                });
                next(err);
            });
        });
}

async function checkIPConfig() {
    var localip_info = get_ip(req);
    console.log(localip_info);
    var clientIp;
    if (localip_info) {
        var clientIps;
        if (localip_info.clientIp.includes("f:")) {
            clientIps = localip_info.clientIp.split('f:');
            clientIp = clientIps[1];
        } else {
            clientIp = localip_info.clientIp;
        }
    }

    // var publicIP = await publicIp.v4();
    var publicIP = '';

    var USerIp = await db.User.findOne({
        where: {
            IP: [clientIp, publicIP],
            IsActive: true,
        }
    });

    if (!USerIp) {
        USerIp = await db.UserIps.findOne({
            include: [{
                model: db.User,
                IsActive: true
            }],
            where: {
                IP: [clientIp, publicIP], IsIPblocked: false, Wrongattemptcount: {
                    [Op.lt]: 3
                }
            }
        });
    }

    if (USerIp) {
        return true;
    } else {
        return false;
    }
}

function verifyToken(req, res, next) {
    console.log("...header ..", req.headers)
    var token = req.headers['x-access-token'];
    if (!token)
        return res.status(403).send({ auth: false, message: 'No token provided.' });
    jwt.verify(token, SECRET_KEY, function (err, decoded) {
        if (err)
            return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
        // if everything good, save to request for use in other routes
        req.userId = decoded.id;
        next();
    });
}

exports.isAuthenticated = isAuthenticated;
exports.verifyToken = verifyToken;
