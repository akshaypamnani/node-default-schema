const bcrypt = require('bcrypt');
const saltRounds = 16;
const db = require('../models');
const jwt = require('jsonwebtoken');
const SECRET_KEY = "secretkey12345";
var util = require('util');
var config = require('../config/config.js');
var CustomMail = require('../SendMail/Mail');
var moment = require('moment');
var crypto = require('crypto');
var get_ip = require('ipware')().get_ip;
const publicIp = require('public-ip');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

exports.newlogin = async (req, res) => {
    try {
        var User = await db.User.findOne({
            include: [db.UserCompany, {
                model: db.UserRole,
                include: [{
                    model: db.Role
                }]
            }],
            where: { Email: req.body.Email, TwoFACode: req.body.TwoFACode, IsActive: 1 }
        });

        if (User && User.UserCompanies) {
            const accessToken = jwt.sign({ id: User.id, companies: User.UserCompanies }, SECRET_KEY, {
                expiresIn: '60m'
            });
            User.update({
                TwoFACode: null
            });

            db.UserLoginLogs.create({
                UserId: User.id,
                loginTime: new Date(),
                logdate: new Date(),
                AccessToken: accessToken,
                CreatedAt: new Date(),
                ModifiedAt: new Date()
            });
            res.send({
                status: 1,
                data: User,
                message: "Succesvol ingelogd!!!",
                Token: accessToken
            });
        } else {
            addblockcountUserIps();
            return res.status(200).send({
                status: 0,
                message: "Email or 2FA Code is Invalid!"
            });
        }
    }
    catch (error) {
        return res.status(500).send({
            message: "Error retrieving user with Email :" + req.body.Email + "...." + error
        });
    }
}

exports.register = (req, res) => {
    console.log("Login..." + req.body.Email);
    db.User.findOne({ where: { "Email": req.body.Email } })
        .then(object => {
            if (!object) {
                console.log("Auth Controller...register")
                console.log(req.body.Password + "..." + saltRounds)
                bcrypt.hash(req.body.Password, saltRounds).then(function (hash) {
                    console.log("register...")
                    req.body.hashedPassword = hash;
                    const object = req.body;
                    console.log(req.params.collection)
                    console.log(object);
                    db.User.create(object)
                        .then(data => {

                            data.update({
                                CreatedAt: new Date(),
                                CreatedBy: data.id
                            })
                            return res.status(200).send({
                                status: 1,
                                message: "Resistered SuccessFully!"
                            });
                            // res.send(data);
                        }).catch(err => {
                            res.status(500).send({
                                message: err.message || "Some error occurred while creating."
                            });
                        });
                });

            }
            else {
                return res.status(200).send({
                    status: 0,
                    message: "User Already Exsist! " + req.body.Email
                });


            }
        }).catch(err => {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "User not found with Email Or User Is Not Active" + req.body.Email
                });
            }
            return res.status(500).send({
                message: "Error retrieving user with Email :" + req.body.Email + "...." + err
            });
        });
}

exports.newGenerateTwoFA = async (req, res) => {
    try {
        var User = await db.User.findOne({ where: { "Email": req.body.Email, "IsActive": 1 } });

        if (User) {
            if (User.validPassword(req.body.Password)) {
                var TFA = Math.random().toString(36).substr(3, 6).toUpperCase();
                var datetime = new Date();
                User.update({
                    TwoFACode: TFA,
                    TwoFACodeTime: datetime
                });

                var ToEmail = req.body.Email;
                var Subject = "Uw verificatiecode voor Rootdash";
                var Message = '<p style="color:#000000;">Beste ' + User.FirstName + ',</p>' +
                    '<p style="color:#000000;">U kunt de onderstaande verificatiecode gebruiken om in te loggen op Rootdash:</p>' +
                    '<h4 style="color:#000000;"><strong>' + TFA + '</strong></h4>' +
                    '<p style="color:#000000;">Let op, deze code is slechts 15 minuten bruikbaar.</p>' +
                    '<p style="color:#000000;">U kunt een nieuwe code aanvragen indien deze onverhoopt is verlopen.</p>' +
                    '<p style="color:#000000;">Heeft u niet proberen in te loggen? Neem dan per direct contact op met onze technische afdeling via: support@rootdash.nl.</p>' +
                    '<p style="color:#000000;">Met vriendelijke groet,</p>' +
                    '<p style="color:#000000;">Team Rootdash</p>' +
                    '<p style="color:#000000;">Date : ' + moment().format('MMMM Do YYYY, h:mm:ss a') + '</p>';
                CustomMail.sendmail(ToEmail, Subject, Message);
                res.json({
                    status: 1, message: 'Code is verzonden naar ' + ToEmail + "!", data: {
                        Generate2Fa: true
                    }
                });
            } else {
                await addblockcountUserIps(req);
                return res.status(200).send({
                    status: 0,
                    message: "Please Enter Valid Password",
                    messageType: 1,
                    data: {
                        Generate2Fa: false
                    }
                });
            }
        } else {
            await addblockcountUserIps(req);
            return res.status(200).send({
                status: 0,
                message: "User not found! " + req.body.Email,
                messageType: 2,
                data: {
                    Generate2Fa: false
                }
            });
        }
    }
    catch (error) {
        return res.status(500).send({
            message: "Error retrieving user with Email :" + req.body.Email + "...." + error
        });
    }
}

exports.resetPassword = async (req, res) => {
    try {
        var User = await db.User.findOne({ where: { "Email": req.body.Email, "IsActive": 1 } });
        var salt = crypto.randomBytes(16).toString('hex');
        var hashedPassword = crypto.pbkdf2Sync(req.body.Password, salt,
            1000, 64, `sha512`).toString(`hex`);
        User.update({
            Password: "",
            hashedPassword: hashedPassword,
            salt: salt,
            IsfirstLogin: 0,
            IsResetPassword: 0
        });

        return res.status(200).send({
            status: 1, message: "Password reset success!!", data: User
        });
    }
    catch (err) {
        return res.status(500).send({
            status: 0, message: "Password is Already reset :", data: err
        });
    }
}

exports.ForgotPasswordLink = (req, res) => {
    var Email = req.body.Email;
    db.User.findOne(
        {
            where: {
                "Email": req.body.Email,
                "IsActive": 1
            }
        }).then(data => {
            if (data) {
                console.log("Exsist..");
                const accessToken = jwt.sign({ Email: req.body.Email }, SECRET_KEY, {
                    expiresIn: '10m'
                });
                data.update({
                    IsResetPassword: 1
                });
                var link = config.ResetMail.url + req.body.Email + "&token=" + accessToken;
                var ToEmail = req.body.Email;
                var Subject = "Een nieuw wachtwoord instellen voor Rootdash.";
                var Message = '<p style="color:#000000;">Beste ' + data.FirstName + ',</p>' +
                    '<p style="color:#000000;">U ontvangt deze email omdat u uw wachtwoord opnieuw wilt instellen.</p>' +
                    '<p style="color:#000000;">De volgende link leidt u naar eenomgeving waar u deze kunt instellen:</p>' +
                    '<h4 style="color:#000000;">' + link + '</h4>' +
                    '<p style="color:#000000;">Deze code is tijdelijk beschikbaar en verloopt binnen 15 minuten.</p>' +
                    '<p style="color:#000000;">Mocht u onverhoopt te laat zijn, dan kunt u de stappen opnieuw doorlopen.</p>' +
                    '<p style="color:#000000;">Heeft u niet geprobeerd om uw wachtwoord opnieuw in te stellen, dan verzoeken wij u direct contact op te nemen met de technische support van Rootdash via: support@rootdash.nl.</p>' +
                    '<p style="color:#000000;">Met vriendelijke groet,</p>' +
                    '<p style="color:#000000;">Team Rootdash</p>' +
                    '<p style="color:#000000;">Date : ' + moment().format('MMMM Do YYYY, h:mm:ss a') + '</p>';
                CustomMail.sendmail(ToEmail, Subject, Message);
                return res.status(200).send({
                    status: 1,
                    message: "Link has been sent to " + req.body.Email + "!",
                    data: { "accessToken": accessToken, Email: req.body.Email }
                });
            }
            else {
                return res.status(200).send({
                    status: 0,
                    message: "Email Does not Exsist"
                });
            }
        }).catch();

}

exports.IsValidJWT = (req, res) => {
    var jwttoken = req.body.jwttoken;
    let Isvalidmail = false;
    console.log("jwttoken..", jwttoken);
    db.User.findOne(
        {
            where: {
                "Email": req.body.Email,
                "IsActive": 1,
                "IsResetPassword": 1
            }
        }).then(data => {
            if (data) {
                jwt.verify(req.body.jwttoken, 'secretkey12345', function (err, decoded) {
                    console.log("decoded..", decoded);
                    if (err) {

                        return res.status(200).send({
                            status: 0,
                            message: "Link has been Expired",
                            data: { Istokenvalid: false }
                        });
                    }
                    else {
                        return res.status(200).send({
                            status: 1,
                            message: "Link is Working",
                            data: { Istokenvalid: true }
                        });
                    }
                });
            }
            else {
                return res.status(200).send({
                    status: 0,
                    message: "Link has been Expired",
                    data: { Istokenvalid: false }
                });
            }
        });
}

exports.IPCheck = async (req, res) => {

    var localip_info = get_ip(req);
    console.log(localip_info);
    var clientIp;
    if (localip_info) {
        var clientIps;
        if (localip_info.clientIp.includes("f:")) {
            clientIps = localip_info.clientIp.split('f:');
            clientIp = clientIps[1];
        } else {
            clientIp = localip_info.clientIp;
        }
    }

    // var publicIP = await publicIp.v4();
    var publicIP = '';

    var USerIp = await db.User.findOne({
        where: {
            IP: [clientIp, publicIP],
            IsActive: true,
        }
    });

    if (!USerIp) {
        USerIp = await db.UserIps.findOne({
            include: [{
                model: db.User,
                IsActive: true
            }],
            where: {
                IP: [clientIp, publicIP], IsIPblocked: false, Wrongattemptcount: {
                    [Op.lt]: 3
                }
            }
        });
    }

    if (USerIp) {
        return res.status(200).send({
            status: 1,
            message: "Authentication Success",
            data: { publicIP: publicIP, localIP: clientIp, Authentication: true }
        });
    } else {
        return res.status(200).send({
            status: 0,
            message: "Authentication Failed",
            data: { publicIP: publicIP, localIP: clientIp, Authentication: false }
        });
    }
}

async function addblockcountUserIps(req) {
    // var localip_info = get_ip(req);
    // console.log(localip_info);
    // var clientIp;
    // if (localip_info) {
    //     var clientIps = localip_info.clientIp.split('f:');
    //     clientIp = clientIps[1];
    // }
    // var publicIP = await publicIp.v4();
    // console.log('publicIP', clientIp, publicIP);

    var localip_info = get_ip(req);
    console.log(localip_info);
    var clientIp;
    if (localip_info) {
        var clientIps;
        if (localip_info.clientIp.includes("f:")) {
            clientIps = localip_info.clientIp.split('f:');
            clientIp = clientIps[1];
        } else {
            clientIp = localip_info.clientIp;
        }
    }

    // var publicIP = await publicIp.v4();
    var publicIP = '';

    var UserIp = await db.UserIps.findOne({
        where: {
            IP: [clientIp, publicIP], IsIPblocked: false
        }
    });

    if (UserIp) {
        console.log('UserIpsssssssssss', UserIp);
        if (UserIp.Wrongattemptcount < 3) {
            UserIp.update({
                Wrongattemptcount: UserIp.Wrongattemptcount + 1
            });
        } else {
            UserIp.update({
                IsIPblocked: true
            });
            db.BlockUnblockIPlog.create({
                IP: clientIp,
                UserIPId: UserIp.id,
                Type: true,
                BlockUnblockDate: new Date(),
                BlockUnblockBy: 1
            });
        }
    }
}