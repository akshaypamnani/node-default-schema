'use strict';

var express = require('express');
var auth = require('./auth.controller');
var router = express.Router();

router.post('/login', auth.newlogin);
router.post('/register', auth.register);
router.post('/GenerateTwoFA', auth.newGenerateTwoFA);
router.post('/resetPassword', auth.resetPassword);
router.post('/ForgotPasswordLink', auth.ForgotPasswordLink);
router.post('/IsValidJWT', auth.IsValidJWT);
router.get('/IPCheck', auth.IPCheck);
module.exports = router;                