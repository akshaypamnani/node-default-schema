
const PDFDocument = require('pdfkit');
const db = require('../models');
const fs = require('fs');
var pdf = require('html-pdf');
var path = require('path');
var moment = require('moment');
var config = require('../config/config.js');
var Sequelize = require('sequelize');
var sequelize = new Sequelize(config.DB.Livedatabase, config.DB.user, config.DB.password, {
    host: config.DB.host,
    dialect: "mysql",
    logging: function () { },
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },
});
const Op = Sequelize.Op;

var htmlContent = '';
var htmlStyle = '';
var htmlBody = '';
var htmlEnd = '';
var Pentest = {};
var BasePath = path.join(__dirname, '../../fonts').replace(/\\/g, "\\\\");
BasePath = BasePath.replace(/\\\\/g, "/");

async function GeneratePDF(objpentest) {

    htmlContent = '<html><head><link rel="stylesheet" type="text/css" href="http://getbootstrap.com/dist/css/bootstrap.min.css">';
    htmlStyle = '<style>@font-face {font-family: "Heebo";src: url("fonts/Heebo-Regular.ttf") format("truetype");font-weight: normal;font-style: normal;}@font-face {font-family: "Heebo-Bold";src: url("fonts/Heebo-Bold.ttf") format("truetype");}h1, strong {font-family: Heebo-Bold}body{margin:0px;font-size: 12px;font-family:Heebo;color:#4c4c4c;}p{margin:0px !important;}.pagebreakafter{page-break-after:always}.ql-align-center{text-align:center;}.explainationtd{color: white;font-size: 15px;text-align: center;padding: 0px !important;font-weight: 600;height:50px;}table{border-collapse: collapse;}thead{background:#f4f7fa;}th{padding:5px;font-size: 12px;text-align: left;}tbody tr{border-bottom:1px solid #dddddd}td{font-size: 9px !important;padding: 5px;}.vulntable tbody tr{font-size:10px;}.pentestchecklisttable{width: 100%;}.pentestchecklisttable tbody tr{font-size:12px;}.pentestchecklisttable tbody td{padding: 10px; }p{font-size:10px;}h1{font-size:1.8rem;}' +
        '.cover-screen {position:relative;background:#ffffff; margin-left:auto;margin-right:auto; width:550px; padding-top:150px;}' +
        '.cover-screen:before {content:"";position:absolute; top:0; bottom:0; width:40%; background:#464d69;}' +
        '.cover-screen .main-circle{width:380px; height:380px;display:table; background:#ffffff;border-radius:50%; position:relative; margin-left:auto;margin-right:auto; text-align:center;}' +
        '.cover-screen .main-circle .circle-inner{display:table-cell; vertical-align:bottom;}' +
        '.cover-screen .main-circle h3{font-size:35px;line-height:1.1;font-family:Heebo-Bold}' +
        '.cover-screen .main-circle span {min-width:200px; display:inline-block; padding:5px 10px; border-top:1px solid #ddd;border-bottom:1px solid #ddd; text-transform:uppercase;margin-bottom: 80px;}' +
        '.cover-screen .main-circle .cm-screen{position:absolute;top:-80px; max-width:250px; left:50%;  -webkit-transform:translateX(-50%);transform:translateX(-50%);}' +
        '.bottom-details {color:#ffffff;position: relative; padding:20px 40px; display:table;width: 100%;}' +
        '.bottom-details .left-section{width:50%;display:table-cell;}' +
        '.bottom-details .right-section{width:50%;display:table-cell;color:#333;}' +
        '.bottom-details .dt-row{margin-bottom:25px;}' +
        '.bottom-details .dt-row p{margin-bottom:0;margin-top:0;}' +
        '.bottom-details .dt-row h5{margin-bottom:0;margin-top:5px;}' +
        '@media print {div {-webkit-print-color-adjust: exact !important;}}' +
        '.back-cover{width:100%; padding-top:150px;}' +
        '.back-cover .logo-circle{width:300px; height:300px; border-radius:50%; border:1px solid #D1D3D4; text-align:center;margin-left:auto;margin-right:auto; margin-bottom:200px;}' +
        '.back-cover .logo-circle img {width:220px;margin-top:45px;}' +
        '.pdf-footer{display:table;}' +
        '.pdf-footer .dt-column{display:table-cell; text-align:center; padding:0 40px;}' +
        '.pdf-footer .dt-column h6{font-size:14px;margin:0;margin-bottom:5px;font-weight:700;}' +
        '.pdf-footer .dt-column p{font-size:10px;margin:0;color:#333333;}' +
        '.pdf-footer .dt-column p a{color:#333333;text-decoration:none;}' +
        '.pdf-footer .dt-column:not(:last-child){border-right:1px solid #D1D3D4;}' +
        '.statusbox {width: 20%;border: 2px solid #827d7d;text-align: center;padding: 20px 10px;font-size: 12px;background: #F4F4F7;color: #000;font-weight: bold;}' +
        '.statusbox + .statusbox {border-left: 0;}' +
        '.clssummarystatus {width: 100%;display: -webkit-box; display: -moz-box;display: -ms-flexbox;display: -webkit-flex; display: flex;}' +
        '@media print {div {-webkit-print-color-adjust: exact !important;}}' +
        '</style>';
    htmlBody = '<body>';
    htmlEnd = '</div></body></html>';

    var pentestid = objpentest.PentestId;
    Pentest = await db.PenTest.findOne({
        include: [{
            model: db.Company,
            include: [db.Asset, db.System]
        }],
        where: { id: pentestid, IsActive: true }
    });

    var lang = 'nl';
    if (objpentest.lang) {
        lang = objpentest.lang;
    }
    console.log('pathsss', Pentest.Company.Assets);
    if (Pentest) {
        await PDFCoverPage(Pentest, lang);
        if (objpentest.reportType && objpentest.reportType == 2) {
            await PDFManageSummary(Pentest, lang);
        } else if (objpentest.reportType && objpentest.reportType == 3) {
            await PDFClassification(lang);
            await PDFHostsIps(Pentest, lang);
            await PDFVulnerabilities(Pentest, objpentest.IsHideAccepted, lang);
        } else if (objpentest.reportType && objpentest.reportType == 4) {
            await PDFPentestChecklist(Pentest, lang);
        } else {
            await PDFManageSummary(Pentest, lang);
            await PDFClassification(lang);
            await PDFHostsIps(Pentest, lang);
            await PDFVulnerabilities(Pentest, objpentest.IsHideAccepted, lang);
            await PDFAttackTreePage(Pentest, lang);
            await PDFPentestChecklist(Pentest, lang);
        }
        await PDFExtrainfo(lang);
        await PDFLastPage(lang);
        await createPDF(Pentest);
    }
}

async function createPDF(Pentest, lang) {
    var html = htmlContent + htmlStyle + htmlBody + htmlEnd;
    var HeaderTitle = 'Penetratierapport || ' + Pentest.Name;

    if (lang == 'nl') {
        HeaderTitle = 'Penetratierapport || ' + Pentest.Name;
    } else {
        HeaderTitle = 'Penetration report || ' + Pentest.Name;
    }

    var options = {
        "header": {
            "height": "10mm",
            "contents": {
                first: '  ',
                default: '<div style="text-align: left;border-bottom: 2px solid #dddddd;color: #444444;font-family:"Heebo";font-size:10px;"><span style="font-size:10px !important;">' + HeaderTitle + '</span><span style="font-size:10px !important;float: right;margin-right: 20px;font-family: Heebo-Bold;">{{page}}</span></div></div>', // fallback value
                last: '  '
            }
        },
        "format": "A4",        // allowed units: A3, A4, A5, Legal, Letter, Tabloid
        "orientation": "portrait",
        "border": {
            "top": "10mm",            // default is 0, units: mm, cm, in, px
            "right": "15mm",
            "bottom": "10mm",
            "left": "15mm"
        },
        base: 'file:///' + BasePath
    };
    var filepath = path.join(__dirname, '../../Downloads/Pentests/Penetratierapport_' + Pentest.id + '.pdf');

    if (fs.existsSync(filepath)) {
        fs.unlinkSync(filepath);
    }

    pdf.create(html, options).toFile('./Downloads/Pentests/Penetratierapport_' + Pentest.id + '.pdf', function (err, res) {
        if (err) {
            return console.log(err);
        } else {
            htmlContent = '';
            htmlStyle = '';
            htmlBody = '';
            htmlEnd = '';
            console.log(res); // { filename: '/app/businesscard.pdf' }
            return 'Penetratierapport_' + Pentest.id + '.pdf';
        }
    });
}

async function PDFCoverPage(Pentest, lang) {
    if (lang == 'nl') {
        htmlBody += '<div class="cover-screen">' +
            '<div class="main-circle"><img class="cm-screen" src="fonts/cm-screen.png" alt="..."><div class="circle-inner"><h3>Security Rapportage</h3><span>' + Pentest.Name + '</span></div></div>' +
            '<div class="bottom-details">' +
            '<div class="left-section"><div class="dt-row"><p>Uitgebracht aan</p><h5>' + Pentest.Name + '</h5></div><div class="dt-row"><p>Organisatie naam</p><h5>' + (Pentest.Company ? Pentest.Company.Name : '--') + '</h5></div></div >' +
            '<div class="right-section"><div class="dt-row"><p>Datum</p><h5>' + moment(new Date()).format('DD-MM-YYYY') + '</h5></div><div class="dt-row"><p>Uitgebracht door</p><h5>Rootsec B.V.</h5></div></div></div ></div ><div class="pagebreakafter"></div><div class="container">';
    } else {

        htmlBody += '<div class="cover-screen">' +
            '<div class="main-circle"><img class="cm-screen" src="fonts/cm-screen.png" alt="..."><div class="circle-inner"><h3>Security Reporting</h3><span>' + Pentest.Name + '</span></div></div>' +
            '<div class="bottom-details">' +
            '<div class="left-section"><div class="dt-row"><p>Pentest name:</p><h5>' + Pentest.Name + '</h5></div><div class="dt-row"><p>Company Name</p><h5>' + (Pentest.Company ? Pentest.Company.Name : '--') + '</h5></div></div >' +
            '<div class="right-section"><div class="dt-row"><p>Date</p><h5>' + moment(new Date()).format('DD-MM-YYYY') + '</h5></div><div class="dt-row"><p>Published by</p><h5>Rootsec B.V.</h5></div></div></div ></div ><div class="pagebreakafter"></div><div class="container">';
    }
}

async function PDFManageSummary(Pentest, lang) {
    var ManageSummary = await db.PenTestSummary.findOne({
        include: [{
            model: db.LanguageTranslations,
            where: { PageRefId: 3, IsActive: true }
        }],
        where: { PenTestId: Pentest.id, IsActive: true }
    });

    if (ManageSummary && ManageSummary.LanguageTranslations && ManageSummary.LanguageTranslations.length > 0) {
        ManageSummary.LanguageTranslations.forEach(langtranslation => {
            if (lang == 'nl') {
                if (langtranslation.DefaultKey == 'Conclusion') {
                    ManageSummary.Conclusion = langtranslation.TitleNL;
                } else if (langtranslation.DefaultKey == 'Recommendation') {
                    ManageSummary.Recommendation = langtranslation.TitleNL;
                }
            } else {
                if (langtranslation.DefaultKey == 'Conclusion') {
                    ManageSummary.Conclusion = langtranslation.TitleEN;
                } else if (langtranslation.DefaultKey == 'Recommendation') {
                    ManageSummary.Recommendation = langtranslation.TitleEN;
                }
            }
        });
    }

    //Conclusion
    if (ManageSummary) {

        if (lang == 'nl') {
            htmlBody += '<h1 style="text-align:center;">Algemene Conclusie</h1><br/>' + ManageSummary.Conclusion + '<br/>';

            htmlBody += '<h1 style="text-align:center;">Algemene beoordeling</h1><br/><br /><div class="clssummarystatus"><div class="statusbox" style="background:' + GetSummaryStatus(ManageSummary.Status, 1) + '">Kritiek</div><div class="statusbox" style="background:' + GetSummaryStatus(ManageSummary.Status, 2) + '">Zeer onveilig</div><div class="statusbox" style="background:' + GetSummaryStatus(ManageSummary.Status, 3) + '">Onveilig</div><div class="statusbox" style="background:' + GetSummaryStatus(ManageSummary.Status, 4) + '">Voldoende</div><div class="statusbox" style="background:' + GetSummaryStatus(ManageSummary.Status, 5) + '">Sterk</div></div><div class="pagebreakafter"></div>';
            //Recommendations
            htmlBody += '<h1 style="text-align:center;">Belangrijkste aanbevelingen</h1><br/>' + ManageSummary.Recommendation + '<div class="pagebreakafter"></div>';
        } else {
            htmlBody += '<h1 style="text-align:center;">General conclusion</h1><br/>' + ManageSummary.Conclusion + '<br/>';

            htmlBody += '<h1 style="text-align:center;">Overall rating</h1><br/><br /><div class="clssummarystatus"><div class="statusbox" style="background:' + GetSummaryStatus(ManageSummary.Status, 1) + '">Critical</div><div class="statusbox" style="background:' + GetSummaryStatus(ManageSummary.Status, 2) + '">Very unsafe</div><div class="statusbox" style="background:' + GetSummaryStatus(ManageSummary.Status, 3) + '">Unsafe</div><div class="statusbox" style="background:' + GetSummaryStatus(ManageSummary.Status, 4) + '">Sufficient</div><div class="statusbox" style="background:' + GetSummaryStatus(ManageSummary.Status, 5) + '">Strong</div></div><div class="pagebreakafter"></div>';
            //Recommendations
            htmlBody += '<h1 style="text-align:center;">Main recommendations</h1><br/>' + ManageSummary.Recommendation + '<div class="pagebreakafter"></div>';
        }
    }
}

function GetSummaryStatus(ManageSummaryStatus, Status) {
    if (Status) {
        if (Status == 1 && ManageSummaryStatus == Status) {
            return '#ff0000'
        } else if (Status == 2 && ManageSummaryStatus == Status) {
            return '#ff6900'
        } else if (Status == 3 && ManageSummaryStatus == Status) {
            return '#fcb900'
        } else if (Status == 4 && ManageSummaryStatus == Status) {
            return '#cddc39'
        } else if (Status == 5 && ManageSummaryStatus == Status) {
            return '#5d92f4'
        }
    }
}

function PDFClassification(lang) {
    if (lang == 'nl') {
        var htmlPDFClassification = fs.readFileSync('./Content/PDFExplaination.html', 'utf8');
        htmlBody += htmlPDFClassification + '<div class="pagebreakafter"></div>';
    } else {
        var htmlPDFClassification = fs.readFileSync('./Content/PDFExplaination_en.html', 'utf8');
        htmlBody += htmlPDFClassification + '<div class="pagebreakafter"></div>';
    }
}

async function PDFHostsIps(Pentest, lang) {

    if (Pentest.Company) {
        if (Pentest.Company.Assets && Pentest.Company.Assets.length > 0) {
            if (lang == 'nl') {
                var htmlAssets = '<table style="width:100%;"><thead><tr><th>Software / url / IP-range</th></tr></thead><tbody>';

                Pentest.Company.Assets.forEach(asset => {
                    htmlAssets += '<tr><td>url: ' + asset.Urls + '</td></tr>';
                    htmlAssets += '<tr><td>IpRange: ' + asset.IpRange + '</td></tr>';
                    htmlAssets += '<tr><td>Software: ' + asset.Software + '</td></tr>';
                    htmlAssets += '<tr><td></td></tr>';
                });

                htmlAssets += '</tbody><tfoot><tr><td colspan="2">Deze objecten bevinden zich in een test omgeving.</td></tr></tfoot></table ><br /><br />';

                htmlBody += '<h1 style="text-align:center;">Objecten van het onderzoek</h1><p style="text-align:center;">De objecten van het onderzoek van deze hacktest zijn:</p><br/>' + htmlAssets;
            } else {
                var htmlAssets = '<table style="width:100%;"><thead><tr><th>Software / url / IP-range</th></tr></thead><tbody>';

                Pentest.Company.Assets.forEach(asset => {
                    htmlAssets += '<tr><td>url: ' + asset.Urls + '</td></tr>';
                    htmlAssets += '<tr><td>IpRange: ' + asset.IpRange + '</td></tr>';
                    htmlAssets += '<tr><td>Software: ' + asset.Software + '</td></tr>';
                    htmlAssets += '<tr><td></td></tr>';
                });

                htmlAssets += '</tbody ><tfoot><tr><td colspan="2">These objects are all within the production environment</td></tr></tfoot></table ><br /><br />';

                htmlBody += '<h1 style="text-align:center;">Research objects</h1><p style="text-align:center;">The research objects during this hack test are:</p><br/>' + htmlAssets;
            }
        }

        if (Pentest.Company.Systems && Pentest.Company.Systems.length > 0) {
            if (lang == 'nl') {
                var htmlSystems = '<table style="width:100%;"><thead><tr><th>IP-range</th></tr></thead><tbody>';

                Pentest.Company.Systems.forEach(System => {
                    htmlSystems += '<tr><td>' + System.Remarks + (System.Name && System.Name.toString().length > 0 ? ' - ' + System.Name : '') + '</td></tr>';
                });

                htmlSystems += '</tbody></table><br/><br/>';

                htmlBody += '<h2 style="text-align:center;">Objecten van het onderzoek</h2><p style="text-align:center;">Tijdens de uitvoering van deze hacktest is gebruik gemaakt van de volgende (infrastructurele) voorzieningen</p><br/>' + htmlSystems + '<div class="pagebreakafter"></div>';
            } else {
                var htmlSystems = '<table style="width:100%;"><thead><tr><th>IP-range</th></tr></thead><tbody>';

                Pentest.Company.Systems.forEach(System => {
                    htmlSystems += '<tr><td>' + System.Remarks + (System.Name && System.Name.toString().length > 0 ? ' - ' + System.Name : '') + '</td></tr>';
                });

                htmlSystems += '</tbody></table><br/><br/>';

                htmlBody += '<h2 style="text-align:center;">Usage of generic facilities</h2><p style="text-align:center;">During the execution of this penetration test we have used the following infrastructural services.</p><br/>' + htmlSystems + '<div class="pagebreakafter"></div>';
            }
        }
    }
}

async function PDFVulnerabilities(Pentest, isHideAccepted, lang) {
    var IssueType = 0;
    if (isHideAccepted != undefined && isHideAccepted == true) {
        IssueType = 4;
    }

    console.log('isHideAccepted', isHideAccepted);

    var Vulnerabilities = await db.Vulnerabilities.findAll({
        include: [{
            model: db.LanguageTranslations,
            where: { PageRefId: 2, IsActive: true }
        }],
        where: {
            PenTestId: Pentest.id,
            IssueType: {
                [Op.ne]: IssueType
            },
            IsActive: true
        }
    });

    if (Vulnerabilities && Vulnerabilities.length > 0) {
        Vulnerabilities.forEach(Vulnerability => {
            if (Vulnerability.LanguageTranslations && Vulnerability.LanguageTranslations.length > 0) {
                Vulnerability.LanguageTranslations.forEach(langtranslation => {
                    if (lang == 'nl') {
                        if (langtranslation.DefaultKey == 'Name') {
                            Vulnerability.Name = langtranslation.TitleNL;
                        }
                    } else {
                        if (langtranslation.DefaultKey == 'Name') {
                            Vulnerability.Name = langtranslation.TitleEN;
                        }
                    }
                });
            }
        });
    }

    if (Vulnerabilities && Vulnerabilities.length > 0) {
        if (lang == 'nl') {
            let htmlVulnerabilities = "";
            let htmlVulnerabilitiesStart = '<table class="vulntable"><thead><tr><th></th><th>Kwetsbaarheid naam</th><th>Facing</th><th>Risico</th><th>Ingewikkeldheid</th><th>Inspanningen</th><th>Datum</th></tr></thead><tbody>';
            var htmlVulnerabilitiesData = '';
            let htmlVulnerabilitiesEnd = '</tbody></table>';
            Vulnerabilities.forEach((vuln, key) => {

                let VulnName = vuln.Name ? vuln.Name : '--';
                let Facing = vuln.Facing != null ? vuln.Facing == 1 ? 'Openbaar' : vuln.Facing == 2 ? 'extern' : vuln.Facing == 3 ? 'intern' : '--' : '--';
                let Risk = vuln.Risk ? vuln.Risk == 1 ? 'Correct' : vuln.Risk == 2 ? 'hoog' : vuln.Risk == 3 ? 'Midden' : vuln.Risk == 4 ? 'Laag' : '--' : '--';
                let Complexity = vuln.Complexity ? vuln.Complexity == 1 ? 'Correct' : vuln.Complexity == 2 ? 'hoog' : vuln.Complexity == 3 ? 'Midden' : vuln.Complexity == 4 ? 'Laag' : '--' : '--';
                let Effort = vuln.Effort ? vuln.Effort == 1 ? 'Correct' : vuln.Effort == 2 ? 'hoog' : vuln.Effort == 3 ? 'Midden' : vuln.Effort == 4 ? 'Laag' : '--' : '--';
                let CreatedDate = vuln.CreatedAt ? moment(vuln.CreatedAt).format('Do MMM, YY') : '--';
                htmlVulnerabilitiesData += '<tr>';
                htmlVulnerabilitiesData += '<td>' + (key + 1) + '.   ' + '</td>';
                htmlVulnerabilitiesData += '<td style="width: 30%;">' + VulnName + '</td>';
                htmlVulnerabilitiesData += '<td style="font-weight:bold;">' + Facing + '</td>';
                htmlVulnerabilitiesData += '<td style="font-weight:bold;color:' + REfillcolor(vuln.Risk) + '">' + Risk + '</td>';
                htmlVulnerabilitiesData += '<td style="font-weight:bold;color:' + Complexityfillcolor(vuln.Complexity) + '">' + Complexity + '</td>';
                htmlVulnerabilitiesData += '<td style="font-weight:bold;color:' + REfillcolor(vuln.Effort) + '">' + Effort + '</td>';
                htmlVulnerabilitiesData += '<td style="width: 100px;">' + CreatedDate + '</td>';
                htmlVulnerabilitiesData += '</tr>';
            });

            htmlVulnerabilities = htmlVulnerabilitiesStart + htmlVulnerabilitiesData + htmlVulnerabilitiesEnd;

            htmlBody += '<h1 style="text-align:center;">Kwetsbaarheden</h1><br/>' + htmlVulnerabilities + '<div class="pagebreakafter"></div>';
        } else {
            let htmlVulnerabilities = "";
            let htmlVulnerabilitiesStart = '<table class="vulntable"><thead><tr><th></th><th>Vulnerability name</th><th>Facing</th><th>Risk</th><th>Complexity</th><th>Efforts</th><th>Date</th></tr></thead><tbody>';
            var htmlVulnerabilitiesData = '';
            let htmlVulnerabilitiesEnd = '</tbody></table>';
            Vulnerabilities.forEach((vuln, key) => {

                let VulnName = vuln.Name ? vuln.Name : '--';
                let Facing = vuln.Facing != null ? vuln.Facing == 1 ? 'Public' : vuln.Facing == 2 ? 'External' : vuln.Facing == 3 ? 'Internal' : '--' : '--';
                let Risk = vuln.Risk ? vuln.Risk == 1 ? 'Correct' : vuln.Risk == 2 ? 'High' : vuln.Risk == 3 ? 'Medium' : vuln.Risk == 4 ? 'Low' : '--' : '--';
                let Complexity = vuln.Complexity ? vuln.Complexity == 1 ? 'Correct' : vuln.Complexity == 2 ? 'High' : vuln.Complexity == 3 ? 'Medium' : vuln.Complexity == 4 ? 'Low' : '--' : '--';
                let Effort = vuln.Effort ? vuln.Effort == 1 ? 'Correct' : vuln.Effort == 2 ? 'High' : vuln.Effort == 3 ? 'Medium' : vuln.Effort == 4 ? 'Low' : '--' : '--';
                let CreatedDate = vuln.CreatedAt ? moment(vuln.CreatedAt).format('Do MMM, YY') : '--';
                htmlVulnerabilitiesData += '<tr>';
                htmlVulnerabilitiesData += '<td>' + (key + 1) + '.   ' + '</td>';
                htmlVulnerabilitiesData += '<td style="width: 42%;">' + VulnName + '</td>';
                htmlVulnerabilitiesData += '<td style="font-weight:bold;">' + Facing + '</td>';
                htmlVulnerabilitiesData += '<td style="font-weight:bold;color:' + REfillcolor(vuln.Risk) + '">' + Risk + '</td>';
                htmlVulnerabilitiesData += '<td style="font-weight:bold;color:' + Complexityfillcolor(vuln.Complexity) + '">' + Complexity + '</td>';
                htmlVulnerabilitiesData += '<td style="font-weight:bold;color:' + REfillcolor(vuln.Effort) + '">' + Effort + '</td>';
                htmlVulnerabilitiesData += '<td style="width: 100px;">' + CreatedDate + '</td>';
                htmlVulnerabilitiesData += '</tr>';
            });

            htmlVulnerabilities = htmlVulnerabilitiesStart + htmlVulnerabilitiesData + htmlVulnerabilitiesEnd;

            htmlBody += '<h1 style="text-align:center;">Vulnerabilities</h1><br/>' + htmlVulnerabilities + '<div class="pagebreakafter"></div>';
        }
    }
}

async function PDFAttackTreePage(Pentest, lang) {
    let PentestAttackTree = '';
    var ManageSummary = await db.PenTestSummary.findOne({
        include: [{
            model: db.LanguageTranslations,
            where: { DefaultKey: 'AttackTree', PageRefId: 3, IsActive: true }
        }],
        where: { PenTestId: Pentest.id, IsActive: true }
    });
    console.log('ManageSummaryManageSummary', ManageSummary);
    if (ManageSummary) {
        PentestAttackTree = ManageSummary.AttackTree;
        if (ManageSummary.LanguageTranslations && ManageSummary.LanguageTranslations.length > 0) {
            if (lang == 'nl') {
                PentestAttackTree = ManageSummary.LanguageTranslations[0].TitleNL;
            } else {
                PentestAttackTree = ManageSummary.LanguageTranslations[0].TitleEN;
            }
        }
    }

    console.log('ManageSummaryLanguageTranslationslength22', PentestAttackTree);
    htmlBody += '<div class="clsextrainfo"><h1 style="text-align:center;">AttackTree</h1><br/>';
    htmlBody += PentestAttackTree + '<div class="pagebreakafter"></div>';
}

// async function PDFAttackTreePage(lang) {
//     if (lang == 'nl') {
//         htmlBody += '<div class="clsextrainfo"><h1 style="text-align:center;">AttackTree</h1><br/>' + '<p>Rootsec biedt inzicht in de belangen, dreigingen en weerbaarheid en de daarmee samenhangende ontwikkelingen op het gebied van cybersecurity. Attack trees worden gebruikt voor het modelleren van veiligheidsrisico’s en risico’s in complexe ICT-systemen.</p><br />' +
//             '<p>Gelet op de sterkte van jullie omgeving kunnen we hier enkel advies geven over wat jullie omgeving, mogelijk, naar een nog hoger niveau kan tillen. Het instellen van alerting op jullie omgeving / WAF zou de security verbeteren. Hackaanvallen tegenhouden is een goede eerste stap en dit doen jullie op dit moment zeer goed, maar anticiperen op toekomstige aanvallen is waar mogelijk nog beter.</p><br />' +
//             '<p>Alle gebeurtenissen in de toekomst monitoren, zoals middels de firewall zou een mooie toevoeging zijn. Hiervoor dient er een goed geconfigureerde IPS/ IDS te komen op de firewall. Zorg ervoor dat kwaadwillende injecties worden gelogd en na herhaaldelijk proberen uiteindelijk resulteren in, bijvoorbeeld, een ban.</p><br />' +
//             '<p>We denken dat omgevingen zoals die van jullie in de toekomst veel aandacht zullen krijgen. Daarom is het ook belangrijk om deze in de toekomst constant te monitoren</p><br />' +
//             '<p>Onze complimenten voor jullie omgeving. In de bijlage vindt u onze Burp state file, deze kunt u gebruiken om alle door ons uitgevoerde injecties te bekijken.</p><br />';
//         htmlBody += '</div><div class="pagebreakafter"></div>';
//     } else {
//         htmlBody += '<div class="clsextrainfo"><h1 style="text-align:center;">AttackTree</h1><br/>' + '<p>Rootsec offers insight into the importance, threats and resilience and the coherent developments in the field of cyber security. Attack trees are used to model the safety risks in complex IT systems.</p><br />' +
//             '<p>Despite the fact that Yamaha’s environment is relatively strong, we do need to emphasize the direct need for patching and maintenance for some of the servers. This is a considerable weakness given the fact that outdated software makes your servers far more attractive to attackers. After having patched and updated all that needed to be, we’d like to see Yamaha start using monthly vulnerability assessments on your external environment. By having checks on a monthly basis you can protect yourself better and by doing so obtain a higher level of IT security.</p><br />' +
//             '<p>Secondly we would like to emphasize the importance of correctly configuring alerting on your environment/firewall. This will greatly improve your level of security. Stopping attacks is a good first step but anticipating and preventing an attack before it happens is even better.</p><br />' +
//             '<p>In order to do so we’d advise you to start monitoring more, for example through your firewall. You would need a properly configured IPS/IDS on your firewall to start doing this. Ensure that malicious injections are logged and, after repeated attempts, eventually result in a ban.</p><br />' +
//             '<p>We believe that environments, like yours, will gain more and more attention in the nearby future. This is why we so strongly emphasize the need for decent monitoring. Having said that, we do expect that an environment like yours might also become subject to a phishing attack. This is due to the fact that such an attack is easier to execute and quite often also more efficient. This is why we recommend you have your SPF and DMARC records checked.</p><br />';
//         htmlBody += '</div><div class="pagebreakafter"></div>';
//     }
// }

async function PDFPentestChecklist(Pentest, lang) {

    // var PentestChecklist = await sequelize.query("SELECT penlist. * , IFNULL( pencksts.Id, 0 ) AS PenTestCheckListStatusId, IFNULL( pencksts.Status, 0 ) AS  Status , IFNULL( pencksts.PenTestId, 0 ) as PenTestId, IFNULL( pencksts.PenTestChecklistId, 0 ) as PenTestChecklistId, IFNULL( tv.id, 0 ) as vulnerabilityId FROM tblPenTestChecklists penlist LEFT JOIN tblPenTestChecklistsStatus pencksts ON penlist.Id = pencksts.PenTestChecklistId AND pencksts.pentestid=" + Pentest.id + " Left join tblVulnerabilities tv on tv.PenTestChecklistStatusId = pencksts.id", { type: Sequelize.QueryTypes.SELECT });

    var PentestChecklist = await sequelize.query("SELECT tlttitle.TitleEN, tlttitle.TitleNL, penlist. *, IFNULL( pencksts.Id, 0 ) AS PenTestCheckListStatusId, IFNULL( pencksts.Status, 0 ) AS  Status , IFNULL( pencksts.PenTestId, 0 ) as PenTestId, IFNULL( pencksts.PenTestChecklistId, 0 ) as PenTestChecklistId, IFNULL( tv.id, 0 ) as vulnerabilityId FROM tblPenTestChecklists penlist LEFT JOIN tblPenTestChecklistsStatus pencksts ON penlist.Id = pencksts.PenTestChecklistId AND pencksts.pentestid=" + Pentest.id + " Left join tblVulnerabilities tv on tv.PenTestChecklistStatusId = pencksts.id LEFT JOIN tblLabelsTranslations tlttitle on penlist.Title = tlttitle.DefaultKey GROUP BY tlttitle.TitleEN, tlttitle.TitleNL, penlist.Id ORDER By penlist.Id", { type: Sequelize.QueryTypes.SELECT });

    if (PentestChecklist && PentestChecklist.length > 0) {

        if (lang == 'nl') {
            let htmlPentestChecklist = "";
            let htmlPentestChecklistStart = '<table class="pentestchecklisttable"><thead><tr><th width="5" style="width:5px;"></th><th>Titel</th><th style="text-align:right;">Staat</th></tr></thead><tbody>';
            var htmlPentestChecklistData = '';
            let htmlPentestChecklistEnd = '</tbody></table>';
            PentestChecklist.forEach((pentestcheck, key) => {
                let Status = pentestcheck.Status != null ? pentestcheck.Status == 0 ? 'N.V.T' : pentestcheck.Status == 1 ? 'CORRECT' : pentestcheck.Status == 2 ? 'NIET CORRECT' : pentestcheck.Status == 3 ? 'N.V.T' : '--' : '--';

                htmlPentestChecklistData += '<tr>';
                htmlPentestChecklistData += '<td width="5" style="width:5px;max-width:5px;">' + (key + 1) + '.   ' + '</td>';
                htmlPentestChecklistData += '<td>' + pentestcheck.TitleNL + '</td>';
                htmlPentestChecklistData += '<td style="text-align:right;font-weight:bold;color:' + PentestChecklistStatusColor(pentestcheck.Status) + '">' + Status + '</td>';
                htmlPentestChecklistData += '</tr>';
            });

            htmlPentestChecklist = htmlPentestChecklistStart + htmlPentestChecklistData + htmlPentestChecklistEnd;

            htmlBody += '<h1 style="text-align:center;">Beveiligingsrichtlijnen</h1><br/>' + htmlPentestChecklist + '<div class="pagebreakafter"></div>';
        } else {
            let htmlPentestChecklist = "";
            let htmlPentestChecklistStart = '<table class="pentestchecklisttable"><thead><tr><th width="5" style="width:5px;"></th><th>Title</th><th style="text-align:right;">Status</th></tr></thead><tbody>';
            var htmlPentestChecklistData = '';
            let htmlPentestChecklistEnd = '</tbody></table>';
            PentestChecklist.forEach((pentestcheck, key) => {
                let Status = pentestcheck.Status != null ? pentestcheck.Status == 0 ? 'N.V.T' : pentestcheck.Status == 1 ? 'CORRECT' : pentestcheck.Status == 2 ? 'INCORRECT' : pentestcheck.Status == 3 ? 'N.V.T' : '--' : '--';

                htmlPentestChecklistData += '<tr>';
                htmlPentestChecklistData += '<td width="5" style="width:5px;max-width:5px;">' + (key + 1) + '.   ' + '</td>';
                htmlPentestChecklistData += '<td>' + pentestcheck.TitleEN + '</td>';
                htmlPentestChecklistData += '<td style="text-align:right;font-weight:bold;color:' + PentestChecklistStatusColor(pentestcheck.Status) + '">' + Status + '</td>';
                htmlPentestChecklistData += '</tr>';
            });

            htmlPentestChecklist = htmlPentestChecklistStart + htmlPentestChecklistData + htmlPentestChecklistEnd;

            htmlBody += '<h1 style="text-align:center;">Security guidelines</h1><br/>' + htmlPentestChecklist + '<div class="pagebreakafter"></div>';
        }
    }
}

function PDFExtrainfo(lang) {
    if (lang == 'nl') {
        var htmlNormenkader = '<p>In dit hoofdstuk zijn de technische beveiligingsnormen vastgelegd die Rootsec BV gebruikt bij het beoordelen van een Information and Communication Technology (ICT)-infrastructuur waarvoor geen beveiligingsbeleid aan ons is overhandigd.</p><br /><p>De criteria zijn tot stand gekomen door best practices van vergelijkbare omgevingen uit de industrie samen te voegen. Deze criteria zullen in veel gevallen betrekking hebben op maatregelen die op operationeel niveau genomen moeten worden.</p><p>De criteria zelf zijn zodanig gekozen dat, als een ICT-omgeving op deze aspecten getest wordt, veel gemaakte beveiligingsgerelateerde fouten zullen worden ontdekt. Hierdoor wordt het risico dat er toch nog fouten in de omgeving aanwezig zullen zijn verkleind. Hiermee willen wij niet aangeven dat een dergelijke test alle fouten uit een ICT-omgeving zal halen. Een test kan alleen de aanwezigheid van fouten aantonen, niet de afwezigheid. De kennis van mogelijke beveiligingsgerelateerde fouten groeit naarmate tijd verstrijkt. Behalve het bekend worden van nieuwe fouten, zullen er wijzigingen optreden in de operationele omgeving. Het valt dan ook aan te bevelen om een technische beveiligingsbeoordeling (Technical Security Assessment) regelmatig te herhalen en daarbij tevens telkens te bekijken of de criteria waarmee getest wordt nog steeds voldoen.</p>';

        var htmlDiversiteit = '<p>De beveiligingsarchitectuur moet bestaan uit meerdere maatregelen die wezenlijk van elkaar verschillen, zodat het doorbreken van een beveiligingsmaatregel niet automatisch leidt tot de val van het gehele systeem. Dit principe staat ook wel bekend als ‘defence in depth.’</p><p>Dit begrip wordt onder meer gebruikt door de Nederlandsche Bank in haar voorschriften voor de juiste inrichting van een ICT-infrastructuur. Het idee erachter is dat informatie beschermd wordt door meerdere lagen. Hiermee wordt voorkomen dat een enkele fout ertoe kan leiden dat informatie beschikbaar raakt voor ongeautoriseerde personen.</p><p>Er zullen minstens twee niveaus van beveiliging geïmplementeerd moeten worden. Naast de installatie en juiste configuratie van een firewall-infrastructuur zullen de gebruikte server-systemen bijvoorbeeld tevens gehardend moeten worden. Daarnaast zal de applicatie zodanig ontwikkeld moeten zijn dat misbruik ervan zoveel mogelijk voorkomen wordt.</p>';

        var htmlRedundantie = '<p>Systeemcomponenten worden vaak meervoudig uitgevoerd, zodat de beschikbaarheid van het systeem niet afhankelijk is van een enkel component. Dit principe kan ook worden toegepast op de beveiligingsmaatregelen zelf. Daarbij geldt dat de beveiligingsarchitectuur moet bestaan uit een combinatie van maatregelen, zodat de beveiliging niet afhankelijk is van een enkele maatregel. Dit principe vermijdt dus impliciet mogelijk aanwezige ‘Single Point of Failure (SPoF).’</p><p>Een SPoF kan leiden tot het onbeschikbaar raken van een service doordat een onderdeel van de totale omgeving niet redundant is uitgevoerd. Dit kan een stuk hardware zijn, maar ook bijvoorbeeld een database met gegevens zonder backupvoorziening. Indien de toegang tot de service als uitermate belangrijk wordt gezien en de vervanging van het betreffende onderdeel te lang op zich laat wachten, kan dit een probleem opleveren. Afhankelijk van de noodzakelijke beschikbaarheid kan gekozen worden voor een hot-standby of het op voorraad houden van het betreffende onderdeel.</p>';

        var htmlIsolatie = '<p>Dit houdt in dat hardware en software die relevant zijn voor de beveiliging (de Trusted Computing Base (TCB)) altijd zo klein en compact mogelijk gehouden moet worden. Hoe groter de TCB, hoe moeilijker het zal zijn om te verifiëren of de beveiliging ervan voldoende gewaarborgd is. Het principe moet minimaal worden toegepast in firewalls.</p><br /><p>We bedoelen hier met ‘firewall’ een configuratie van een of meerdere systemen die een netwerkseg- ment beschermen tegen ongeautoriseerde toegang van buiten. Indien de inrichting van de firewall- systemen die deel uitmaken van de ICT-infrastructuur onjuist is, kan het wegvallen van de firewall leiden tot het volledig bereikbaar worden van het netwerk voor buitenstaanders.</p><p>De firewall-systemen die de productiesegmenten beschermen tegen ongeautoriseerd verkeer van buiten zullen zodanig ingericht worden dat in het geval van een probleem geen toegang verkregen kan worden tot het beveiligde netwerk. Tevens zullen deze firewall-systemen uitsluitend verkeer toestaan dat expliciet is geautoriseerd. Dit laatste staat ook wel bekend als de implementatie van veilige verstekwaarden (defaults), ook wel het ‘deny all, except’ principe genoemd</p>';

        var htmlBeveiliginggegevens = '<p>Gegevens op systemen, zoals in databases, zijn zeer waardevol. Toegang tot deze gegevens zal geregeld moeten worden met een vorm van toegangscontrole (ook als deze gegevens op back-up tapes staan). Deze toegangscontrole zal minimaal bestaan uit een vorm van identificatie (bijvoorbeeld een gebruikersnaam) en een vorm van authenticatie (bijvoorbeeld een wachtwoord). Om altijd juiste gegevens beschikbaar te hebben is het goed om een rollback-mechanisme te hebben. Het uitfaseren van hardware en software mag niet leiden tot het in handen vallen van gegevens aan derden. Hierbij valt te denken aan uitgefaseerde systemen met gegevens op de harde schijf, maar ook aan afgedankte backup-tapes. Het vastleggen van zowel geslaagde als mislukte authenticaties is een belangrijk hulpmiddel om pogingen tot ongeoorloofde toegang te detecteren en te traceren, terwijl een welkomstboodschap met daarin de gebruiksrichtlijnen zijn nut kan bewijzen in het geval dat een inbraak tot een rechtszaak leidt. In het kader van een technische beveiligingsbeoordeling (Technical Security Assessment) zal aandacht gegeven worden aan de beveiliging van de gegevens zoals deze beschikbaar zijn op de operationele systemen. De andere genoemde aspecten maken deel uit van de procedures en de noodzakelijke organisatie. Beschermde (persoonlijke) gegevens dienen niet voor derden (gebruikers) toegankelijk te zijn. Alleen geautoriseerde gebruikers zullen hun eigen gegevens kunnen wijzigen.</p>';

        var htmlBeschikbaarheidgegevens = '<p>De gegevens zullen zoveel mogelijk beschikbaar moeten zijn. De beschikbaarheid wordt verbeterd als de betrokken systemen fouttolerant gedrag vertonen en bestand zijn tegen ‘Denial of Service (DoS)’ aanvallen. In het kader van een technische beveiligingsbeoordeling kan nagegaan worden op welke wijze de beschikbaarheid van gegevens negatief kan worden beïnvloed.</p><p>Hierdoor kunnen de onderzoekers duidelijk maken waar zich problemen kunnen voordoen en op welke wijze deze problemen wellicht voorkomen of opgelost kunnen worden.</p>';

        var htmlBeveiligingcommunicatie = '<p>Afhankelijk van de soort gegevens die wordt verstuurd is het belangrijk maatregelen te nemen om na te gaan of de zender degene is die hij claimt te zijn (anti-spoofing maatregelen zoals authenticatie) en/of dat de ontvanger is die zij claimt te zijn. Ook kan het wenselijk zijn de informatieoverdracht zelf te beveiliging tegen afluisteren en modificatie door derden. Een beveiliging tegen afluisteren kan van belang zijn voor de informatieaanbieder, maar ook voor de informatieontvanger; bijvoorbeeld uit privacyoverwegingen.</p>';

        var htmlVolledigheid = '<p>Elke vorm van toegang mag pas plaatsvinden na autorisatie door het systeem. Gebruikers en processen dienen zich daartoe altijd eerst te authenticeren.</p><p>Natuurlijk kan het noodzakelijke niveau van de autorisatie afhangen van de waarde die aan het systeem, de processen en de gegevens die op het systeem worden verwerkt en zijn opgeslagen, wordt toegekend.</p>';

        var htmlAdditioneelbeveiligenservers = '<p>Dit volgt direct uit het eerder genoemde ‘defence in depth’ principe. Een server moet ingericht worden voor het leveren van één of meerdere specifieke diensten en niet meer. Dit wordt ook wel eens weergegeven met de term ‘hardening’. Een standaard installatie is onvoldoende, omdat deze in veel gevallen services beschikbaar stelt die niet noodzakelijk zijn en wellicht beveiligingsrisico’s met zich meebrengen. Daarnaast moet ervoor gezorgd worden dat de inrichting van servers die dezelfde services leveren gelijk wordt gehouden. Hiermee samenhangend is het tijdig installeren van beveiligingsgerelateerde ‘patches’ noodzakelijk. Een technische beveiligingsbeoordeling (Technical Security Assessment) zal nagaan wat de huidige configuratie is van de verschillende server systemen en aanbevelingen doen over noodzakelijke wijzigingen.</p>';

        var htmlBeperkingprivileges = '<p>Het systeem moet zo opgezet zijn dat bepaalde gebruikers en processen niet meer functies mogen uitvoeren of hulpbronnen mogen gebruiken dan strikt noodzakelijk is. Dit principe staat ook bekend onder de namen: ‘Least Privilege’, ‘Need to Know’ en ‘Need to Use’.</p><p>Op systemen zelf zal dit principe van minimale privileges gehanteerd moeten worden. Gebruikers zullen alleen toegang krijgen tot die delen van het systeem die voor hen bereikbaar moeten zijn. Het implementeren van beperkingen kan inbraken niet voorkomen maar zal het effect ervan beperken. Het principe van beperking kan verder doorgevoerd worden door een grote mate van functiescheiding toe te passen. Bij dit ‘least privilege’ principe zal ook gedacht moeten worden aan het benaderen van de backoffice-omgeving met beperkte privileges. De applicaties zullen bijvoorbeeld niet met database- beheerprivileges met de backoffice-systemen mogen communiceren. Een ander voorbeeld van beperking vindt men terug in het terugdringen van onnodige informatielek- kage. Alle informatie die uitlekt naar een aanvaller, zoals testbestanden, documentatie, foutmeldingen etc., maken het voor die aanvaller eenvoudiger om de beveiliging te doorbreken. Dit moet dus zoveel mogelijk voorkomen worden.</p>';

        htmlBody += '<div class="clsextrainfo"><h1 style="text-align:center;">Normenkader</h1><br/>' + htmlNormenkader + '<br />';
        htmlBody += '<h1 style="text-align:center;">Diversiteit</h1><br/>' + htmlDiversiteit + '<div class="pagebreakafter"></div>';
        htmlBody += '<h1 style="text-align:center;">Redundantie</h1><br/>' + htmlRedundantie + '<br />';
        htmlBody += '<h1 style="text-align:center;">Isolatie</h1><br/>' + htmlIsolatie + '<div class="pagebreakafter"></div>';
        htmlBody += '<h1 style="text-align:center;">Beveiliging van gegevens</h1><br/>' + htmlBeveiliginggegevens + '<br />';
        htmlBody += '<h1 style="text-align:center;">Beschikbaarheid van gegevens</h1><br/>' + htmlBeschikbaarheidgegevens + '<div class="pagebreakafter"></div>';
        htmlBody += '<h1 style="text-align:center;">Beveiliging van communicatie</h1><br/>' + htmlBeveiligingcommunicatie + '<br />';
        htmlBody += '<h1 style="text-align:center;">Volledigheid</h1><br/>' + htmlVolledigheid + '<br />';
        htmlBody += '<h1 style="text-align:center;">Additioneel beveiligen van servers</h1><br/>' + htmlAdditioneelbeveiligenservers + '<div class="pagebreakafter"></div>';
        htmlBody += '<h1 style="text-align:center;">Beperking van privileges</h1><br/>' + htmlBeperkingprivileges + '</div><div class="pagebreakafter"></div>';
    }
    else {
        var htmlSetofstandards = '<p>In this chapter we cover the technical set of standards which are used by the client during the assessment of an ICT infrastructure for which we have not been given a predetermined security policy.</p><br /><p>The criteria used in our set of standards are based on best practices from similar environments, merged into one standard. These criteria will, in many cases, relate to measures which have to be taken on a operational level. The criteria are made in such a way that when they are applied to an ICT environment, the most commonly made security related mistakes will be uncovered. This will greatly reduce the risk that there are still vulnerabilities in the environment. This does however not mean that such an assessment can or will remove all leaks or vulnerabilities from the ICT environment. A test can only show the active issues, not the lack thereof. The knowledge of possible security related issues grows as time goes by. Aside from uncovering new issues, changes will happen in the operational environment. We therefore advise to use and often repeat a Technical Security Assessment to continuously check whether or not the criteria used for testing are still up-to-date.</p>';

        var htmlDiversity = '<p>The security architecture must consist out of several measures which significantly differ from one another to ensure that breaching one measure does not mean the entire system collapses. This principle is also known as ‘defence in depth’. This definitive is, among others, used by the Dutch National bank in its regulations for configuring ICT infrastructure properly. The idea behind is that the information is protected by several layers. This will prevent that one mistake might lead to information becoming accessible to unauthorized people. There should be at least two security levels implemented. Aside from the installation and the right configuration of a firewall infrastructure, the server systems also need to be hardened. Finally the application should be developed in such a way that the possibility of misusing it is as limited as possible.</p>';

        var htmlRedundancy = '<p>System components are often multiple in number so that the availability of the system is not solely dependent on just one single component. This principle can also be applied to the security measures. When looking at the security architecture this too should consist out of a multiplicity of measures just so the security is not dependent on one single measure. This principle implicitly avoids the possibility of “Single Point of Failure” (SPoF).</p><p>A SpoF can lead to the unavailability of a service because a component of the entire environment is not executed in multiplicity. This could be a piece of hardware but also a database with no back-up options. In case access to the service is considered to be of the utmost importance and replacing the part in question takes too long, this could cause major problems. Dependent on the importance you can opt to keep the part in stock.</p>';

        var htmlIsolation = '<p>This means that hard- and software which are relevant for the security (the Trusted Computing Base (TCB)) should always be as small and compact as possible. The larger the TCB the more difficult it will be to verify if the security adequately safeguards it. This principle should, at minimum, be applied to firewalls.</p><br /><p>By firewall we mean a configuration of one or multiple systems which protect a network segment against unauthorized external access .If the configuration of the firewall systems, which are part of the ICT-infrastructure, is not done properly it could mean that the entire network becomes accessible to the outside world when the firewalls malfunction. The firewall systems which protect the production segments from unauthorized external traffic will have to be configured In such a way that when an issue arises no unauthorized parties are able to access the network. On top of that this firewall system will exclusively allow traffic which has been explicitly authorised. This last point is also known as the implementation of safe defaults, also known as the ‘deny all, except’ principle.</p>';

        var htmlSecuringthedata = '<p>Data on systems such as in databases, are very valuable. Access to this data shall have to be governed by means of a form of access control (even when this data is also stored on a back-up). This form of access control should at least consist out of an identification method (for example a password). To ensure that you will always have the right data available, it’s advisable to have a roll-back mechanism. The phasing out of hardware and software should not lead to data ending up in the wrong hands. This could, for example, be old systems with their hard drives still intact or discarded back-up tapes. The recording of both successful and unsuccessful authentications is an important tool in tracing and detecting attempts of unauthorized access. At the same time a welcome message with the user guidelines could prove to be useful in case a hacking attempt leads to a trial. As part of a Technical Security Assessment the focus shall be on protecting the data based on how they are available (located) on the operational systems. The other aforementioned aspects are part of the procedures and the necessary organisation. Protected (personal) data should not be accessible to third parties (users). Only authorized users will be able to view their personal data.</p>';

        var htmlAvailabilityofdata = '<p>The data should be available as much as possible. The availability is improved when the systems involved are showing behaving of fault tolerance and when they are resistant to Denial of Service (DoS) attacks. As part of Technical Security Assessment it’s possible to assess in which way the availability of data can be negatively influenced. This will allow the researchers to pinpoint the areas that could cause issues and how these problems can be avoided and / or solved.</p>';

        var htmlSecuringthecommunication = '<p>Dependent on the type of data which is send, it is important to ensure that that the sender is in fact the person he claims to be (anti-spoofing measures such as authentication). It can also be advisable to protect the information transfer from wiretapping and modification by third parties. Protection against wiretapping can be of importance to the information supplier, but also to the recipient out of possible privacy concerns.</p>';

        var htmlCompleteness = '<p>Any form of access can only happen after authorization by the system. Users and processes should therefore always have to authenticate them-(it)sel(f)ves. The necessary level of authentication can of course be dependent on the value of the system, data and processes which are running and / or are stored on the system.</p>';

        var htmlAdditionalsecurityofservers = '<p>This is subsequent to the aforementioned ‘defence in depth’ principle. A server should be configured for supplying one or more specific services. This principle is also known as ‘hardening’. A standard installation will not suffice as a standard installation will often make additional services available which could potentially lead to additional risks. Aside from that it’s important to ensure that the configuration of servers, who run the same services, are the same. This means that the timely installation of security related patches is an absolute necessity. A Technical Security Assessment can check what the current configuration is like and consequently can advise on necessary changes.</p>';

        var htmlLimitationofprivileges = '<p>The system should be configured in such a way that certain users and processes should not be allowed to execute more functions or resources than strictly necessary. This principle is also known as the ‘Least Privilege’, ‘Need to know’ and ‘Need to Use’. On the systems this principle of minimal privileges should be applied. Users will only get access to the areas of the system which should be accessible to them. The implementation if limitations cannot prevent possible break in attempts but it will reduce the effect of it. The principle of limitation can be implemented even further by adding a high degree of divisions of function. In this ‘Least privilege’ principle it’s also important to take the back office environment into consideration. The application with database management privileges should not be communicating with back office-systems. Another good example of limitation can be found in the prevention of unnecessary data leakage. All information that leaks to an attacker, such as text files, documentation, errors etc. make it easier for the attacker to break through the security. It is therefore important to prevent this from happening.</p>';

        htmlBody += '<div class="clsextrainfo"><h1 style="text-align:center;">Set of standards</h1><br/>' + htmlSetofstandards + '<br />';
        htmlBody += '<h1 style="text-align:center;">Diversity</h1><br/>' + htmlDiversity + '<div class="pagebreakafter"></div>';
        htmlBody += '<h1 style="text-align:center;">Redundancy</h1><br/>' + htmlRedundancy + '<br />';
        htmlBody += '<h1 style="text-align:center;">Isolation</h1><br/>' + htmlIsolation + '<div class="pagebreakafter"></div>';
        htmlBody += '<h1 style="text-align:center;">Securing the data</h1><br/>' + htmlSecuringthedata + '<br />';
        htmlBody += '<h1 style="text-align:center;">Availability of data</h1><br/>' + htmlAvailabilityofdata + '<div class="pagebreakafter"></div>';
        htmlBody += '<h1 style="text-align:center;">Securing the communication</h1><br/>' + htmlSecuringthecommunication + '<br />';
        htmlBody += '<h1 style="text-align:center;">Completeness</h1><br/>' + htmlCompleteness + '<br />';
        htmlBody += '<h1 style="text-align:center;">Additional security of servers</h1><br/>' + htmlAdditionalsecurityofservers + '<div class="pagebreakafter"></div>';
        htmlBody += '<h1 style="text-align:center;">Limitation of privileges</h1><br/>' + htmlLimitationofprivileges + '</div><div class="pagebreakafter"></div>';
    }
}

async function PDFLastPage(lang) {
    if (lang == 'nl') {
        htmlLastpage = '<div class="back-cover"><div class="logo-circle"><img src="fonts/appLogo.png" alt="..."></div><div class="pdf-footer"><div class="dt-column"><h6>Adres</h6><p>Rootsec B.V.<br> P.J.Oudweg 4<br>1314CH Almere</p></div><div class="dt-column"><h6>Telefoon</h6><p><a href="tel:036 760 0451">036 760 0451</a></p></div><div class="dt-column"><h6>Contact</h6><p>Emil Pilecki<br> e.<a href="mailto:pilecki@rootsec.com">pilecki@rootsec.com</a></p></div></div></div>';
    } else {
        htmlLastpage = '<div class="back-cover"><div class="logo-circle"><img src="fonts/appLogo.png" alt="..."></div><div class="pdf-footer"><div class="dt-column"><h6>Address</h6><p>Rootsec B.V.<br> P.J.Oudweg 4<br>1314CH Almere</p></div><div class="dt-column"><h6>Phone</h6><p><a href="tel:036 760 0451">036 760 0451</a></p></div><div class="dt-column"><h6>Contact</h6><p>Emil Pilecki<br> e.<a href="mailto:pilecki@rootsec.com">pilecki@rootsec.com</a></p></div></div></div>';
    }
    htmlBody += htmlLastpage;
}

function REfillcolor(type) {
    if (type) {
        if (type == 1) {
            return '#00d014';
        } else if (type == 2) {
            return '#eb144c';
        } else if (type == 3) {
            return '#ff6900';
        } else if (type == 4) {
            return '#fcb900';
        } else {
            return '#fcb900';
        }
    }
}

function Complexityfillcolor(type) {
    if (type) {
        if (type == 1) {
            return '#00d014';
        } else if (type == 2) {
            return '#fcb900';
        } else if (type == 3) {
            return '#ff6900';
        } else if (type == 4) {
            return '#eb144c';
        } else {
            return '#eb144c';
        }
    }
}

function PentestChecklistStatusColor(Type) {
    if (Type) {
        if (Type == 1) {
            return '#00D014'
        } else if (Type == 2) {
            return '#ff0000'
        } else {

        }
    }
}

exports.GeneratePDF = GeneratePDF;