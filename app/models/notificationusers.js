'use strict';
module.exports = (sequelize, DataTypes) => {
  const NotificationUsers = sequelize.define('NotificationUsers', {
    NotificationId: DataTypes.INTEGER,
    UserId: DataTypes.INTEGER,
    IsRead:DataTypes.TINYINT,
    CreatedAt: "DATETIME",
    CreatedBy: DataTypes.INTEGER,
    ModifiedAt: "DATETIME",
    ModifiedBy: DataTypes.INTEGER
  },  {
    tableName: 'tblNotificationUsers',
    timestamps: false
  });
  NotificationUsers.associate = function(models) {
    // associations can be defined here
    NotificationUsers.belongsTo(models.Notifications, { foreignKey: 'NotificationId',foreignKeyConstraint:true  });
    // NotificationUsers.belongsTo(models.User, { foreignKey: 'UserId',foreignKeyConstraint:true  });
  };
  return NotificationUsers;
};