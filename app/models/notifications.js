'use strict';
module.exports = (sequelize, DataTypes) => {
  const Notifications = sequelize.define('Notifications', {
    PenTestId: DataTypes.INTEGER,
    PenTestChecklistStatusId: DataTypes.INTEGER,
    CommentId: DataTypes.INTEGER,
    CreatedAt: "DATETIME",
    CreatedBy: DataTypes.INTEGER,
    ModifiedAt: "DATETIME",
    ModifiedBy: DataTypes.INTEGER
  },
    {
      tableName: 'tblNotifications',
      timestamps: false
    });
  Notifications.associate = function (models) {
    Notifications.belongsTo(models.PenTest,{ foreignKey: 'PenTestId' });
    Notifications.belongsTo(models.PenTestComments,{ foreignKey: 'CommentId' });
    // Notifications.belongsTo(models.PenTestChecklist,{ foreignKey: 'PenTestChecklistStatusId' });
    // associations can be defined here
  };
  return Notifications;
};