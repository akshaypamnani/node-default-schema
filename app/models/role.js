'use strict';
module.exports = (sequelize, DataTypes) => {
  const Role = sequelize.define('Role', {
    Name: DataTypes.STRING,
    Description: DataTypes.STRING,
    CreatedAt: "DATETIME",
    CreatedBy: DataTypes.INTEGER,
    ModifiedAt: "DATETIME",
    ModifiedBy: DataTypes.INTEGER,
    IsActive: DataTypes.TINYINT
  }, {
      tableName: 'tblRoles',
      timestamps: false,      
    defaultScope:{
      order: [['Name', 'ASC']]
    }
      // defaultScope: {
      //   where: {
      //     IsActive: true
      //   }
      // }
    });
  Role.associate = function (models) {
    // associations can be defined here
    // Role.hasMany(models.UserRole, { foreignKey: 'RoleId', foreignKeyConstraint: true });
  };
  return Role;
};