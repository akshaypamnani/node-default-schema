'use strict';
module.exports = (sequelize, DataTypes) => {
  const VlunerabilityAttachments = sequelize.define('VlunerabilityAttachments', {
    VulnerabilityId: DataTypes.INTEGER,
    FileName: DataTypes.STRING,
    CreatedAt: "DATETIME",
    CreatedBy: DataTypes.INTEGER,
    ModifiedAt: "DATETIME",
    ModifiedBy: DataTypes.INTEGER,
    IsActive: DataTypes.TINYINT
  }, {
      tableName: 'tblVlunerabilityAttachments',
      timestamps: false,
      // defaultScope: {
      //   where: {
      //     IsActive: true
      //   }
      // }
    });
  VlunerabilityAttachments.associate = function (models) {
    // associations can be defined here

    // VlunerabilityAttachments.belongsTo(models.Vulnerabilities, { foreignKey: 'VulnerabilityId' });
  };
  return VlunerabilityAttachments;
};