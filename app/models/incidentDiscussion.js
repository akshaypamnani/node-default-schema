'use strict';
module.exports = (sequelize, DataTypes) => {
  const IncidentDiscussions = sequelize.define('IncidentDiscussions', {
    IncidentId: DataTypes.INTEGER,
    Comment: DataTypes.STRING,
    CommentorId: DataTypes.INTEGER,
    CommentDate: "DATETIME",
    IsUrgentInput: DataTypes.TINYINT,
    IsDonotneedReply: DataTypes.TINYINT,
    CreatedAt: "DATETIME",
    CreatedBy: DataTypes.INTEGER,
    ModifiedAt: "DATETIME",
    ModifiedBy: DataTypes.INTEGER,
    IsActive: DataTypes.TINYINT
  }, {
    tableName: 'tblIncidentDiscussions',
    timestamps: false,
    defaultScope: {
      where: {
        IsActive: true
      }
    }
  });
  IncidentDiscussions.associate = function (models) {
  };
  return IncidentDiscussions;
};