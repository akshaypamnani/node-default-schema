'use strict';
module.exports = (sequelize, DataTypes) => {
  const Tickets = sequelize.define('Tickets', {
    TicketCode: DataTypes.STRING,
    Subject: DataTypes.STRING,
    Description: DataTypes.STRING,
    TicketType: DataTypes.INTEGER,
    ReferenceId: DataTypes.INTEGER,
    AssignedUser: DataTypes.INTEGER,
    Priority: DataTypes.INTEGER,
    Status: DataTypes.INTEGER,
    CreatedAt: "DATETIME",
    CreatedBy: DataTypes.INTEGER,
    ModifiedAt: "DATETIME",
    ModifiedBy: DataTypes.INTEGER,
    IsActive: DataTypes.TINYINT
  }, {
    tableName: 'tblTickets',
    timestamps: false,
    defaultScope: {
      where: {
        IsActive: true
      }
    }
  });
  Tickets.associate = function (models) {
  };
  return Tickets;
};