'use strict';
module.exports = (sequelize, DataTypes) => {
  const System = sequelize.define('System', {
    Name: DataTypes.STRING,
    Remarks: DataTypes.STRING,
    DepartmentId: DataTypes.INTEGER,
    CompanyId: DataTypes.INTEGER,
    CreatedAt: "DATETIME",
    CreatedBy: DataTypes.INTEGER,
    ModifiedAt: "DATETIME",
    ModifiedBy: DataTypes.INTEGER,
    Code:DataTypes.STRING,
    IsActive: DataTypes.TINYINT
  }, {
      tableName: 'tblSystem',
      timestamps: false
      // defaultScope: {
      //   where: {
      //     IsActive: true
      //   }
      // }
    });
  System.associate = function (models) {
    // associations can be defined here
    // System.hasMany(models.VulnerabilitySystem,{ foreignKey: 'SystemId' });
    //  System.belongsTo(models.Department, { foreignKey: 'DepartmentId',foreignKeyConstraint:true  });
    //  System.belongsTo(models.Company, { foreignKey: 'CompanyId',foreignKeyConstraint:true  });
   
    System.hasMany(models.Host, { foreignKey: 'SystemId', foreignKeyConstraint: true });

  };
  return System;
};