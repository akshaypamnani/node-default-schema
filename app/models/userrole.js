'use strict';
module.exports = (sequelize, DataTypes) => {
  const UserRole = sequelize.define('UserRole', {
    UserId: DataTypes.INTEGER,
    RoleId: DataTypes.INTEGER,
    CreatedAt: "DATETIME",
    CreatedBy: DataTypes.INTEGER,
    ModifiedAt: "DATETIME",
    ModifiedBy: DataTypes.INTEGER
  }, {
      tableName: 'tblUserRole',
      timestamps: false
    });
  UserRole.associate = function (models) {
    // associations can be defined here
    // UserRole.belongsTo(models.User, { foreignKey: 'UserId', foreignKeyConstraint: true });
    UserRole.belongsTo(models.Role, { foreignKey: 'RoleId', foreignKeyConstraint: true });
  };
  return UserRole;
};