'use strict';
module.exports = (sequelize, DataTypes) => {
  const ChatMessageNotifications = sequelize.define('ChatMessageNotifications', {
    MessageId: DataTypes.INTEGER,
    UserId: DataTypes.INTEGER,
    ReceiverId: DataTypes.INTEGER,
    IsRead: DataTypes.INTEGER,
    CreatedAt: "DATETIME",
    CreatedBy: DataTypes.INTEGER,
    ModifiedAt: "DATETIME",
    ModifiedBy: DataTypes.INTEGER
  }, {
    tableName: 'tblChatMessageNotifications',
    timestamps: false
  });
  ChatMessageNotifications.associate = function (models) { };
  return ChatMessageNotifications;
};