'use strict';
module.exports = (sequelize, DataTypes) => {
  const UserCompany = sequelize.define('UserCompany', {
    UserId: DataTypes.INTEGER,
    CompanyId: DataTypes.INTEGER,
    CreatedAt: "DATETIME",
    CreatedBy: DataTypes.INTEGER,
    ModifiedAt: "DATETIME",
    ModifiedBy: DataTypes.INTEGER
  }, {
    tableName: 'tblUserCompany',
    timestamps: false
  });
  UserCompany.associate = function(models) {    
    UserCompany.belongsTo(models.User, { foreignKey: 'UserId',foreignKeyConstraint:true  });
    UserCompany.belongsTo(models.Company, { foreignKey: 'CompanyId',foreignKeyConstraint:true  });
    // associations can be defined here
  };
  return UserCompany;
};