
'use strict';
var crypto = require('crypto');

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    FirstName: DataTypes.STRING,
    LastName: DataTypes.STRING,
    Email: DataTypes.STRING,
    MobileNo: DataTypes.STRING,
    IP: DataTypes.STRING,
    // UserName:DataTypes.STRING,
    Password: DataTypes.STRING,
    TwoFACode: DataTypes.STRING,
    TwoFACodeTime: "DATETIME",
    hashedPassword: DataTypes.STRING,
    IsfirstLogin: DataTypes.TINYINT,
    IsResetPassword: DataTypes.TINYINT,
    CreatedAt: "DATETIME",
    CreatedBy: DataTypes.INTEGER,
    ModifiedAt: "DATETIME",
    ModifiedBy: DataTypes.INTEGER,
    IsActive: DataTypes.TINYINT,
    salt: DataTypes.STRING,
    Code:DataTypes.STRING
  }, {
      tableName: 'tblUser',
      timestamps: false,

     
    });
  User.associate = function (models) {
    User.hasMany(models.UserCompany, { foreignKey: 'UserId' });
    User.hasMany(models.UserIps, { foreignKey: 'UserId' });
    User.hasOne(models.UserRole, { foreignKey: 'UserId' });
  };

  // User.prototype.setPassword = function(password) { 
  //      this.salt = crypto.randomBytes(16).toString('hex'); 
  //      this.hashedPassword = crypto.pbkdf2Sync(password, this.salt,  
  //      1000, 64, `sha512`).toString(`hex`); 
  //  }; 

  User.prototype.validPassword = function (password) {
    if (!this.salt) {
      return false;
    }
    var hash = crypto.pbkdf2Sync(password,
      this.salt.toString(), 1000, 64, `sha512`).toString(`hex`);
    return this.hashedPassword === hash;
  };
  return User;
};