'use strict';
module.exports = (sequelize, DataTypes) => {
  const Company = sequelize.define('Company', {
    Name: DataTypes.STRING,
    LegalEntity: DataTypes.STRING,
    Logo: DataTypes.STRING	,
    CommerceNo: DataTypes.STRING,
    ContactPersonEmail: DataTypes.STRING,
    IpAddress: DataTypes.STRING,
    CreatedAt: "DATETIME",
    CreatedBy: DataTypes.INTEGER,
    ModifiedAt: "DATETIME",
    ModifiedBy: DataTypes.INTEGER,
    IsActive: DataTypes.TINYINT,
    Code:DataTypes.STRING
  }, {
    tableName: 'tblCompany',
    timestamps: false,   
    defaultScope: {      
      where: {
        IsActive: true
      },
      // order: [['Name', 'ASC']]
    }
  });
  Company.associate = function(models) {
    // associations can be defined here
  Company.hasMany(models.UserCompany,{ foreignKey: 'CompanyId' });
  Company.hasMany(models.Asset,{ foreignKey: 'CompanyId' });
  Company.hasMany(models.System,{ foreignKey: 'CompanyId' });
  // Company.hasMany(models.PenTest,{ foreignKey: 'CompanyId' });
  };
  return Company;
};