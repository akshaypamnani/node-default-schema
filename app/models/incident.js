'use strict';
module.exports = (sequelize, DataTypes) => {
  const Incidents = sequelize.define('Incidents', {
    IncidentCode: DataTypes.STRING,
    Subject: DataTypes.STRING,
    Description: DataTypes.STRING,
    IncidentType: DataTypes.INTEGER,
    ReferenceId: DataTypes.INTEGER,
    AssignedUser: DataTypes.INTEGER,
    Priority: DataTypes.INTEGER,
    Status: DataTypes.INTEGER,
    CreatedAt: "DATETIME",
    CreatedBy: DataTypes.INTEGER,
    ModifiedAt: "DATETIME",
    ModifiedBy: DataTypes.INTEGER,
    IsActive: DataTypes.TINYINT
  }, {
    tableName: 'tblIncidents',
    timestamps: false,
    defaultScope: {
      where: {
        IsActive: true
      }
    }
  });
  Incidents.associate = function (models) {
  };
  return Incidents;
};