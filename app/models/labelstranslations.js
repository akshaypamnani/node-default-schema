'use strict';
module.exports = (sequelize, DataTypes) => {
  const LabelsTranslations = sequelize.define('LabelsTranslations', {
    DefaultKey: DataTypes.STRING,
    TitleEN: DataTypes.STRING,
    TitleNL: DataTypes.STRING,
    CreatedAt: "DATETIME",
    CreatedBy: DataTypes.INTEGER,
    ModifiedAt: "DATETIME",
    ModifiedBy: DataTypes.INTEGER,
    IsActive: DataTypes.TINYINT
  }, {
    tableName: 'tblLabelsTranslations',
    timestamps: false,
    // defaultScope: {
    //   where: {
    //     IsActive: true
    //   },
    //   order: [['Name', 'ASC']]
    // }
  });
  LabelsTranslations.associate = function (models) {
  };
  return LabelsTranslations;
};