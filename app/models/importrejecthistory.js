'use strict';
module.exports = (sequelize, DataTypes) => 
{
  const ImportRejectHistory = sequelize.define('ImportRejectHistory', 
  {
    ImportMasterId: DataTypes.INTEGER,
    columnName:DataTypes.STRING,
    message:DataTypes.STRING,
    CreatedAt: "DATETIME",
    CreatedBy: DataTypes.INTEGER,
    ModifiedAt: "DATETIME",
    ModifiedBy: DataTypes.INTEGER,
    IsActive: DataTypes.TINYINT
    
  }, {
    tableName: 'tblImportRejectHistory',
    timestamps: false,
    
  });
  ImportRejectHistory.associate = function (models) 
  {
    // associations can be defined here
    // ImportRejectHistory.belongsTo(models.ImportMaster, { foreignKey: 'ImportMasterId',foreignKeyConstraint:true  });
  };
  return ImportRejectHistory;
};