'use strict';
module.exports = (sequelize, DataTypes) => {
  const Damages = sequelize.define('Damages', {
    Name: DataTypes.STRING,
    NameEN: DataTypes.STRING,
    Description: DataTypes.STRING,
    CreatedAt: "DATETIME",
    CreatedBy: DataTypes.INTEGER,
    ModifiedAt: "DATETIME",
    ModifiedBy: DataTypes.INTEGER,
    IsActive: DataTypes.TINYINT
  },
    {
      tableName: 'tblDamages',
      timestamps: false,
      // defaultScope:{
      //   order: [['Name', 'ASC']]
      // }
    }
  );
  Damages.associate = function (models) {
    // associations can be defined here
    Damages.hasMany(models.DamageOptions, { foreignKey: 'DamageId' });
    // Damages.hasOne(models.VulnerabilityDamages, { foreignKey: 'DamageId' });
  };
  return Damages;
};