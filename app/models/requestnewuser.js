
'use strict';
var crypto = require('crypto');

module.exports = (sequelize, DataTypes) => {
  const RequestNewUser = sequelize.define('RequestNewUser', {
    FirstName: DataTypes.STRING,
    LastName: DataTypes.STRING,
    Email: DataTypes.STRING,
    MobileNo: DataTypes.STRING,
    IP: DataTypes.STRING,
    JobDescription: DataTypes.STRING,
    IsAccepted: DataTypes.TINYINT,
    RequestedBy: DataTypes.INTEGER,
    CompanyId: DataTypes.INTEGER,
    CreatedAt: "DATETIME",
    CreatedBy: DataTypes.INTEGER,
    ModifiedAt: "DATETIME",
    ModifiedBy: DataTypes.INTEGER,
    IsActive: DataTypes.TINYINT
  }, {
    tableName: 'tblRequestNewUser',
    timestamps: false,

    defaultScope: {
      where: {
        IsActive: true
      },
      order: [['FirstName', 'ASC']]
    }
  });
  RequestNewUser.associate = function (models) {
    RequestNewUser.belongsTo(models.User, { foreignKey: 'RequestedBy' });
    RequestNewUser.belongsTo(models.Company, { foreignKey: 'CompanyId' });
  };
  return RequestNewUser;
};