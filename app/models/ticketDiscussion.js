'use strict';
module.exports = (sequelize, DataTypes) => {
  const TicketDiscussions = sequelize.define('TicketDiscussions', {
    TicketId: DataTypes.INTEGER,
    Comment: DataTypes.STRING,
    CommentorId: DataTypes.INTEGER,
    CommentDate: "DATETIME",
    IsUrgentInput: DataTypes.TINYINT,
    IsDonotneedReply: DataTypes.TINYINT,
    CreatedAt: "DATETIME",
    CreatedBy: DataTypes.INTEGER,
    ModifiedAt: "DATETIME",
    ModifiedBy: DataTypes.INTEGER,
    IsActive: DataTypes.TINYINT
  }, {
    tableName: 'tblTicketDiscussions',
    timestamps: false,
    defaultScope: {
      where: {
        IsActive: true
      }
    }
  });
  TicketDiscussions.associate = function (models) {
  };
  return TicketDiscussions;
};