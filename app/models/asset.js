'use strict';
module.exports = (sequelize, DataTypes) => {
  const Asset = sequelize.define('Asset', {
    Name: DataTypes.STRING,
    Urls: DataTypes.STRING,
    IpRange: DataTypes.STRING,
    Software: DataTypes.STRING,
    Remarks: DataTypes.STRING,
    CompanyId: DataTypes.INTEGER,
    SystemId: DataTypes.INTEGER,
    CreatedAt: "DATETIME",
    CreatedBy: DataTypes.INTEGER,
    ModifiedAt: "DATETIME",
    ModifiedBy: DataTypes.INTEGER,
    IsActive: DataTypes.TINYINT,
    Code:DataTypes.STRING
  }, {
    tableName: 'tblAsset',
    timestamps: false
    
    // defaultScope: {
    //   where: {
    //     IsActive: true
    //   }
    // }
  });
  Asset.associate = function (models) {
    // associations can be defined here
    Asset.belongsTo(models.System, { foreignKey: 'SystemId' });
    Asset.belongsTo(models.Company, { foreignKey: 'CompanyId', foreignKeyConstraint: true });
    Asset.hasMany(models.Host, { foreignKey: 'AssetId', foreignKeyConstraint: true });
    Asset.hasMany(models.LanguageTranslations, { as: 'LanguageTranslations', foreignKey: 'ReferenceId' });
    // Asset.hasMany(models.PenTestAssets,{ foreignKey: 'AssetId' });
  };
  return Asset;
};