'use strict';
module.exports = (sequelize, DataTypes) => {
  const OpportunityOptions = sequelize.define('OpportunityOptions', {
    OpportunityId: DataTypes.INTEGER,
    OptionText: DataTypes.STRING,
    OptionTextEN: DataTypes.STRING,
    CreatedAt: "DATETIME",
    CreatedBy: DataTypes.INTEGER,
    ModifiedAt: "DATETIME",
    ModifiedBy: DataTypes.INTEGER,
    IsActive: DataTypes.TINYINT
  }, {
      tableName: 'tblOpportunityOptions',
      timestamps: false
    });
  OpportunityOptions.associate = function (models) {
  };
  return OpportunityOptions;
};