'use strict';
module.exports = (sequelize, DataTypes) => {
  const UserIps = sequelize.define('UserIps', {
    IP: DataTypes.STRING,
    UserId: DataTypes.INTEGER,
    IsIPblocked: DataTypes.TINYINT,
    Wrongattemptcount: DataTypes.INTEGER,
    CreatedAt: "DATETIME",
    CreatedBy: DataTypes.INTEGER,
    ModifiedAt: "DATETIME",
    ModifiedBy: DataTypes.INTEGER,
    IsActive: DataTypes.TINYINT
  },
    {
      tableName: 'tblUserIps',
      timestamps: false,
      // defaultScope:{
      //   order: [['Name', 'ASC']]
      // }
    }
  );
  UserIps.associate = function (models) {
    // associations can be defined here
    UserIps.belongsTo(models.User, { foreignKey: 'UserId' });
    // Damages.hasOne(models.VulnerabilityDamages, { foreignKey: 'DamageId' });
  };
  return UserIps;
};