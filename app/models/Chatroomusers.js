'use strict';
module.exports = (sequelize, DataTypes) => {
  const ChatroomUsers = sequelize.define('ChatroomUsers', {
    UserId: DataTypes.INTEGER,
    RoomId: DataTypes.INTEGER,
    CreatedAt: "DATETIME",
    CreatedBy: DataTypes.INTEGER,
    ModifiedAt: "DATETIME",
    ModifiedBy: DataTypes.INTEGER
  }, {
    tableName: 'tblChatroomUsers',
    timestamps: false
  });
  ChatroomUsers.associate = function (models) { };
  return ChatroomUsers;
};