'use strict';
module.exports = (sequelize, DataTypes) => {
  const Vulnerabilities = sequelize.define('Vulnerabilities', {
    PenTestChecklistStatusId: DataTypes.INTEGER,
    PenTestId: DataTypes.INTEGER,
    Name: DataTypes.STRING,
    Facing: DataTypes.INTEGER,
    Risk: DataTypes.INTEGER,
    Complexity: DataTypes.INTEGER,
    Effort: DataTypes.INTEGER,
    SystemsDesc: DataTypes.STRING,
    RiskDesc: DataTypes.STRING,
    VulnSolution: DataTypes.STRING,
    IssueType: DataTypes.INTEGER,
    IsAccepted: DataTypes.TINYINT,
    AcceptedRemarks: DataTypes.STRING,
    AcceptedBy: DataTypes.INTEGER,
    AcceptedSignature: DataTypes.STRING,
    ArgumentId: DataTypes.INTEGER,
    Order: DataTypes.INTEGER,
    Status: DataTypes.INTEGER,
    CreatedAt: "DATETIME",
    CreatedBy: DataTypes.INTEGER,
    ModifiedAt: "DATETIME",
    ModifiedBy: DataTypes.INTEGER,
    IsActive: DataTypes.TINYINT,
    Code: DataTypes.STRING
  }, {
    tableName: 'tblVulnerabilities',
    timestamps: false,
    // defaultScope: {
    //   order: [
    //     ['Risk', 'ASC']
    // ]
    // }
  });
  Vulnerabilities.associate = function (models) {
    // associations can be defined here
    // Vulnerabilities.hasMany(models.VulnerabilityHosts,{ foreignKey: 'HostId' });
    Vulnerabilities.hasMany(models.VulnerabilityHosts, { foreignKey: 'VulnerabilityId' });
    Vulnerabilities.hasMany(models.VulnerabilityOpportunities, { foreignKey: 'VulnerabilityId' });
    Vulnerabilities.hasMany(models.VulnerabilityDamages, { foreignKey: 'VulnerabilityId' });
    Vulnerabilities.hasMany(models.VlunerabilityAttachments, { foreignKey: 'VulnerabilityId' });
    Vulnerabilities.hasMany(models.LanguageTranslations, { foreignKey: 'ReferenceId' });
    Vulnerabilities.hasMany(models.VulnerabilityAsset, { foreignKey: 'VulnerabilityId' });
    Vulnerabilities.hasMany(models.VulnerabilityUser, { foreignKey: 'VulnerabilityId' });
    Vulnerabilities.belongsTo(models.PenTest, { foreignKey: 'PenTestId' });
    Vulnerabilities.belongsTo(models.PenTestCheckListStatus, { foreignKey: 'PenTestCheckListStatusId' });
  };
  return Vulnerabilities;
};