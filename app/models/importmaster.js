'use strict';
module.exports = (sequelize, DataTypes) => {
  const ImportMasters = sequelize.define('ImportMasters',
    {
      ImportType: DataTypes.TINYINT,
      Status: DataTypes.TINYINT,
      Filename: DataTypes.STRING,
      CreatedAt: "DATETIME",
      CreatedBy: DataTypes.INTEGER,
      ModifiedAt: "DATETIME",
      ModifiedBy: DataTypes.INTEGER,
      IsActive: DataTypes.TINYINT

    }, {
    tableName: 'tblImportMasters',
    timestamps: false,
  });
  ImportMasters.associate = function (models) {
  };
  return ImportMasters;
};