'use strict';
module.exports = (sequelize, DataTypes) => {
  const TicketAttachments = sequelize.define('TicketAttachments', {
    TicketId: DataTypes.INTEGER,
    TicketType: DataTypes.TINYINT,
    FileName: DataTypes.STRING,
    CreatedAt: "DATETIME",
    CreatedBy: DataTypes.INTEGER,
    ModifiedAt: "DATETIME",
    ModifiedBy: DataTypes.INTEGER,
    IsActive: DataTypes.TINYINT
  }, {
    tableName: 'tblTicketAttachments',
    timestamps: false,
    defaultScope: {
      where: {
        IsActive: true
      }
    }
  });
  TicketAttachments.associate = function (models) {
  };
  return TicketAttachments;
};