'use strict';
module.exports = (sequelize, DataTypes) => {
  const DamageOptions = sequelize.define('DamageOptions', {
    DamageId: DataTypes.INTEGER,
    OptionText: DataTypes.STRING,
    OptionTextEN: DataTypes.STRING,
    CreatedAt: "DATETIME",
    CreatedBy: DataTypes.INTEGER,
    ModifiedAt: "DATETIME",
    ModifiedBy: DataTypes.INTEGER,
    IsActive: DataTypes.TINYINT
  }, {
      tableName: 'tblDamageOptions',
      timestamps: false
    });
    DamageOptions.associate = function (models) {
  };
  return DamageOptions;
};