'use strict';
module.exports = (sequelize, DataTypes) => {
  const Department = sequelize.define('Department', {
    Name: DataTypes.STRING,
    Description: DataTypes.STRING,
    CreatedAt: "DATETIME",
    CreatedBy: DataTypes.INTEGER,
    ModifiedAt: "DATETIME",
    ModifiedBy: DataTypes.INTEGER,
    IsActive: DataTypes.TINYINT
  },
    {
      tableName: 'tblDepartment',
      timestamps: false,
      defaultScope:{
        order: [['Name', 'ASC']]
      }
      // defaultScope: {
      //   where: {
      //     IsActive: true
      //   }
      // }
    }
  );
  Department.associate = function (models) {
    // associations can be defined here
  };
  return Department;
};