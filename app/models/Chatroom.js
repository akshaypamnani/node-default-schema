
'use strict';
var crypto = require('crypto');

module.exports = (sequelize, DataTypes) => {
  const Chatroom = sequelize.define('Chatroom', {
    RoomCode: DataTypes.STRING,
    ChatType: DataTypes.INTEGER,
    CreatedAt: "DATETIME",
    CreatedBy: DataTypes.INTEGER,
    ModifiedAt: "DATETIME",
    ModifiedBy: DataTypes.INTEGER,
    IsActive: DataTypes.TINYINT,
  }, {
    tableName: 'tblChatroom',
    timestamps: false,

    defaultScope: {
      where: {
        IsActive: true
      }
    }
  });
  Chatroom.associate = function (models) {
  };

  return Chatroom;
};