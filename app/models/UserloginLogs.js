'use strict';
module.exports = (sequelize, DataTypes) => {
  const UserLoginLogs = sequelize.define('UserLoginLogs', {
    UserId: DataTypes.INTEGER,
    loginTime: "DATETIME",
    logoutTime: "DATETIME",
    logdate: "DATE",
    durationTime: DataTypes.INTEGER,
    IsOnline: DataTypes.INTEGER,
    AccessToken: DataTypes.STRING,
    CreatedAt: "DATETIME",
    ModifiedAt: "DATETIME",
    IsActive: DataTypes.TINYINT
  },
    {
      tableName: 'tblUserloginlogoutLogs',
      timestamps: false,
    }
  );
  UserLoginLogs.associate = function (models) {
  };
  return UserLoginLogs;
};