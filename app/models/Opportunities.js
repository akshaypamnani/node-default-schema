'use strict';
module.exports = (sequelize, DataTypes) => {
  const Opportunities = sequelize.define('Opportunities', {
    Name: DataTypes.STRING,
    NameEN: DataTypes.STRING,
    Description: DataTypes.STRING,
    CreatedAt: "DATETIME",
    CreatedBy: DataTypes.INTEGER,
    ModifiedAt: "DATETIME",
    ModifiedBy: DataTypes.INTEGER,
    IsActive: DataTypes.TINYINT
  },
    {
      tableName: 'tblOpportunities',
      timestamps: false,
      // defaultScope:{
      //   order: [['Name', 'ASC']]
      // }
    }
  );
  Opportunities.associate = function (models) {
    // associations can be defined here
    Opportunities.hasMany(models.OpportunityOptions, { foreignKey: 'OpportunityId' });
    // Opportunities.hasOne(models.VulnerabilityOpportunities, { foreignKey: 'OpportunityId' });
  };
  return Opportunities;
};