'use strict';
module.exports = (sequelize, DataTypes) => {
  const LanguageTranslations = sequelize.define('LanguageTranslations', {
    ReferenceId: DataTypes.INTEGER,
    DefaultKey: DataTypes.STRING,
    TitleEN: DataTypes.STRING,
    TitleNL: DataTypes.STRING,
    PageRefId: DataTypes.INTEGER,
    PageRefName: DataTypes.STRING,
    CreatedAt: "DATETIME",
    CreatedBy: DataTypes.INTEGER,
    ModifiedAt: "DATETIME",
    ModifiedBy: DataTypes.INTEGER,
    IsActive: DataTypes.TINYINT
  }, {
    tableName: 'tblLanguageTranslations',
    timestamps: false,
    // defaultScope: {
    //   where: {
    //     IsActive: true
    //   },
    //   order: [['Name', 'ASC']]
    // }
  });
  LanguageTranslations.associate = function (models) {
  };
  return LanguageTranslations;
};