'use strict';
module.exports = (sequelize, DataTypes) => {
  const BlockUnblockIPlog = sequelize.define('BlockUnblockIPlog', {
    IP: DataTypes.STRING,
    UserIPId: DataTypes.INTEGER,
    Type: DataTypes.TINYINT,
    BlockUnblockDate: "DATETIME",
    BlockUnblockBy: DataTypes.INTEGER,
    IsActive: DataTypes.TINYINT
  },
    {
      tableName: 'tblBlockUnblockIPlog',
      timestamps: false,
      // defaultScope:{
      //   order: [['Name', 'ASC']]
      // }
    }
  );
  BlockUnblockIPlog.associate = function (models) {
    // associations can be defined here
    // Damages.hasMany(models.DamageOptions, { foreignKey: 'DamageId' });
    // Damages.hasOne(models.VulnerabilityDamages, { foreignKey: 'DamageId' });
  };
  return BlockUnblockIPlog;
};