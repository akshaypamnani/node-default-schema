
'use strict';
var crypto = require('crypto');

module.exports = (sequelize, DataTypes) => {
  const ChatMessages = sequelize.define('ChatMessages', {
    Message: DataTypes.STRING,
    ChatroomId: DataTypes.INTEGER,
    MessageType: DataTypes.INTEGER,
    SendBy: DataTypes.INTEGER,
    CreatedAt: "DATETIME",
    CreatedBy: DataTypes.INTEGER,
    ModifiedAt: "DATETIME",
    ModifiedBy: DataTypes.INTEGER,
    IsActive: DataTypes.TINYINT,
  }, {
    tableName: 'tblChatMessages',
    timestamps: false,

    defaultScope: {
      where: {
        IsActive: true
      }
    }
  });
  ChatMessages.associate = function (models) {
  };

  return ChatMessages;
};