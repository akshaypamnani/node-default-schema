'use strict';
module.exports = (sequelize, DataTypes) => {
  const Host = sequelize.define('Host', {
    PortNo: DataTypes.STRING,
    Protocol: DataTypes.STRING,
    ProtocolType: DataTypes.STRING,
    HostIP: DataTypes.STRING,
    Type: DataTypes.STRING,
    TypeId: DataTypes.INTEGER,
    AssetId: DataTypes.INTEGER,
    SystemId: DataTypes.INTEGER,
    CreatedAt: "DATETIME",
    CreatedBy: DataTypes.INTEGER,
    ModifiedAt: "DATETIME",
    ModifiedBy: DataTypes.INTEGER,
    IsActive: DataTypes.TINYINT
  }, {
    tableName: 'tblHost',
      timestamps: false,
      // defaultScope: {
      //   where: {
      //     IsActive: true
      //   }
      // }
    });
  Host.associate = function (models) {
    // associations can be defined here

   // Host.hasMany(models.VulnerabilityHosts, { foreignKey: 'HostId' });
    // Host.hasMany(models.VulnerabilityHosts,{ foreignKey: 'VulnerabilityId' });
    // Host.belongsTo(models.Asset, { foreignKey: 'AssetId', foreignKeyConstraint: false });
  //  Host.belongsTo(models.System, { foreignKey: 'SystemId', foreignKeyConstraint: false });
  };
  return Host;
};