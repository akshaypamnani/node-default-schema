const schema =
{
    // company object schema
    'Company Code':
    {
        prop: 'companyCode',
        type: String
    },
    'Company Name':
    {
        prop: 'companyName',
        type: String
    },
    'Company Legal Entity':
    {
        prop: 'companyLegalEntity',
        type: String
    },
    'Company Logo':
    {
        prop: 'companyLogo',
        type: String
    },
    'Company Commerce No':
    {
        prop: 'companyCommerceNo',
        type: String
    },
    'Company Contact Person Email':
    {
        prop: 'companyContactPersonEmail',
        type: String
    },
    'Company Ip Address':
    {
        prop: 'companyIpAddress',
        type: String
    },

    // system object schema

    'System Code':
    {
        prop: 'systemCode',
        type: String
    },
    'System Name':
    {
        prop: 'systemName',
        type: String
    },
    'System Remarks':
    {
        prop: 'systemRemarks',
        type: String
    },
    'System DepartmentId':
    {
        prop: 'systemDepartmentId',
        type: Number
    },

    // assets object schema
    'Asset Code':
    {
        prop: 'assetCode',
        type: String
    },
    'Asset Name':
    {
        prop: 'assetName',
        type: String
    },
    'Asset Urls':
    {
        prop: 'assetUrls',
        type: String
    },
    'Asset IpRange':
    {
        prop: 'assetIpRange',
        type: String
    },
    'Asset Software':
    {
        prop: 'assetSoftware',
        type: String
    },
    'Asset Remarks':
    {
        prop: 'assetRemarks',
        type: String
    },

    // user object schema

    'User Code':
    {
        prop: 'userCode',
        type: String
    },
    'User Firstname':
    {
        prop: 'userFirstname',
        type: String
    },
    'User Lastname':
    {
        prop: 'userLastname',
        type: String
    },
    'User Email':
    {
        prop: 'userEmail',
        type: String
    },
    'User MobileNo':
    {
        prop: 'userMobileNo',
        type: String
    },
    'User IP':
    {
        prop: 'userIP',
        type: String
    },
    'User Role':
    {
        prop: 'userRole',
        type: String
    },
      // Vulnerabilitie object schema

      'Pentest Code':
      {
          prop: 'PentestCode',
          type: String
      },
      'Vulnerability Code':
      {
          prop: 'VulnerabilityCode',
          type: String
      },
      'Vulnerability Name':
      {
          prop: 'VulnerabilityName',
          type: String
      },
      'Facing':
      {
          prop: 'Facing',
          type: String
      },
      'Risk':
      {
          prop: 'Risk',
          type: String
      },
      'Complexity':
      {
          prop: 'Complexity',
          type: String
      },
      'Effort':
      {
          prop: 'Effort',
          type: String
      },
      'System Description':
      {
          prop: 'SystemDescription',
          type: String
      },
      'Risk Description':
      {
          prop: 'RiskDescription',
          type: String
      },
      'Vuln Solution':
      {
          prop: 'VulnSolution',
          type: String
      },
      'Issue Type':
      {
          prop: 'IssueType',
          type: String
      }


}

exports.schema = schema;