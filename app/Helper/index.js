const db = require('../models');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
var config = require('../../app/config/config');
var jwt = require('jsonwebtoken');
const SECRET_KEY = "secretkey12345";

async function GetLatestCode(model) {
    var newCode = '';
    try {

        var objCode = await db[model].findOne({
            where:
            {
                IsActive: true,
            },
            order: [['id', 'DESC']]
        })

        // let Prefix = getPrefix(model);
        let Prefix = config.TablePrefix[model];

        if (objCode) {
            newCode = GenerateCode(objCode.Code, Prefix);
        }
        else {
            newCode = GenerateCode('NEW', Prefix);
        }

    }
    catch
    {

    }
    return newCode;
}

function GenerateCode(Code, Prefix) {
    var NewCode = Prefix + '000';
    var latestYear = ((new Date()).getFullYear()).toString().substring(2);

    NewCode += latestYear;

    if (Code == 'NEW') {
        NewCode += '001';
    }
    else {
        var lastYear = Code.substring(6, 8);
        var OldCode = Code.substring(8);

        if (lastYear == latestYear) {
            if (OldCode && parseInt(OldCode)) {
                OldCode = (parseInt(OldCode) + 1).toString();
                var pad = "000";
                OldCode = pad.substring(0, pad.length - OldCode.length) + OldCode;
                NewCode += OldCode;
            }
        } else {
            OldCode = '001';
            var pad = "000";
            OldCode = pad.substring(0, pad.length - OldCode.length) + OldCode;
            NewCode += OldCode;
        }
    }
    return NewCode;
}

async function VerifyToken(token) {
    try {
        var VerifiedToken = await jwt.verify(token, SECRET_KEY);
        console.log('VerifiedToken', VerifiedToken);
        return VerifiedToken;
    }
    catch (err) {
        return res.status(500).send({ status: 0, message: 'Faileddd', data: err });
    }
    // return VerifiedToken;
}

async function VerifiedUser(token) {
    try {
        token = token.toString().substring(7);
        var VerifiedToken = await jwt.verify(token, SECRET_KEY);
        console.log('VerifiedToken', VerifiedToken);
        return VerifiedToken;
    }
    catch (err) {
        return res.status(500).send({ status: 0, message: 'Faileddd', data: err });
    }
    // return VerifiedToken;
}

exports.GenerateCode = GenerateCode;
exports.GetLatestCode = GetLatestCode;
exports.VerifyToken = VerifyToken;
exports.VerifiedUser = VerifiedUser;



